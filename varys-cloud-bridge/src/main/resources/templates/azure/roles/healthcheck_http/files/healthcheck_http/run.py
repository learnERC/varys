#!/usr/bin/env python3

from elasticsearch import Elasticsearch
import os
import requests
import time

ELASTICSEARCH_HOST = os.getenv('ELASTICSEARCH_HOST', 'localhost')
TARGET_HOST = "http://{}/_health".format(os.getenv('TARGET_HOST'))

def main():
    client = Elasticsearch(host=ELASTICSEARCH_HOST, port=9092)
    while True:
        r = requests.get(TARGET_HOST)
        client.index(index='healthcheck_probe', doc_type='_doc', body=r.json())
        time.sleep(30)

if __name__ == '__main__':
    main()
