package io.varys.cloudbridge.config;

import freemarker.template.TemplateExceptionHandler;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
public class FreeMarkerConfig {

  @Bean
  public freemarker.template.Configuration configuration(
      @Value("${templatesRootDir}") Resource templatesRootDir) throws IOException {
    final var configuration =
        new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_30);
    configuration.setDirectoryForTemplateLoading(templatesRootDir.getFile());
    configuration.setDefaultEncoding("UTF-8");
    configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    configuration.setLogTemplateExceptions(false);
    configuration.setWrapUncheckedExceptions(true);
    configuration.setFallbackOnNullLoopVariable(false);

    return configuration;
  }
}
