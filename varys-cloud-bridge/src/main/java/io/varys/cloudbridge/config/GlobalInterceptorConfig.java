package io.varys.cloudbridge.config;

import io.varys.cloudbridge.interceptor.LogGrpcInterceptor;
import net.devh.boot.grpc.server.interceptor.GlobalServerInterceptorConfigurer;
import net.devh.boot.grpc.server.interceptor.GlobalServerInterceptorRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GlobalInterceptorConfig {

  @Bean
  public GlobalServerInterceptorConfigurer globalInterceptorConfigurerAdapter() {
    return new GlobalServerInterceptorConfigurer() {
      @Override
      public void addServerInterceptors(GlobalServerInterceptorRegistry registry) {
        registry.addServerInterceptors(new LogGrpcInterceptor());
      }
    };
  }
}
