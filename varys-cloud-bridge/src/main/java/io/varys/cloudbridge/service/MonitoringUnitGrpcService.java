package io.varys.cloudbridge.service;

import io.grpc.stub.StreamObserver;
import io.varys.cloudbridge.grpc.message.MonitoringUnitSyncStatus;
import io.varys.cloudbridge.grpc.message.ProbeConfiguration;
import io.varys.cloudbridge.grpc.message.ProbeSyncStatus;
import io.varys.cloudbridge.grpc.service.MonitoringUnitServiceGrpc;
import io.varys.cloudbridge.grpc.service.SynchronizeReply;
import io.varys.cloudbridge.grpc.service.SynchronizeRequest;
import io.varys.cloudbridge.plugin.TargetSystemPlugin;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Value;

@GrpcService
@Slf4j
public class MonitoringUnitGrpcService
    extends MonitoringUnitServiceGrpc.MonitoringUnitServiceImplBase {

  private final List<TargetSystemPlugin> targetSystemPlugins;

  public MonitoringUnitGrpcService(List<TargetSystemPlugin> targetSystemPlugins) {
    this.targetSystemPlugins = targetSystemPlugins;
  }

  private static Predicate<TargetSystemPlugin> canHandle(String targetSystem) {
    return p -> p.getTargetSystem().equals(targetSystem);
  }

  @Override
  public void synchronize(
      SynchronizeRequest request, StreamObserver<SynchronizeReply> responseObserver) {

    final var monitoringUnit = request.getMonitoringUnit();
    final var probesToAdd = request.getProbesToAddMap();
    final var probesToUpdate = request.getProbesToUpdateMap();
    final var probesToDelete = request.getProbesToDeleteMap();

    logger.debug("Found {} target system plugins", targetSystemPlugins.size());

      targetSystemPlugins.stream()
          .filter(canHandle(monitoringUnit.getDestinationSystem()))
          .findFirst()
          .ifPresentOrElse(
              targetSystemPlugin -> {
                logger.debug("Selected {}", targetSystemPlugin.getClass());
                final var reply =
                    targetSystemPlugin.synchronizeMonitoringUnit(
                        monitoringUnit, probesToAdd, probesToUpdate, probesToDelete);
                responseObserver.onNext(reply);
                responseObserver.onCompleted();
              },
              () -> {
                logger.error(
                    "{} target system not supported: check the enabled plugins",
                    monitoringUnit.getDestinationSystem());
                generateFailure(responseObserver, probesToAdd, probesToUpdate, probesToDelete);
              });
  }

  private void generateFailure(
      final StreamObserver<SynchronizeReply> responseObserver,
      final Map<String, ProbeConfiguration> probesToAdd,
      final Map<String, ProbeConfiguration> probesToUpdate,
      final Map<String, ProbeConfiguration> probesToDelete) {
    final var probes = probesToAdd.keySet();
    probes.addAll(probesToUpdate.keySet());
    probes.addAll(probesToDelete.keySet());

    final var probeDeploymentStatuses = new HashMap<String, ProbeSyncStatus>();
    probes.forEach(p -> probeDeploymentStatuses.put(p, ProbeSyncStatus.OUT_OF_SYNC));
    final var reply =
        SynchronizeReply.newBuilder()
            .setStatus(MonitoringUnitSyncStatus.FAILURE)
            .putAllProbeDeploymentStatuses(probeDeploymentStatuses)
            .build();
    responseObserver.onNext(reply);
    responseObserver.onCompleted();
  }

  private void generateDirtyFailure(
      StreamObserver<SynchronizeReply> responseObserver,
      final Map<String, ProbeConfiguration> probesToAdd,
      final Map<String, ProbeConfiguration> probesToUpdate,
      final Map<String, ProbeConfiguration> probesToDelete) {
    final var probes = probesToAdd.keySet();
    probes.addAll(probesToUpdate.keySet());
    probes.addAll(probesToDelete.keySet());

    final var probeDeploymentStatuses = new HashMap<String, ProbeSyncStatus>();
    probes.forEach(p -> probeDeploymentStatuses.put(p, ProbeSyncStatus.DIRTY));
    final var reply =
        SynchronizeReply.newBuilder()
            .setStatus(MonitoringUnitSyncStatus.DIRTY_FAILURE)
            .putAllProbeDeploymentStatuses(probeDeploymentStatuses)
            .build();
    responseObserver.onNext(reply);
    responseObserver.onCompleted();
  }
}
