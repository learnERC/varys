package io.varys.cloudbridge.plugin.kubernetes;

import freemarker.template.Configuration;
import io.kubernetes.client.openapi.models.V1ConfigMap;
import io.kubernetes.client.openapi.models.V1DaemonSet;
import io.kubernetes.client.openapi.models.V1Deployment;
import io.kubernetes.client.openapi.models.V1Role;
import io.kubernetes.client.openapi.models.V1RoleBinding;
import io.kubernetes.client.openapi.models.V1Service;
import io.kubernetes.client.openapi.models.V1ServiceAccount;
import io.kubernetes.client.util.Yaml;
import io.varys.cloudbridge.exception.TemplateLoadingException;
import io.varys.cloudbridge.plugin.utils.TemplateLoader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

@ConditionalOnProperty(value = "plugins.kubernetes.enabled", havingValue = "true")
@Component
@Slf4j
public class KubernetesTemplateLoader extends TemplateLoader {

  private final String kubernetesTemplatesDir;

  public KubernetesTemplateLoader(
      @Value("${templatesRootDir}") String templateRootDir,
      @Value("${plugins.kubernetes.templatesDir}") String kubernetesTemplatesDir,
      Configuration templateConfiguration) {
    super(templateRootDir, templateConfiguration);
    this.kubernetesTemplatesDir = kubernetesTemplatesDir;
  }

  protected Optional<List<V1Service>> loadServices(
      final String probeId, final Map<String, ?> templateVariables)
      throws TemplateLoadingException {
    try {
      final var servicesDir =
          ResourceUtils.getFile(
              Path.of(kubernetesTemplatesDir, String.valueOf(probeId), "services").toString());
      return Optional.of(
          loadAllTemplatesAsClass(servicesDir, templateVariables, V1Service.class));
    } catch (FileNotFoundException e) {
      return Optional.empty();
    }
  }

  protected Optional<List<V1ConfigMap>> loadConfigMaps(
      final String probeId, final Map<String, ?> templateVariables)
      throws TemplateLoadingException {
    try {
      final var configMapsDir =
          ResourceUtils.getFile(
              Path.of(kubernetesTemplatesDir, String.valueOf(probeId), "config-maps").toString());
      return Optional.of(
          loadAllTemplatesAsClass(configMapsDir, templateVariables, V1ConfigMap.class));
    } catch (FileNotFoundException e) {
      return Optional.empty();
    }
  }

  protected Optional<V1DaemonSet> loadDaemonSet(
      final String probeId, final Map<String, ?> templateVariables)
      throws TemplateLoadingException {
    try {
      final var templateFile =
          ResourceUtils.getFile(
              Path.of(kubernetesTemplatesDir, String.valueOf(probeId), "daemon-set.yml")
                  .toString());
      logger.trace("Loading DaemonSet for probe {}", probeId);
      return Optional.of(
          loadTemplateAsClass(templateFile.toPath(), templateVariables, V1DaemonSet.class));
    } catch (FileNotFoundException e) {
      logger.trace("DaemonSet for probe {} not found", probeId);
      return Optional.empty();
    }
  }

  private Optional<V1Deployment> loadDeployment(
      final String probeId, final Map<String, ?> templateVariables) {
    try {
      final var templateFile =
          ResourceUtils.getFile(
              Path.of(kubernetesTemplatesDir, String.valueOf(probeId), "deployment.yml")
                  .toString());
      logger.trace("Loading Deployment for probe {}", probeId);
      return Optional.of(
          loadTemplateAsClass(templateFile.toPath(), templateVariables, V1Deployment.class));
    } catch (FileNotFoundException e) {
      logger.trace("Deployment for probe {} not found", probeId);
      return Optional.empty();
    }
  }

  protected Optional<List<V1RoleBinding>> loadRoleBindings(
      final String probeId, final Map<String, ?> templateVariables) {
    try {
      final var roleBindingsDir =
          ResourceUtils.getFile(
              Path.of(kubernetesTemplatesDir, String.valueOf(probeId), "role-bindings").toString());
      logger.trace("Loading RoleBindings for probe {}", probeId);
      return Optional.of(
          loadAllTemplatesAsClass(roleBindingsDir, templateVariables, V1RoleBinding.class));
    } catch (FileNotFoundException e) {
      logger.trace("RoleBindings for probe {} not found", probeId);
      return Optional.empty();
    }
  }

  protected Optional<List<V1Role>> loadRoles(
      final String probeId, final Map<String, ?> templateVariables) {
    try {
      final var rolesDir =
          ResourceUtils.getFile(
              (Path.of(kubernetesTemplatesDir, String.valueOf(probeId), "roles").toString()));
      logger.trace("Loading Roles for probe {}", probeId);
      return Optional.of(loadAllTemplatesAsClass(rolesDir, templateVariables, V1Role.class));
    } catch (FileNotFoundException e) {
      logger.trace("Roles for probe {} not found", probeId);
      return Optional.empty();
    }
  }

  protected Optional<List<V1ServiceAccount>> loadServiceAccounts(
      final String probeId, final Map<String, ?> templateVariables) {
    try {
      final var serviceAccountsDir =
          ResourceUtils.getFile(
              Path.of(kubernetesTemplatesDir, String.valueOf(probeId), "service-accounts")
                  .toString());
      logger.trace("Loading ServiceAccounts for probe {}", probeId);
      return Optional.of(
          loadAllTemplatesAsClass(serviceAccountsDir, templateVariables, V1ServiceAccount.class));
    } catch (FileNotFoundException e) {
      logger.trace("ServiceAccounts for probe {} not found", probeId);
      return Optional.empty();
    }
  }

  private <T> List<T> loadAllTemplatesAsClass(
      final File templatesDir, final Map<String, ?> templateVariables, final Class<T> clazz)
      throws TemplateLoadingException {
    final var templateFiles = loadAllTemplatesAsFile(templatesDir, templateVariables);
    return templateFiles.stream()
        .map(template -> loadTemplateAsClass(template, clazz))
        .collect(Collectors.toUnmodifiableList());
  }

  protected <T> T loadTemplateAsClass(final File templateFile, final Class<T> clazz)
      throws TemplateLoadingException {
    try {
      return Yaml.loadAs(templateFile, clazz);
    } catch (IOException e) {
      logger.trace("Failed to load template {} as class {}", templateFile.getName(), clazz);
      throw new TemplateLoadingException(e);
    }
  }

  protected <T> T loadTemplateAsClass(
      final Path templatePah, final Map<String, ?> templateVariables, final Class<T> clazz)
      throws TemplateLoadingException {
    final var templateFile = loadTemplateAsFile(templatePah, templateVariables);
    try {
      return Yaml.loadAs(templateFile, clazz);
    } catch (IOException e) {
      logger.trace("Failed to load template {} as class {}", templateFile.getName(), clazz);
      throw new TemplateLoadingException(e);
    }
  }
}
