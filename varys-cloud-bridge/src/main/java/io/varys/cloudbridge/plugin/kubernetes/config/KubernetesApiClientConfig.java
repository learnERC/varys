package io.varys.cloudbridge.plugin.kubernetes.config;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.util.ClientBuilder;
import io.kubernetes.client.util.KubeConfig;
import java.io.IOException;
import java.io.InputStreamReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@ConditionalOnProperty(value = "plugins.kubernetes.enabled", havingValue = "true")
@Configuration
public class KubernetesApiClientConfig {

  @Bean
  public ApiClient kubernetesApiClient(
      @Value("${plugins.kubernetes.kubeconfig}") Resource kubeConfigResource) throws IOException {
    ApiClient client =
        ClientBuilder.kubeconfig(
                KubeConfig.loadKubeConfig(
                    new InputStreamReader(kubeConfigResource.getInputStream())))
            .build();

    io.kubernetes.client.openapi.Configuration.setDefaultApiClient(client);

    return client;
  }
}
