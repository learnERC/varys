package io.varys.cloudbridge.plugin.azure;

import com.microsoft.azure.management.Azure;
import com.microsoft.azure.management.compute.KnownLinuxVirtualMachineImage;
import com.microsoft.azure.management.compute.VirtualMachine;
import com.microsoft.azure.management.compute.VirtualMachineSizeTypes;
import com.microsoft.azure.management.resources.fluentcore.arm.Region;
import io.varys.cloudbridge.exception.TemplateLoadingException;
import io.varys.cloudbridge.exception.UnsupportedPatternException;
import io.varys.cloudbridge.grpc.message.MonitoringUnit;
import io.varys.cloudbridge.grpc.message.MonitoringUnitSyncStatus;
import io.varys.cloudbridge.grpc.message.Pattern;
import io.varys.cloudbridge.grpc.message.ProbeConfiguration;
import io.varys.cloudbridge.grpc.message.ProbeSyncStatus;
import io.varys.cloudbridge.grpc.service.SynchronizeReply;
import io.varys.cloudbridge.plugin.TargetSystemPlugin;
import io.varys.cloudbridge.plugin.azure.config.AzurePluginConfig;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

@ConditionalOnProperty(value = "plugins.azure.enabled", havingValue = "true")
@Service
@Slf4j
public class AzurePlugin implements TargetSystemPlugin {

  private final AzurePluginConfig azurePluginConfig;
  private final AzureTemplateLoader azureTemplateLoader;
  private final Azure azureApi;
  private final String sshPubKey;
  private final String sshKey;
  private final String sshKeyPath;
  @Value("${dataOutput.host}")
  private String dataOutputHost;

  public AzurePlugin(
      AzurePluginConfig azurePluginConfig, AzureTemplateLoader azureTemplateLoader, Azure azureApi)
      throws IOException {
    this.azurePluginConfig = azurePluginConfig;
    this.azureTemplateLoader = azureTemplateLoader;
    this.azureApi = azureApi;
    this.sshPubKey =
        new String(Files.readAllBytes(azurePluginConfig.getSshPubKeyPath().getFile().toPath()));
    this.sshKey =
        new String(Files.readAllBytes(azurePluginConfig.getSshKeyPath().getFile().toPath()));
    this.sshKeyPath = azurePluginConfig.getSshKeyPath().getFile().getPath();
  }

  @Override
  public String getTargetSystem() {
    return azurePluginConfig.getTargetSystemId();
  }

  @Override
  public SynchronizeReply synchronizeMonitoringUnit(
      MonitoringUnit monitoringUnit,
      Map<String, ProbeConfiguration> probesToAdd,
      Map<String, ProbeConfiguration> probesToUpdate,
      Map<String, ProbeConfiguration> probesToDelete) {

    final var pattern = monitoringUnit.getPattern();
    if (!isPatternSupported(pattern)) {
      throw new UnsupportedPatternException(
          String.format(
              "Pattern %s is not supported on %s",
              pattern.name(), azurePluginConfig.getTargetSystemId()));
    }

    Map<String, ProbeSyncStatus> probeDeploymentStatusMap = Map.of();
    if (probesToAdd.isEmpty() && probesToUpdate.isEmpty() && !probesToDelete.isEmpty()) {
      probeDeploymentStatusMap = deleteMonitoringUnit(monitoringUnit, probesToDelete);
    } else if (!probesToAdd.isEmpty() && probesToUpdate.isEmpty() && probesToDelete.isEmpty()) {
      probeDeploymentStatusMap = createMonitoringUnit(monitoringUnit, probesToAdd);
    } else if(!probesToDelete.isEmpty() || !probesToAdd.isEmpty() || !probesToUpdate.isEmpty()) {
      probeDeploymentStatusMap =
          updateMonitoringUnit(monitoringUnit, probesToAdd, probesToUpdate, probesToDelete);
    }

    if (probeDeploymentStatusMap.values().stream()
        .allMatch(Predicate.isEqual(ProbeSyncStatus.SYNCED))) {
      return SynchronizeReply.newBuilder()
          .setStatus(MonitoringUnitSyncStatus.SUCCESS)
          .putAllProbeDeploymentStatuses(probeDeploymentStatusMap)
          .build();
    }
    if (probeDeploymentStatusMap.values().stream()
        .anyMatch(Predicate.isEqual(ProbeSyncStatus.DIRTY))) {
      return SynchronizeReply.newBuilder()
          .setStatus(MonitoringUnitSyncStatus.DIRTY_FAILURE)
          .putAllProbeDeploymentStatuses(probeDeploymentStatusMap)
          .build();
    }

    return SynchronizeReply.newBuilder()
        .setStatus(MonitoringUnitSyncStatus.FAILURE)
        .putAllProbeDeploymentStatuses(probeDeploymentStatusMap)
        .build();
  }

  private Map<String, ProbeSyncStatus> createMonitoringUnit(
      MonitoringUnit monitoringUnit, Map<String, ProbeConfiguration> probesToAdd) {

    if (Pattern.INTERNAL_PROBES.equals(monitoringUnit.getPattern())) {
      final var monitoringUnitVm =
          azureApi
              .virtualMachines()
              .getByResourceGroup(azurePluginConfig.getResourceGroup(), monitoringUnit.getMetadataMap().get("targetSourceSystemId"));
      final var monitoringUnitVmPublicIP = monitoringUnitVm.getPrimaryPublicIPAddress().ipAddress();
      return addProbes(monitoringUnit.getId(), monitoringUnitVmPublicIP, probesToAdd);
    } else {
      final var network =
          azureApi
              .networks()
              .getByResourceGroup(
                  azurePluginConfig.getResourceGroup(), azurePluginConfig.getNetwork());
      final var monitoringUnitVm =
          azureApi
              .virtualMachines()
              .define(monitoringUnit.getId())
              .withRegion(Region.EUROPE_WEST)
              .withExistingResourceGroup(azurePluginConfig.getResourceGroup())
              .withExistingPrimaryNetwork(network)
              .withSubnet(azurePluginConfig.getSubnet())
              .withPrimaryPrivateIPAddressDynamic()
              .withNewPrimaryPublicIPAddress(String.format("varys-%s", monitoringUnit.getId()))
              .withPopularLinuxImage(KnownLinuxVirtualMachineImage.DEBIAN_10)
              .withRootUsername(azurePluginConfig.getVmRootUsername())
              .withSsh(sshPubKey)
              .withSize(VirtualMachineSizeTypes.STANDARD_B1S)
              .withTags(Map.of("manager", "varys"))
              .create();

      final var monitoringUnitVmPublicIP = monitoringUnitVm.getPrimaryPublicIPAddress().ipAddress();
      logger.trace(
          "Created MonitoringUnit {} ({}): {}", monitoringUnitVm.name(), monitoringUnitVmPublicIP, monitoringUnitVm.powerState());
      try {
        Thread.sleep(30000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      return addProbes(monitoringUnit.getId(), monitoringUnitVmPublicIP, probesToAdd);
    }
  }

  private Map<String, ProbeSyncStatus> updateMonitoringUnit(
      MonitoringUnit monitoringUnit,
      Map<String, ProbeConfiguration> probesToAdd,
      Map<String, ProbeConfiguration> probesToUpdate,
      Map<String, ProbeConfiguration> probesToDelete) {
    VirtualMachine monitoringUnitVm = null;
    if (Pattern.INTERNAL_PROBES.equals(monitoringUnit.getPattern())) {
      monitoringUnitVm =
          azureApi
              .virtualMachines()
              .getByResourceGroup(azurePluginConfig.getResourceGroup(), monitoringUnit.getMetadataMap().get("targetSourceSystemId"));
    } else {
      monitoringUnitVm =
          azureApi
              .virtualMachines()
              .getByResourceGroup(azurePluginConfig.getResourceGroup(), monitoringUnit.getId());
    }
    final var monitoringUnitVmPublicIP = monitoringUnitVm.getPrimaryPublicIPAddress().ipAddress();
    logger.trace("Get MonitoringUnit {} ({})", monitoringUnitVm.name(), monitoringUnitVmPublicIP);
    final Map<String, ProbeSyncStatus> probesStatus = new HashMap<>();

    probesStatus.putAll(addProbes(monitoringUnit.getId(), monitoringUnitVmPublicIP, probesToAdd));
    probesStatus.putAll(
        updateProbes(monitoringUnit.getId(), monitoringUnitVmPublicIP, probesToUpdate));
    probesStatus.putAll(
        deleteProbes(monitoringUnit.getId(), monitoringUnitVmPublicIP, probesToDelete));
    return probesStatus;
  }

  private Map<String, ProbeSyncStatus> deleteMonitoringUnit(
      MonitoringUnit monitoringUnit, Map<String, ProbeConfiguration> probesToDelete) {

    if (Pattern.INTERNAL_PROBES.equals(monitoringUnit.getPattern())) {
      final var monitoringUnitVm =
          azureApi
              .virtualMachines()
              .getByResourceGroup(azurePluginConfig.getResourceGroup(), monitoringUnit.getMetadataMap().get("targetSourceSystemId"));
      final var monitoringUnitVmPublicIP = monitoringUnitVm.getPrimaryPublicIPAddress().ipAddress();
      return deleteProbes(monitoringUnit.getId(), monitoringUnitVmPublicIP, probesToDelete);
    }

    final var monitoringUnitVm =
        azureApi
            .virtualMachines()
            .getByResourceGroup(azurePluginConfig.getResourceGroup(), monitoringUnit.getId());

    final var monitoringUnitVmNetwork = monitoringUnitVm.getPrimaryNetworkInterface();
    final var monitoringUnitVmPublicIP = monitoringUnitVm.getPrimaryPublicIPAddress();

    azureApi.virtualMachines().deleteById(monitoringUnitVm.id());
    azureApi.networkInterfaces().deleteById(monitoringUnitVmNetwork.id());
    azureApi.publicIPAddresses().deleteById(monitoringUnitVmPublicIP.id());
    azureApi.disks().deleteById(monitoringUnitVm.osDiskId());

    final Map<String, ProbeSyncStatus> probesStatus = new HashMap<>();
    probesToDelete.keySet().forEach(probe -> probesStatus.put(probe, ProbeSyncStatus.SYNCED));
    return probesStatus;
  }

  private Map<String, ProbeSyncStatus> addProbes(
      final String monitoringUnitId,
      final String monitoringUnitVmPublicIP,
      final Map<String, ProbeConfiguration> probesToAdd) {
    final Map<String, ProbeSyncStatus> probesStatus = new HashMap<>();
    probesToAdd.forEach(
        (probe, probeConfiguration) -> {
          final var envTemplateVariables =
              Map.of(
                  "dataOutputHost", dataOutputHost,
                  "monitoringUnitId", monitoringUnitId,
                  "monitoringUnitPublicIP", monitoringUnitVmPublicIP,
                  "username", azurePluginConfig.getVmRootUsername(),
                  "sshKey", sshKey,
                  "sshKeyPath", sshKeyPath,
                  "probes", probesToAdd);
          final var inventoryTemplateVariables =
              Map.of("hostnames", List.of(monitoringUnitVmPublicIP));
          final var projectTemplateVariables = Map.of("probeRolesToAdd", List.of(probe));
          final var probeSyncStatus =
              workProbe(
                  monitoringUnitId,
                  envTemplateVariables,
                  inventoryTemplateVariables,
                  projectTemplateVariables);
          probesStatus.put(probe, probeSyncStatus);
        });
    return probesStatus;
  }

  private Map<String, ProbeSyncStatus> deleteProbes(
      final String monitoringUnitId,
      final String monitoringUnitVmPublicIP,
      final Map<String, ProbeConfiguration> probesToDelete) {
    final Map<String, ProbeSyncStatus> probesStatus = new HashMap<>();
    probesToDelete.forEach(
        (probe, probeConfiguration) -> {
          final var envTemplateVariables =
              Map.of(
                  "dataOutputHost", dataOutputHost,
                  "monitoringUnitId", monitoringUnitId,
                  "monitoringUnitPublicIP", monitoringUnitVmPublicIP,
                  "username", azurePluginConfig.getVmRootUsername(),
                  "sshKey", sshKey,
                  "sshKeyPath", sshKeyPath,
                  "probes", probesToDelete);
          final var inventoryTemplateVariables =
              Map.of("hostnames", List.of(monitoringUnitVmPublicIP));
          final var projectTemplateVariables = Map.of("probeRolesToDelete", List.of(probe));
          final var probeSyncStatus =
              workProbe(
                  monitoringUnitId,
                  envTemplateVariables,
                  inventoryTemplateVariables,
                  projectTemplateVariables);
          probesStatus.put(probe, probeSyncStatus);
        });
    return probesStatus;
  }

  private Map<String, ProbeSyncStatus> updateProbes(
      final String monitoringUnitId,
      final String monitoringUnitVmPublicIP,
      final Map<String, ProbeConfiguration> probesToUpdate) {
    final Map<String, ProbeSyncStatus> probesStatus = new HashMap<>();
    probesToUpdate.forEach(
        (probe, probeConfiguration) -> {
          final var envTemplateVariables =
              Map.of(
                  "dataOutputHost", dataOutputHost,
                  "monitoringUnitId", monitoringUnitId,
                  "monitoringUnitPublicIP", monitoringUnitVmPublicIP,
                  "username", azurePluginConfig.getVmRootUsername(),
                  "sshKey", sshKey,
                  "sshKeyPath", sshKeyPath,
                  "probes", probesToUpdate);
          final var inventoryTemplateVariables =
              Map.of("hostnames", List.of(monitoringUnitVmPublicIP));
          final var projectTemplateVariables = Map.of("probeRolesToUpdate", List.of(probe));
          final var probeSyncStatus =
              workProbe(
                  monitoringUnitId,
                  envTemplateVariables,
                  inventoryTemplateVariables,
                  projectTemplateVariables);
          probesStatus.put(probe, probeSyncStatus);
        });
    return probesStatus;
  }

  private ProbeSyncStatus workProbe(
      String monitoringUnitId,
      Map<String, Object> envTemplateVariables,
      Map<String, List<String>> inventoryTemplateVariables,
      Map<String, List<String>> projectTemplateVariables) {
    final String ansibleRunnerInputDir;
    try {
      ansibleRunnerInputDir =
          prepareAnsibleRunner(
              monitoringUnitId,
              envTemplateVariables,
              inventoryTemplateVariables,
              projectTemplateVariables);
    } catch (TemplateLoadingException e) {
      logger.error("Failed to prepare Ansible Runner");
      return ProbeSyncStatus.OUT_OF_SYNC;
    }

    final String rolesPath;
    try {
      rolesPath =
          ResourceUtils.getFile(String.format("%s/roles", azurePluginConfig.getTemplatesDir()))
              .getPath();
    } catch (FileNotFoundException e) {
      logger.error("Failed to get Ansible roles dir: {}", e.getMessage());
      return ProbeSyncStatus.OUT_OF_SYNC;
    }
    try {
      final var exitValue = runAnsibleRunner(ansibleRunnerInputDir, rolesPath);
      if (exitValue != 0) {
        return ProbeSyncStatus.DIRTY;
      }
      return ProbeSyncStatus.SYNCED;
    } catch (IOException | InterruptedException e) {
      logger.trace(
          "Failed to execute Ansible Runner for MonitoringUnit {}: {}",
          monitoringUnitId,
          e.getMessage());
      return ProbeSyncStatus.DIRTY;
    }
  }

  private String prepareAnsibleRunner(
      String monitoringUnitId,
      Map<String, ?> envTemplateVariables,
      Map<String, ?> inventoryTemplateVariables,
      Map<String, ?> projectTemplateVariables)
      throws TemplateLoadingException {
    final var ansibleRunnerInputDir =
        azureTemplateLoader.generateAnsibleRunnerInputDir(monitoringUnitId);
    logger.trace("Generated Ansible Runner Input directory: {}", ansibleRunnerInputDir.toString());
    azureTemplateLoader.buildAnsibleRunnerEnvDir(ansibleRunnerInputDir, envTemplateVariables);
    azureTemplateLoader.buildAnsibleRunnerInventoryDir(
        ansibleRunnerInputDir, inventoryTemplateVariables);
    azureTemplateLoader.buildAnsibleRunnerProjectDir(
        ansibleRunnerInputDir, projectTemplateVariables);
    return ansibleRunnerInputDir.toString();
  }

  private int runAnsibleRunner(String ansibleRunnerInputDir, String rolesPath)
      throws IOException, InterruptedException {
    final var processBuilder =
        new ProcessBuilder(
            "/usr/local/bin/ansible-runner",
            "--playbook",
            "playbook.yml",
            "--roles-path",
            rolesPath,
            "run",
            ansibleRunnerInputDir);
    logger.trace("{}", processBuilder.command());
    final var process = processBuilder.start();
    final var reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
    String line;
    while ((line = reader.readLine()) != null) {
      logger.trace(line);
    }
    process.waitFor(5L, TimeUnit.SECONDS);
    return process.exitValue();
  }

  private boolean isPatternSupported(Pattern pattern) {
    switch (pattern) {
      case DEFAULT_PATTERN:
      case UNRECOGNIZED:
      case SHARED_SIDECAR_SINGLE_PROBE:
      case RESERVED_SIDECAR_SINGLE_PROBE:
      case SHARED_GLOBAL_UNIT_SINGLE_PROBE:
      case RESERVED_GLOBAL_UNIT_SINGLE_PROBE:
        return false;
      default:
        return true;
    }
  }
}
