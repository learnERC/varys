package io.varys.cloudbridge.plugin;

import io.varys.cloudbridge.grpc.message.MonitoringUnit;
import io.varys.cloudbridge.grpc.message.ProbeConfiguration;
import io.varys.cloudbridge.grpc.service.SynchronizeReply;
import java.util.Map;

public interface TargetSystemPlugin {

  String getTargetSystem();

  SynchronizeReply synchronizeMonitoringUnit(
      MonitoringUnit monitoringUnit,
      Map<String, ProbeConfiguration> probesToAdd,
      Map<String, ProbeConfiguration> probesToUpdate,
      Map<String, ProbeConfiguration> probesToDelete);
}
