package io.varys.cloudbridge.plugin.azure.config;

import com.microsoft.azure.management.Azure;
import com.microsoft.rest.LogLevel;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@ConditionalOnProperty(value = "plugins.azure.enabled", havingValue = "true")
@Configuration
public class AzureApiClientConfig {

  @Bean
  public Azure azureApiClient(@Value("${plugins.azure.azureAuthPath}") Resource azureAuthResource)
      throws IOException {
    return Azure.configure()
        .withLogLevel(LogLevel.BASIC)
        .authenticate(azureAuthResource.getFile())
        .withDefaultSubscription();
  }
}
