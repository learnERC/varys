package io.varys.cloudbridge.plugin.kubernetes;

import io.kubernetes.client.openapi.ApiException;
import io.varys.cloudbridge.exception.TemplateLoadingException;
import io.varys.cloudbridge.exception.UnsupportedPatternException;
import io.varys.cloudbridge.grpc.message.MonitoringUnit;
import io.varys.cloudbridge.grpc.message.MonitoringUnitSyncStatus;
import io.varys.cloudbridge.grpc.message.Pattern;
import io.varys.cloudbridge.grpc.message.ProbeConfiguration;
import io.varys.cloudbridge.grpc.message.ProbeSyncStatus;
import io.varys.cloudbridge.grpc.service.SynchronizeReply;
import io.varys.cloudbridge.plugin.TargetSystemPlugin;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@ConditionalOnProperty(value = "plugins.kubernetes.enabled", havingValue = "true")
@Service
@Slf4j
public class KubernetesPlugin implements TargetSystemPlugin {

  private final String targetSystem;
  @Value("${dataOutput.host}")
  private String dataOutputHost;
  private final KubernetesTemplateLoader kubernetesTemplateLoader;
  private final KubernetesAPIWrapper kubernetesAPIWrapper;

  public KubernetesPlugin(
      @Value("${plugins.kubernetes.targetSystemId}") String targetSystem,
      KubernetesTemplateLoader kubernetesTemplateLoader,
      KubernetesAPIWrapper kubernetesAPIWrapper) {
    this.targetSystem = targetSystem;
    this.kubernetesTemplateLoader = kubernetesTemplateLoader;
    this.kubernetesAPIWrapper = kubernetesAPIWrapper;
  }

  @Override
  public String getTargetSystem() {
    return this.targetSystem;
  }

  @Override
  public SynchronizeReply synchronizeMonitoringUnit(
      final MonitoringUnit monitoringUnit,
      final Map<String, ProbeConfiguration> probesToAdd,
      final Map<String, ProbeConfiguration> probesToUpdate,
      final Map<String, ProbeConfiguration> probesToDelete) {

    final var pattern = monitoringUnit.getPattern();
    if (!isPatternSupported(pattern)) {
      throw new UnsupportedPatternException(
          String.format("Pattern %s is not supported on %s", pattern.name(), targetSystem));
    }

    /*
     * !!! Only single probe pattern are supported !!!
     * Consequently, we check only one map has entries to perform more specific CUD operations.
     */
    Map<String, ProbeSyncStatus> probeDeploymentStatusMap = Map.of();
    if (probesToAdd.isEmpty() && probesToUpdate.isEmpty() && !probesToDelete.isEmpty()) {
      probeDeploymentStatusMap = deleteMonitoringUnit(monitoringUnit, probesToDelete);
    } else if (!probesToAdd.isEmpty() && probesToUpdate.isEmpty() && probesToDelete.isEmpty()) {
      probeDeploymentStatusMap = createMonitoringUnit(monitoringUnit, probesToAdd);
    } else if (!probesToUpdate.isEmpty() && probesToAdd.isEmpty() && probesToDelete.isEmpty()) {
      probeDeploymentStatusMap = updateMonitoringUnit(monitoringUnit, probesToUpdate);
    }

    if (probeDeploymentStatusMap.values().stream()
        .allMatch(Predicate.isEqual(ProbeSyncStatus.SYNCED))) {
      return SynchronizeReply.newBuilder()
          .setStatus(MonitoringUnitSyncStatus.SUCCESS)
          .putAllProbeDeploymentStatuses(probeDeploymentStatusMap)
          .build();
    }
    if (probeDeploymentStatusMap.values().stream()
        .anyMatch(Predicate.isEqual(ProbeSyncStatus.DIRTY))) {
      return SynchronizeReply.newBuilder()
          .setStatus(MonitoringUnitSyncStatus.DIRTY_FAILURE)
          .putAllProbeDeploymentStatuses(probeDeploymentStatusMap)
          .build();
    }

    return SynchronizeReply.newBuilder()
        .setStatus(MonitoringUnitSyncStatus.FAILURE)
        .putAllProbeDeploymentStatuses(probeDeploymentStatusMap)
        .build();
  }

  private boolean isPatternSupported(Pattern pattern) {
    switch (pattern) {
      case DEFAULT_PATTERN:
      case UNRECOGNIZED:
      case INTERNAL_PROBES:
      case SHARED_SIDECAR_MULTI_PROBE:
      case RESERVED_SIDECAR_MULTI_PROBE:
      case SHARED_GLOBAL_UNIT_MULTI_PROBE:
      case RESERVED_GLOBAL_UNIT_MULTI_PROBE:
        return false;
      default:
        return true;
    }
  }

  private Map<String, ProbeSyncStatus> createMonitoringUnit(
      final MonitoringUnit monitoringUnit, final Map<String, ProbeConfiguration> probesToAdd) {
    logger.info("Creating MonitoringUnit {}", monitoringUnit.getId());
    return probesToAdd.entrySet().stream()
        .collect(
            Collectors.toMap(
                Entry::getKey,
                entry -> {
                  final var probeId = entry.getKey();
                  final var probeConfiguration = entry.getValue();
                  final var templateVariables =
                      Map.of(
                          "dataOutputHost",
                          dataOutputHost,
                          "monitoringUnit",
                          monitoringUnit,
                          "probeId",
                          probeId,
                          "probeConfiguration",
                          probeConfiguration);
                  try {
                    final var serviceAccounts =
                        kubernetesTemplateLoader.loadServiceAccounts(probeId, templateVariables);
                    if (serviceAccounts.isPresent()) {
                      try {
                        kubernetesAPIWrapper.createServiceAccounts(serviceAccounts.get());
                      } catch (ApiException e) {
                        // TODO: handle different errors
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error(
                        "An error occurred loading ServiceAccounts templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var roles =
                        kubernetesTemplateLoader.loadRoles(probeId, templateVariables);
                    if (roles.isPresent()) {
                      try {
                        kubernetesAPIWrapper.createRoles(roles.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading Roles templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var roleBindings =
                        kubernetesTemplateLoader.loadRoleBindings(probeId, templateVariables);
                    if (roleBindings.isPresent()) {
                      try {
                        kubernetesAPIWrapper.createRoleBindings(roleBindings.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading RoleBindings templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var configMaps =
                        kubernetesTemplateLoader.loadConfigMaps(probeId, templateVariables);
                    if (configMaps.isPresent()) {
                      try {
                        kubernetesAPIWrapper.createConfigMaps(configMaps.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading ConfigMaps templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var daemonSet =
                        kubernetesTemplateLoader.loadDaemonSet(probeId, templateVariables);
                    if (daemonSet.isPresent()) {
                      try {
                        kubernetesAPIWrapper.createDaemonSet(daemonSet.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading DaemonSet template: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var services =
                        kubernetesTemplateLoader.loadServices(probeId, templateVariables);
                    if (services.isPresent()) {
                      try {
                        kubernetesAPIWrapper.createServices(services.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading Services templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  return ProbeSyncStatus.SYNCED;
                }));
  }

  private Map<String, ProbeSyncStatus> updateMonitoringUnit(
      final MonitoringUnit monitoringUnit, final Map<String, ProbeConfiguration> probesToUpdate) {
    logger.info("Updating MonitoringUnit {}", monitoringUnit.getId());
    return probesToUpdate.entrySet().stream()
        .collect(
            Collectors.toMap(
                entry -> entry.getKey(),
                entry -> {
                  final var probeId = entry.getKey();
                  final var probeConfiguration = entry.getValue();
                  final var templateVariables =
                      Map.of(
                          "dataOutputHost",
                          dataOutputHost,
                          "monitoringUnit",
                          monitoringUnit,
                          "probeId",
                          probeId,
                          "probeConfiguration",
                          probeConfiguration);
                  try {
                    final var serviceAccounts =
                        kubernetesTemplateLoader.loadServiceAccounts(probeId, templateVariables);
                    if (serviceAccounts.isPresent()) {
                      try {
                        kubernetesAPIWrapper.updateServiceAccounts(serviceAccounts.get());
                      } catch (ApiException e) {
                        // TODO: handle different errors
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error(
                        "An error occurred loading ServiceAccounts templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var roles =
                        kubernetesTemplateLoader.loadRoles(probeId, templateVariables);
                    if (roles.isPresent()) {
                      try {
                        kubernetesAPIWrapper.updateRoles(roles.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading Roles templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var roleBindings =
                        kubernetesTemplateLoader.loadRoleBindings(probeId, templateVariables);
                    if (roleBindings.isPresent()) {
                      try {
                        kubernetesAPIWrapper.updateRoleBindings(roleBindings.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading RoleBindings templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var configMaps =
                        kubernetesTemplateLoader.loadConfigMaps(probeId, templateVariables);
                    if (configMaps.isPresent()) {
                      try {
                        kubernetesAPIWrapper.updateConfigMaps(configMaps.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading ConfigMaps templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var daemonSet =
                        kubernetesTemplateLoader.loadDaemonSet(probeId, templateVariables);
                    if (daemonSet.isPresent()) {
                      try {
                        kubernetesAPIWrapper.updateDaemonSet(daemonSet.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading DaemonSet template: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var services =
                        kubernetesTemplateLoader.loadServices(probeId, templateVariables);
                    if (services.isPresent()) {
                      try {
                        kubernetesAPIWrapper.updateServices(services.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading Services templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  return ProbeSyncStatus.SYNCED;
                }));
  }

  private Map<String, ProbeSyncStatus> deleteMonitoringUnit(
      final MonitoringUnit monitoringUnit, final Map<String, ProbeConfiguration> probesToDelete) {
    logger.info("Deleting MonitoringUnit {}", monitoringUnit.getId());
    return probesToDelete.entrySet().stream()
        .collect(
            Collectors.toMap(
                entry -> entry.getKey(),
                entry -> {
                  final var probeId = entry.getKey();
                  final var probeConfiguration = entry.getValue();
                  final var templateVariables =
                      Map.of(
                          "dataOutputHost",
                          dataOutputHost,
                          "monitoringUnit",
                          monitoringUnit,
                          "probeId",
                          probeId,
                          "probeConfiguration",
                          probeConfiguration);
                  try {
                    final var services =
                        kubernetesTemplateLoader.loadServices(probeId, templateVariables);
                    if (services.isPresent()) {
                      try {
                        kubernetesAPIWrapper.deleteServices(services.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading Services templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var serviceAccounts =
                        kubernetesTemplateLoader.loadServiceAccounts(probeId, templateVariables);
                    if (serviceAccounts.isPresent()) {
                      try {
                        kubernetesAPIWrapper.deleteServiceAccounts(serviceAccounts.get());
                      } catch (ApiException e) {
                        // TODO: handle different errors
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error(
                        "An error occurred loading ServiceAccounts templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var roles =
                        kubernetesTemplateLoader.loadRoles(probeId, templateVariables);
                    if (roles.isPresent()) {
                      try {
                        kubernetesAPIWrapper.deleteRoles(roles.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading Roles templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var roleBindings =
                        kubernetesTemplateLoader.loadRoleBindings(probeId, templateVariables);
                    if (roleBindings.isPresent()) {
                      try {
                        kubernetesAPIWrapper.deleteRoleBindings(roleBindings.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading RoleBindings templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var configMaps =
                        kubernetesTemplateLoader.loadConfigMaps(probeId, templateVariables);
                    if (configMaps.isPresent()) {
                      try {
                        kubernetesAPIWrapper.deleteConfigMaps(configMaps.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading ConfigMaps templates: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  try {
                    final var daemonSet =
                        kubernetesTemplateLoader.loadDaemonSet(probeId, templateVariables);
                    if (daemonSet.isPresent()) {
                      try {
                        kubernetesAPIWrapper.deleteDaemonSet(daemonSet.get());
                      } catch (ApiException e) {
                        return ProbeSyncStatus.DIRTY;
                      }
                    }
                  } catch (TemplateLoadingException e) {
                    logger.error("An error occurred loading DaemonSet template: {}", e.getMessage());
                    return ProbeSyncStatus.OUT_OF_SYNC;
                  }

                  return ProbeSyncStatus.SYNCED;
                }));
  }
}
