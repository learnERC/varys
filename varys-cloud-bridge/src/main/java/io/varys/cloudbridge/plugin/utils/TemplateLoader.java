package io.varys.cloudbridge.plugin.utils;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.varys.cloudbridge.exception.TemplateLoadingException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ResourceUtils;

@Slf4j
public class TemplateLoader {

  private final String templatesRootDir;
  private final Configuration templateConfiguration;

  public TemplateLoader(String templatesRootDir, Configuration templateConfiguration) {
    this.templatesRootDir = templatesRootDir;
    this.templateConfiguration = templateConfiguration;
  }

  public List<File> loadAllTemplatesAsFile(
      final File templatesDir, final Map<String, ?> templateVariables)
      throws TemplateLoadingException {
    try {
      final Stream<Path> templates = Files.list(templatesDir.toPath());
      return templates
          .filter(Files::isRegularFile)
          .map(path -> loadTemplateAsFile(path, templateVariables))
          .collect(Collectors.toUnmodifiableList());
    } catch (IOException e) {
      logger.trace("Failed to load templates from {}", templatesDir.getName());
      throw new TemplateLoadingException(e);
    }
  }

  public File loadTemplateAsFile(final Path templatePath, final Map<String, ?> templateVariables)
      throws TemplateLoadingException {
    final Template template;
    final File tempFile;
    try {
      final var templateRelativePath =
          Path.of(ResourceUtils.getFile(templatesRootDir).getPath()).relativize(templatePath);
      template = templateConfiguration.getTemplate(templateRelativePath.toString());
      logger.trace("Got template {}", template.getName());
      tempFile = File.createTempFile("tmpl-", ".yml");
      try (var fileWriter = new FileWriter(tempFile)) {
        template.process(templateVariables, fileWriter);
        logger.trace("Processed template {}", template.getName());
      }
      return tempFile;
    } catch (IOException | TemplateException e) {
      logger.trace("Failed to load template {} as file", templatePath);
      throw new TemplateLoadingException(e);
    }
  }
}
