package io.varys.cloudbridge.plugin.azure.config;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@ConditionalOnProperty(value = "plugins.azure.enabled", havingValue = "true")
@Component
@ConfigurationProperties(prefix = "plugins.azure")
@Data
public class AzurePluginConfig {

  private boolean enabled;
  private Resource azureAuthPath;
  private String templatesDir;
  private String targetSystemId;
  private String resourceGroup;
  private String network;
  private String subnet;
  private String vmRootUsername;
  private Resource sshPubKeyPath;
  private Resource sshKeyPath;
}
