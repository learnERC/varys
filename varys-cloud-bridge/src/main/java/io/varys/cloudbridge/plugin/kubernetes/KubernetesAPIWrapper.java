package io.varys.cloudbridge.plugin.kubernetes;

import io.kubernetes.client.custom.V1Patch;
import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.apis.AppsV1Api;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.apis.RbacAuthorizationV1Api;
import io.kubernetes.client.openapi.models.V1ConfigMap;
import io.kubernetes.client.openapi.models.V1DaemonSet;
import io.kubernetes.client.openapi.models.V1Role;
import io.kubernetes.client.openapi.models.V1RoleBinding;
import io.kubernetes.client.openapi.models.V1Service;
import io.kubernetes.client.openapi.models.V1ServiceAccount;
import io.kubernetes.client.util.PatchUtils;
import io.kubernetes.client.util.Yaml;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@ConditionalOnProperty(value = "plugins.kubernetes.enabled", havingValue = "true")
@Service
@Slf4j
public class KubernetesAPIWrapper {
  private static final String FIELD_MANAGER = "varys.io/cloud-bridge";
  private final AppsV1Api appsV1Api;
  private final CoreV1Api coreV1Api;
  private final RbacAuthorizationV1Api rbacAuthorizationV1Api;

  public KubernetesAPIWrapper(ApiClient apiClient) {
    this.appsV1Api = new AppsV1Api(apiClient);
    this.coreV1Api = new CoreV1Api(apiClient);
    this.rbacAuthorizationV1Api = new RbacAuthorizationV1Api();
  }

  protected void deleteDaemonSet(V1DaemonSet v1DaemonSet) throws ApiException {
    logger.debug("Deleting DaemonSet {}", v1DaemonSet.getMetadata().getName());
    logger.trace("{}", v1DaemonSet.toString());
    try {
      appsV1Api.deleteNamespacedDaemonSet(
          v1DaemonSet.getMetadata().getName(),
          v1DaemonSet.getMetadata().getNamespace(),
          null,
          null,
          null,
          null,
          null,
          null);
    } catch (ApiException e) {
      if (e.getCode() == 404) {
        logger.warn(
            "DaemonSet {} not found during deletion process", v1DaemonSet.getMetadata().getName());
      } else {
        logger.error(
            "A {} error occurred deleting DaemonSet {}: {}",
            e.getCode(),
            v1DaemonSet.getMetadata().getName(),
            e.getResponseBody());
        throw e;
      }
    }
  }

  protected void deleteRoleBindings(List<V1RoleBinding> v1RoleBindings) throws ApiException {
    for (V1RoleBinding v1RoleBinding : v1RoleBindings) {
      deleteRoleBinding(v1RoleBinding);
    }
  }

  protected void deleteRoleBinding(V1RoleBinding v1RoleBinding) throws ApiException {
    logger.debug("Deleting RoleBinding {}", v1RoleBinding.getMetadata().getName());
    logger.trace("{}", v1RoleBinding.toString());
    try {
      rbacAuthorizationV1Api.deleteNamespacedRoleBinding(
          v1RoleBinding.getMetadata().getName(),
          v1RoleBinding.getMetadata().getNamespace(),
          null,
          null,
          null,
          null,
          null,
          null);
    } catch (ApiException e) {
      if (e.getCode() == 404) {
        logger.warn(
            "RoleBinding {} not found during deletion process",
            v1RoleBinding.getMetadata().getName());
      } else {
        logger.error(
            "A {} error occurred deleting RoleBinding {}: {}",
            e.getCode(),
            v1RoleBinding.getMetadata().getName(),
            e.getResponseBody());
        throw e;
      }
    }
  }

  protected void deleteRoles(List<V1Role> v1Roles) throws ApiException {
    for (V1Role v1Role : v1Roles) {
      deleteRole(v1Role);
    }
  }

  protected void deleteRole(V1Role v1Role) throws ApiException {
    logger.debug("Deleting Role {}", v1Role.getMetadata().getName());
    logger.trace("{}", v1Role.toString());
    try {
      rbacAuthorizationV1Api.deleteNamespacedRole(
          v1Role.getMetadata().getName(),
          v1Role.getMetadata().getNamespace(),
          null,
          null,
          null,
          null,
          null,
          null);
    } catch (ApiException e) {
      if (e.getCode() == 404) {
        logger.warn("Role {} not found during deletion process", v1Role.getMetadata().getName());
      } else {
        logger.error(
            "A {} error occurred deleting Role {}: {}",
            e.getCode(),
            v1Role.getMetadata().getName(),
            e.getResponseBody());
        throw e;
      }
    }
  }

  protected void deleteConfigMaps(List<V1ConfigMap> configMaps) throws ApiException {
    for (V1ConfigMap v1ConfigMap : configMaps) {
      deleteConfigMap(v1ConfigMap);
    }
  }

  protected void deleteConfigMap(V1ConfigMap v1ConfigMap) throws ApiException {
    logger.debug("Deleting ConfigMap {}", v1ConfigMap.getMetadata().getName());
    logger.trace("{}", v1ConfigMap.toString());
    try {
      coreV1Api.deleteNamespacedConfigMap(
          v1ConfigMap.getMetadata().getName(),
          v1ConfigMap.getMetadata().getNamespace(),
          null,
          null,
          null,
          null,
          null,
          null);
    } catch (ApiException e) {
      if (e.getCode() == 404) {
        logger.warn(
            "ConfigMap {} not found during deletion process", v1ConfigMap.getMetadata().getName());
      } else {
        logger.error(
            "A {} error occurred deleting ConfigMap {}: {}",
            e.getCode(),
            v1ConfigMap.getMetadata().getName(),
            e.getResponseBody());
        throw e;
      }
    }
  }

  protected void deleteServices(List<V1Service> services) throws ApiException {
    for (V1Service v1Service : services) {
      deleteService(v1Service);
    }
  }

  protected void deleteService(V1Service v1Service) throws ApiException {
    logger.debug("Deleting Service {}", v1Service.getMetadata().getName());
    logger.trace("{}", v1Service.toString());
    try {
      coreV1Api.deleteNamespacedService(
          v1Service.getMetadata().getName(),
          v1Service.getMetadata().getNamespace(),
          null,
          null,
          null,
          null,
          null,
          null);
    } catch (ApiException e) {
      if (e.getCode() == 404) {
        logger.warn(
            "Service {} not found during deletion process", v1Service.getMetadata().getName());
      } else {
        logger.error(
            "A {} error occurred deleting Service {}: {}",
            e.getCode(),
            v1Service.getMetadata().getName(),
            e.getResponseBody());
        throw e;
      }
    }
  }

  protected void deleteServiceAccounts(List<V1ServiceAccount> serviceAccounts) throws ApiException {
    for (V1ServiceAccount v1ServiceAccount : serviceAccounts) {
      deleteServiceAccount(v1ServiceAccount);
    }
  }

  protected void deleteServiceAccount(V1ServiceAccount v1ServiceAccount) throws ApiException {
    logger.debug("Deleting ServiceAccount {}", v1ServiceAccount.getMetadata().getName());
    logger.trace("{}", v1ServiceAccount.toString());
    try {
      coreV1Api.deleteNamespacedServiceAccount(
          v1ServiceAccount.getMetadata().getName(),
          v1ServiceAccount.getMetadata().getNamespace(),
          null,
          null,
          null,
          null,
          null,
          null);
    } catch (ApiException e) {
      if (e.getCode() == 404) {
        logger.warn(
            "ServiceAccount {} not found during deletion process",
            v1ServiceAccount.getMetadata().getName());
      } else {
        logger.error(
            "A {} error occurred deleting ServiceAccount {}: {}",
            e.getCode(),
            v1ServiceAccount.getMetadata().getName(),
            e.getResponseBody());
        throw e;
      }
    }
  }

  protected void createServiceAccounts(List<V1ServiceAccount> serviceAccounts) throws ApiException {
    for (V1ServiceAccount v1ServiceAccount : serviceAccounts) {
      createServiceAccount(v1ServiceAccount);
    }
  }

  protected void createServiceAccount(V1ServiceAccount v1ServiceAccount) throws ApiException {
    logger.debug("Creating ServiceAccount {}", v1ServiceAccount.getMetadata().getName());
    logger.trace("{}", v1ServiceAccount.toString());
    try {
      coreV1Api.createNamespacedServiceAccount(
          v1ServiceAccount.getMetadata().getNamespace(),
          v1ServiceAccount,
          null,
          null,
          FIELD_MANAGER);
    } catch (ApiException e) {
      logger.error(
          "A {} error occurred creating ServiceAccount {}: {}",
          e.getCode(),
          v1ServiceAccount.getMetadata().getName(),
          e.getResponseBody());
      logger.trace(e.getMessage());
      throw e;
    }
  }

  protected void createRoles(List<V1Role> roles) throws ApiException {
    for (V1Role v1Role : roles) {
      createRole(v1Role);
    }
  }

  protected void createRole(V1Role v1Role) throws ApiException {
    logger.debug("Creating Role {}", v1Role.getMetadata().getName());
    logger.trace("{}", v1Role.toString());
    try {
      rbacAuthorizationV1Api.createNamespacedRole(
          v1Role.getMetadata().getNamespace(), v1Role, null, null, FIELD_MANAGER);
    } catch (ApiException e) {
      logger.error(
          "A {} error occurred creating Role {}: {}",
          e.getCode(),
          v1Role.getMetadata().getName(),
          e.getResponseBody());
      throw e;
    }
  }

  protected void createRoleBindings(List<V1RoleBinding> roleBindings) throws ApiException {
    for (V1RoleBinding v1RoleBinding : roleBindings) {
      createRoleBinding(v1RoleBinding);
    }
  }

  protected void createRoleBinding(V1RoleBinding v1RoleBinding) throws ApiException {
    logger.debug("Creating RoleBinding {}", v1RoleBinding.getMetadata().getName());
    logger.trace("{}", v1RoleBinding.toString());
    try {
      rbacAuthorizationV1Api.createNamespacedRoleBinding(
          v1RoleBinding.getMetadata().getNamespace(), v1RoleBinding, null, null, FIELD_MANAGER);
    } catch (ApiException e) {
      logger.error(
          "A {} error occurred creating RoleBinding {}: {}",
          e.getCode(),
          v1RoleBinding.getMetadata().getName(),
          e.getResponseBody());
      throw e;
    }
  }

  protected void createConfigMaps(List<V1ConfigMap> configMaps) throws ApiException {
    for (V1ConfigMap v1ConfigMap : configMaps) {
      createConfigMap(v1ConfigMap);
    }
  }

  private void createConfigMap(V1ConfigMap v1ConfigMap) throws ApiException {
    logger.debug("Creating ConfigMap {}", v1ConfigMap.getMetadata().getName());
    logger.trace("{}", v1ConfigMap.toString());
    try {
      coreV1Api.createNamespacedConfigMap(
          v1ConfigMap.getMetadata().getNamespace(), v1ConfigMap, null, null, FIELD_MANAGER);
    } catch (ApiException e) {
      logger.error(
          "A {} error occurred creating ConfigMap {}: {}",
          e.getCode(),
          v1ConfigMap.getMetadata().getName(),
          e.getResponseBody());
      throw e;
    }
  }

  protected void createServices(List<V1Service> services) throws ApiException {
    for (V1Service v1Service : services) {
      createService(v1Service);
    }
  }

  private void createService(V1Service v1Service) throws ApiException {
    logger.debug("Creating Service {}", v1Service.getMetadata().getName());
    logger.trace("{}", v1Service.toString());
    try {
      coreV1Api.createNamespacedService(
          v1Service.getMetadata().getNamespace(), v1Service, null, null, FIELD_MANAGER);
    } catch (ApiException e) {
      logger.error(
          "A {} error occurred creating Service {}: {}",
          e.getCode(),
          v1Service.getMetadata().getName(),
          e.getResponseBody());
      throw e;
    }
  }

  protected void createDaemonSet(V1DaemonSet v1DaemonSet) throws ApiException {
    logger.debug("Creating DaemonSet {}", v1DaemonSet.getMetadata().getName());
    logger.trace("{}", v1DaemonSet.toString());
    try {
      appsV1Api.createNamespacedDaemonSet(
          v1DaemonSet.getMetadata().getNamespace(), v1DaemonSet, null, null, FIELD_MANAGER);
    } catch (ApiException e) {
      logger.error(
          "A {} error occurred creating DaemonSet {}: {}",
          e.getCode(),
          v1DaemonSet.getMetadata().getName(),
          e.getResponseBody());
      throw e;
    }
  }

  protected void updateServiceAccounts(List<V1ServiceAccount> serviceAccounts) throws ApiException {
    for (V1ServiceAccount v1ServiceAccount : serviceAccounts) {
      updateServiceAccount(v1ServiceAccount);
    }
  }

  protected void updateServiceAccount(V1ServiceAccount v1ServiceAccount) throws ApiException {
    logger.debug("Updating ServiceAccount {}", v1ServiceAccount.getMetadata().getName());
    logger.trace("{}", v1ServiceAccount.toString());
    try {
      PatchUtils.patch(
          V1ServiceAccount.class,
          () ->
              coreV1Api.patchNamespacedServiceAccountCall(
                  v1ServiceAccount.getMetadata().getName(),
                  v1ServiceAccount.getMetadata().getNamespace(),
                  new V1Patch(Yaml.dump(v1ServiceAccount)),
                  null,
                  null,
                  FIELD_MANAGER,
                  true,
                  null),
          V1Patch.PATCH_FORMAT_APPLY_YAML,
          coreV1Api.getApiClient());
    } catch (ApiException e) {
      logger.error(
          "A {} error occurred updating ServiceAccount {}: {}",
          e.getCode(),
          v1ServiceAccount.getMetadata().getName(),
          e.getResponseBody());
      throw e;
    }
  }

  protected void updateRoles(List<V1Role> roles) throws ApiException {
    for (V1Role v1Role : roles) {
      updateRole(v1Role);
    }
  }

  protected void updateRole(V1Role v1Role) throws ApiException {
    logger.debug("Updating Role {}", v1Role.getMetadata().getName());
    logger.trace("{}", v1Role.toString());
    try {
      PatchUtils.patch(
          V1Role.class,
          () ->
              rbacAuthorizationV1Api.patchNamespacedRoleCall(
                  v1Role.getMetadata().getName(),
                  v1Role.getMetadata().getNamespace(),
                  new V1Patch(Yaml.dump(v1Role)),
                  null,
                  null,
                  FIELD_MANAGER,
                  true,
                  null),
          V1Patch.PATCH_FORMAT_APPLY_YAML,
          rbacAuthorizationV1Api.getApiClient());
    } catch (ApiException e) {
      logger.error(
          "An {} error occurred updating Role {}: {}",
          e.getCode(),
          v1Role.getMetadata().getName(),
          e.getResponseBody());
      throw e;
    }
  }

  protected void updateRoleBindings(List<V1RoleBinding> roleBindings) throws ApiException {
    for (V1RoleBinding v1RoleBinding : roleBindings) {
      updateRoleBinding(v1RoleBinding);
    }
  }

  protected void updateRoleBinding(V1RoleBinding v1RoleBinding) throws ApiException {
    logger.debug("Updating RoleBinding {}", v1RoleBinding.getMetadata().getName());
    logger.trace("{}", v1RoleBinding.toString());
    try {
      PatchUtils.patch(
          V1RoleBinding.class,
          () ->
              rbacAuthorizationV1Api.patchNamespacedRoleBindingCall(
                  v1RoleBinding.getMetadata().getName(),
                  v1RoleBinding.getMetadata().getNamespace(),
                  new V1Patch(Yaml.dump(v1RoleBinding)),
                  null,
                  null,
                  FIELD_MANAGER,
                  true,
                  null),
          V1Patch.PATCH_FORMAT_APPLY_YAML,
          rbacAuthorizationV1Api.getApiClient());
    } catch (ApiException e) {
      logger.error(
          "A {} error occurred updating RoleBinding {}: {}",
          e.getCode(),
          v1RoleBinding.getMetadata().getName(),
          e.getResponseBody());
      throw e;
    }
  }

  protected void updateConfigMaps(List<V1ConfigMap> configMaps) throws ApiException {
    for (V1ConfigMap v1ConfigMap : configMaps) {
      updateConfigMap(v1ConfigMap);
    }
  }

  private void updateConfigMap(V1ConfigMap v1ConfigMap) throws ApiException {
    logger.debug("Updating ConfigMap {}", v1ConfigMap.getMetadata().getName());
    logger.trace("{}", v1ConfigMap.toString());
    try {
      PatchUtils.patch(
          V1ConfigMap.class,
          () ->
              coreV1Api.patchNamespacedConfigMapCall(
                  v1ConfigMap.getMetadata().getName(),
                  v1ConfigMap.getMetadata().getNamespace(),
                  new V1Patch(Yaml.dump(v1ConfigMap)),
                  null,
                  null,
                  FIELD_MANAGER,
                  true,
                  null),
          V1Patch.PATCH_FORMAT_APPLY_YAML,
          coreV1Api.getApiClient());
    } catch (ApiException e) {
      logger.error(
          "A {} error occurred updating ConfigMap {}: {}",
          e.getCode(),
          v1ConfigMap.getMetadata().getName(),
          e.getResponseBody());
      throw e;
    }
  }

  protected void updateServices(List<V1Service> services) throws ApiException {
    for (V1Service v1Service : services) {
      updateService(v1Service);
    }
  }

  private void updateService(V1Service v1Service) throws ApiException {
    logger.debug("Updating Service {}", v1Service.getMetadata().getName());
    logger.trace("{}", v1Service.toString());
    try {
      PatchUtils.patch(
          V1Service.class,
          () ->
              coreV1Api.patchNamespacedServiceCall(
                  v1Service.getMetadata().getName(),
                  v1Service.getMetadata().getNamespace(),
                  new V1Patch(Yaml.dump(v1Service)),
                  null,
                  null,
                  FIELD_MANAGER,
                  true,
                  null),
          V1Patch.PATCH_FORMAT_APPLY_YAML,
          coreV1Api.getApiClient());
    } catch (ApiException e) {
      logger.error(
          "A {} error occurred updating Service {}: {}",
          e.getCode(),
          v1Service.getMetadata().getName(),
          e.getResponseBody());
      throw e;
    }
  }

  protected void updateDaemonSet(V1DaemonSet v1DaemonSet) throws ApiException {
    logger.debug("Updating DaemonSet {}", v1DaemonSet.getMetadata().getName());
    logger.trace("{}", v1DaemonSet.toString());
    try {
      PatchUtils.patch(
          V1DaemonSet.class,
          () ->
              appsV1Api.patchNamespacedDaemonSetCall(
                  v1DaemonSet.getMetadata().getName(),
                  v1DaemonSet.getMetadata().getNamespace(),
                  new V1Patch(Yaml.dump(v1DaemonSet)),
                  null,
                  null,
                  FIELD_MANAGER,
                  true,
                  null),
          V1Patch.PATCH_FORMAT_APPLY_YAML,
          appsV1Api.getApiClient());
    } catch (ApiException e) {
      logger.error(
          "A {} error occurred updating DaemonSet {}: {}",
          e.getCode(),
          v1DaemonSet.getMetadata().getName(),
          e.getResponseBody());
      throw e;
    }
  }
}
