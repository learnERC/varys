package io.varys.cloudbridge.plugin.azure;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import io.varys.cloudbridge.exception.TemplateLoadingException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

@ConditionalOnProperty(value = "plugins.azure.enabled", havingValue = "true")
@Component
@Slf4j
public class AzureTemplateLoader {

  private final String templatesRootDir;
  private final Configuration templateConfiguration;
  private final String azureTemplatesDir;

  public AzureTemplateLoader(
      @Value("${templatesRootDir}") String templatesRootDir,
      @Value("${plugins.azure.templatesDir}") String azureTemplatesDir,
      Configuration templateConfiguration) {
    this.templatesRootDir = templatesRootDir;
    this.templateConfiguration = templateConfiguration;
    this.azureTemplatesDir = azureTemplatesDir;
  }

  public Path generateAnsibleRunnerInputDir(String monitoringUnitId) throws TemplateLoadingException {
    try {
      final var inputDir =
          Files.createTempDirectory(String.format("ansible_runner_%s_", monitoringUnitId));
      Files.createDirectory(inputDir.resolve("env"));
      Files.createDirectory(inputDir.resolve("inventory"));
      Files.createDirectory(inputDir.resolve("project"));
      return inputDir;
    } catch (IOException e) {
      logger.error("Failed to generate Ansible runner input dir");
      throw new TemplateLoadingException(e);
    }
  }

  public void buildAnsibleRunnerEnvDir(Path ansibleRunnerInputDir, Map<String, ?> templateVariables)
      throws TemplateLoadingException {
    final var envDir = Path.of(azureTemplatesDir, "env").toString();
    final Stream<Path> envFiles;
    try {
      envFiles = Files.list(ResourceUtils.getFile(envDir).toPath());
    } catch (IOException e) {
      logger.error("Failed to list files in: {}", envDir);
      throw new TemplateLoadingException(e);
    }
    envFiles.forEach(
        templatePath -> {
          try {
            final var templateRelativePath =
                Path.of(ResourceUtils.getFile(templatesRootDir).getPath()).relativize(templatePath);
            final var template = templateConfiguration.getTemplate(templateRelativePath.toString());
            final var envFile =
                Files.createFile(
                    ansibleRunnerInputDir.resolve("env").resolve(templatePath.getFileName()));

            try (var fileWriter = new FileWriter(envFile.toFile())) {
              template.process(templateVariables, fileWriter);
              logger.trace("Processed template {}", template.getName());
            }
          } catch (IOException | TemplateException e) {
            logger.error("Failed to load template {} as file", templatePath);
            throw new TemplateLoadingException(e);
          }
        });
  }

  public void buildAnsibleRunnerInventoryDir(
      Path ansibleRunnerInputDir, Map<String, ?> templateVariables) throws TemplateLoadingException {
    final var inventoryDir = Path.of(azureTemplatesDir, "inventory").toString();
    final Stream<Path> inventoryFiles;
    try {
      inventoryFiles = Files.list(ResourceUtils.getFile(inventoryDir).toPath());
    } catch (IOException e) {
      logger.error("Failed to list files in: {}", inventoryDir);
      throw new TemplateLoadingException(e);
    }
    inventoryFiles.forEach(
        templatePath -> {
          try {
            final var templateRelativePath =
                Path.of(ResourceUtils.getFile(templatesRootDir).getPath()).relativize(templatePath);
            final var template = templateConfiguration.getTemplate(templateRelativePath.toString());
            final var inventoryFile =
                Files.createFile(
                    ansibleRunnerInputDir.resolve("inventory").resolve(templatePath.getFileName()));

            try (var fileWriter = new FileWriter(inventoryFile.toFile())) {
              template.process(templateVariables, fileWriter);
              logger.trace("Processed template {}", template.getName());
            }
          } catch (IOException | TemplateException e) {
            logger.error("Failed to load template {} as file", templatePath);
            throw new TemplateLoadingException(e);
          }
        });
  }

  public void buildAnsibleRunnerProjectDir(
      Path ansibleRunnerInputDir, Map<String, ?> templateVariables) throws TemplateLoadingException {
    final var projectDir = Path.of(azureTemplatesDir, "project").toString();
    final Stream<Path> projectFiles;
    try {
      projectFiles = Files.list(ResourceUtils.getFile(projectDir).toPath());
    } catch (IOException e) {
      logger.error("Failed to list files in: {}", projectDir);
      throw new TemplateLoadingException(e);
    }
    projectFiles.forEach(
        templatePath -> {
          try {
            final var templateRelativePath =
                Path.of(ResourceUtils.getFile(templatesRootDir).getPath()).relativize(templatePath);
            final var template = templateConfiguration.getTemplate(templateRelativePath.toString());
            final var inventoryFile =
                Files.createFile(
                    ansibleRunnerInputDir.resolve("project").resolve(templatePath.getFileName()));

            try (var fileWriter = new FileWriter(inventoryFile.toFile())) {
              template.process(templateVariables, fileWriter);
              logger.trace("Processed template {}", template.getName());
            }
          } catch (IOException | TemplateException e) {
            logger.error("Failed to load template {} as file: {}", templatePath, e.getMessage());
            throw new TemplateLoadingException(e);
          }
        });
  }
}
