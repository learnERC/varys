# VARYS Cloud Bridge

## Synopsis

## Configuration files
Here below you can find the list of configuration files expected by the application
to work properly:

- `application.yml`: the main configuration file, you can find an example at `src/main/resources/application.yml.example`
- `varys-ssh-key`: the SSH private key added to the Monitoring Unit VMs
- `varys-ssh-key.pub`: the SSH public key used by VARYS Cloud Bridge plugins to access the Monitoring Unit VMs
- `kubeconfig`: used by the Kubernetes plugin (required when it is enabled)
- `azureauth.properties`: used by the Azure plugin (required when it is enabled)