package io.varys.monitoringclaimcontroller.config;

public class StreamKey {
  public static final String CREATED = "monitoring-claims:created";
  public static final String UPDATED = "monitoring-claims:updated";
  public static final String DELETED = "monitoring-claims:deleted";
}
