package io.varys.monitoringclaimcontroller.config;

import io.avaje.config.Config;
import javax.inject.Singleton;
import lombok.Data;

@Singleton
@Data
public class VarysApiConfig {
  private final String host;
  private final int port;
  private final String basePath;

  public VarysApiConfig() {
    this.host = Config.get("varys.api.host");
    this.port = Config.getInt("varys.api.port");
    this.basePath = Config.get("varys.api.basePath");
  }
}
