package io.varys.monitoringclaimcontroller;

import io.avaje.inject.SystemContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class App {
  public static void main(String[] args) {
    final var controller = SystemContext.getBean(Controller.class);
    controller.work();
  }
}
