package io.varys.monitoringclaimcontroller.enums;

public enum SharingPolicy {
  RESERVED,
  SHARED
}
