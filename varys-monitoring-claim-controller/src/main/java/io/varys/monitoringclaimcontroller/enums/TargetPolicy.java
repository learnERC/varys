package io.varys.monitoringclaimcontroller.enums;

public enum TargetPolicy {
  GLOBAL_UNIT,
  SIDECAR
}
