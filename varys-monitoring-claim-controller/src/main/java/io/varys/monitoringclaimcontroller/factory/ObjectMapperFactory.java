package io.varys.monitoringclaimcontroller.factory;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.avaje.inject.Bean;
import io.avaje.inject.Factory;
import lombok.extern.slf4j.Slf4j;

@Factory
@Slf4j
public class ObjectMapperFactory {

  public ObjectMapperFactory() {
    log.trace("Created ObjectMapperFactory instance");
  }

  @Bean
  ObjectMapper buildObjectMapper() {
    return new ObjectMapper();
  }
}
