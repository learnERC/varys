package io.varys.monitoringclaimcontroller.factory;

import io.avaje.inject.Bean;
import io.avaje.inject.Factory;
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.endpoint.MonitoringClaimApi;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;
import io.varys.api.jclient.endpoint.ProbeApi;
import io.varys.monitoringclaimcontroller.config.VarysApiConfig;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

@Factory
@Slf4j
public class VarysApiFactory {

  private final VarysApiConfig varysApiConfig;

  @Inject
  public VarysApiFactory(VarysApiConfig varysApiConfig) {
    this.varysApiConfig = varysApiConfig;
    log.trace("Created ApiFactory instance");
  }

  @Bean
  ApiClient buildApiClient() {
    return new ApiClient().setHost(varysApiConfig.getHost()).setPort(varysApiConfig.getPort());
  }

  @Bean
  MonitoringClaimApi buildMonitoringClaimApi(ApiClient apiClient) {
    return new MonitoringClaimApi(apiClient);
  }

  @Bean
  MonitoringUnitApi buildMonitoringUnitApi(ApiClient apiClient) {
    return new MonitoringUnitApi(apiClient);
  }

  @Bean
  ProbeApi buildProbeApi(ApiClient apiClient) {
    return new ProbeApi(apiClient);
  }
}
