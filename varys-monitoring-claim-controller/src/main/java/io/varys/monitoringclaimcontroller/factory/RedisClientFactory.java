package io.varys.monitoringclaimcontroller.factory;

import io.avaje.inject.Bean;
import io.avaje.inject.Factory;
import io.lettuce.core.RedisClient;
import io.varys.monitoringclaimcontroller.config.RedisConfig;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

@Factory
@Slf4j
public class RedisClientFactory {

  private final RedisConfig redisConfig;

  @Inject
  public RedisClientFactory(RedisConfig redisConfig) {
    this.redisConfig = redisConfig;
    log.trace("Created RedisClientFactory instance");
  }

  @Bean
  RedissonClient buildRedissonClient() {
    Config config = new Config();
    config
        .useSingleServer()
        .setAddress(String.format("redis://%s:%s", redisConfig.getHost(), redisConfig.getPort()))
        .setConnectionMinimumIdleSize(1)
        .setConnectionPoolSize(1)
        .setSubscriptionConnectionMinimumIdleSize(1)
        .setSubscriptionConnectionPoolSize(1);
    return Redisson.create(config);
  }

  @Bean
  RedisClient buildRedisClient() {
    return RedisClient.create(redisConfig.getUri());
  }
}
