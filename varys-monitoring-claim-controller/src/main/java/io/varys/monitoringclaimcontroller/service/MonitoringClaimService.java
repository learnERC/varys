package io.varys.monitoringclaimcontroller.service;

import io.varys.api.jclient.model.MonitoringClaim;
import io.varys.api.jclient.model.MonitoringClaimStatus;
import io.varys.api.jclient.model.MonitoringConfiguration;
import java.util.List;

public interface MonitoringClaimService {

  MonitoringClaim updateStatus(String monitoringClaimId, String eTag, MonitoringClaimStatus status);

  MonitoringClaim get(String monitoringClaimId);

  void handleCreated(String id);

  void handleUpdated(String id);

  void handleDeleted(String id);

  List<MonitoringConfiguration> getConfigurations(String monitoringClaimId);
}
