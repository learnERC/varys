package io.varys.monitoringclaimcontroller.service;

import io.varys.api.jclient.model.ConfigurationPattern;
import io.varys.api.jclient.model.Goal;
import io.varys.api.jclient.model.MonitoringConfiguration;
import io.varys.api.jclient.model.MonitoringUnit;
import io.varys.api.jclient.model.MonitoringUnitStatus;
import io.varys.api.jclient.model.Target;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface MonitoringUnitService {
  MonitoringUnit create(ConfigurationPattern pattern, String destinationSystem, Map<String, String> metadata);

  MonitoringUnit get(String monitoringUnitId);

  MonitoringUnit updateStatus(String monitoringUnitId, String eTag, MonitoringUnitStatus status);

  MonitoringUnit createAndAttachDesiredConfigurations(
      String monitoringUnitId,
      String userId,
      String monitoringClaimId,
      Target target,
      Map<String, Goal> selectedProbes);

  MonitoringUnit detachDesiredConfigurations(
      String monitoringUnitId, List<MonitoringConfiguration> monitoringConfigurations);

  Optional<MonitoringUnit> getAvailableByTarget(ConfigurationPattern pattern, Target target);

  Optional<MonitoringUnit> getAvailableByTargetAndUser(
      ConfigurationPattern pattern, Target target, String userId);

  Optional<MonitoringUnit> getAvailableByTargetAndProbe(
      ConfigurationPattern pattern, Target target, String probeId);

  Optional<MonitoringUnit> getAvailableByTargetAndUserAndProbe(
      ConfigurationPattern pattern, Target target, String userId, String probeId);

  Optional<MonitoringUnit> getAvailableByTargetSource(
      ConfigurationPattern pattern, String targetSource);

  Optional<MonitoringUnit> getAvailableByTargetSourceAndUser(
      ConfigurationPattern pattern, String targetSource, String userId);

  Optional<MonitoringUnit> getAvailableByTargetSourceAndProbe(
      ConfigurationPattern pattern, String targetSource, String probeId);

  Optional<MonitoringUnit> getAvailableByTargetSourceAndUserAndProbe(
      ConfigurationPattern pattern, String targetSource, String userId, String probeId);
}
