package io.varys.monitoringclaimcontroller.service;

import io.varys.api.jclient.model.EnvType;
import io.varys.api.jclient.model.Goal;
import io.varys.api.jclient.model.MonitoringClaim;
import io.varys.api.jclient.model.MonitoringConfiguration;
import io.varys.api.jclient.model.Target;
import io.varys.monitoringclaimcontroller.enums.SharingPolicy;
import io.varys.monitoringclaimcontroller.enums.TargetPolicy;
import java.util.List;

public interface PatternService {
  boolean canHandle(
      SharingPolicy sharingPolicy, TargetPolicy targetPolicy, EnvType executionEnvironment);

  void applyForCreated(MonitoringClaim monitoringClaim);

  void applyForUpdated(MonitoringClaim monitoringClaim);

  void applyForDeleted(MonitoringClaim monitoringClaim);

  void createDesiredConfigurations(
      String userId, String monitoringClaimId, Target target, List<Goal> goals);

  void deleteDesiredConfigurations(
      List<MonitoringConfiguration> monitoringClaimConfigurations, List<Goal> goals);
}
