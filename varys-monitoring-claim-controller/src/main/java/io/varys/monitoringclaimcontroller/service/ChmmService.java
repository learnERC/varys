package io.varys.monitoringclaimcontroller.service;

import io.varys.api.jclient.model.Goal;
import java.util.List;

public interface ChmmService {
  List<Goal> expandGoals(List<Goal> goals);
}
