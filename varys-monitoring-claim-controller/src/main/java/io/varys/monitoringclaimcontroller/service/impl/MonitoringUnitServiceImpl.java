package io.varys.monitoringclaimcontroller.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;
import io.varys.api.jclient.model.ConfigurationPattern;
import io.varys.api.jclient.model.Goal;
import io.varys.api.jclient.model.MonitoringConfiguration;
import io.varys.api.jclient.model.MonitoringUnit;
import io.varys.api.jclient.model.MonitoringUnitStatus;
import io.varys.api.jclient.model.Target;
import io.varys.api.jclient.model.UpdateMonitoringUnitStatusRequest;
import io.varys.monitoringclaimcontroller.service.MonitoringUnitService;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

@Singleton
@Slf4j
public class MonitoringUnitServiceImpl implements MonitoringUnitService {

  private final ObjectMapper objectMapper;
  private final MonitoringUnitApi monitoringUnitApi;

  @Inject
  public MonitoringUnitServiceImpl(ObjectMapper objectMapper, MonitoringUnitApi monitoringUnitApi) {
    this.objectMapper = objectMapper;
    this.monitoringUnitApi = monitoringUnitApi;
    log.trace("Created MonitoringUnitService instance");
  }

  @Override
  public MonitoringUnit create(ConfigurationPattern pattern, String destinationSystem, Map<String, String> metadata) {
    final var monitoringUnit = new MonitoringUnit();
    monitoringUnit.setPattern(pattern);
    monitoringUnit.destinationSystem(destinationSystem);
    monitoringUnit.setMetadata(metadata);
    try {
      return monitoringUnitApi.createMonitoringUnit(monitoringUnit);
    } catch (ApiException e) {
      log.error(
          "Failed to create MonitoringUnit with pattern {} and destination system {}",
          pattern,
          destinationSystem);
      throw new RuntimeException(e.getCause());
    }
  }

  @Override
  public MonitoringUnit get(final String monitoringUnitId) {
    try {
      return monitoringUnitApi.getMonitoringUnit(monitoringUnitId);
    } catch (ApiException e) {
      log.error("Failed to get MonitoringUnit: {}", e.getMessage());
      throw new RuntimeException(e.getCause());
    }
  }

  @Override
  public MonitoringUnit updateStatus(
      String monitoringUnitId, String eTag, MonitoringUnitStatus status) {
    try {
      return monitoringUnitApi.updateMonitoringUnitStatus(
          monitoringUnitId, eTag, new UpdateMonitoringUnitStatusRequest().status(status));
    } catch (ApiException e) {
      log.error(
          "Failed to update MonitoringUnit {} status to {}: {}",
          monitoringUnitId,
          status,
          e.getResponseBody());
      throw new RuntimeException(e.getCause());
    }
  }

  @Override
  public MonitoringUnit createAndAttachDesiredConfigurations(
      String monitoringUnitId,
      String userId,
      String monitoringClaimId,
      Target target,
      Map<String, Goal> selectedProbes) {
    try {
      final var configurations =
          selectedProbes.entrySet().stream()
              .map(
                  entry ->
                      new MonitoringConfiguration()
                          .userId(userId)
                          .monitoringClaimId(monitoringClaimId)
                          .target(target)
                          .probeId(entry.getKey())
                          .goal(Goal.fromValue(entry.getValue().getValue()))
                          .monitoringUnitId(monitoringUnitId))
              .collect(Collectors.toList());
      return monitoringUnitApi.createAndAttachDesiredConfigurations(
          monitoringUnitId, configurations);
    } catch (ApiException e) {
      log.error(
          "Failed to created and attach desired configuration to MonitoringUnit {}: {}",
          monitoringUnitId,
          e.getResponseBody());
      throw new RuntimeException(e.getCause());
    }
  }

  @Override
  public MonitoringUnit detachDesiredConfigurations(
      String monitoringUnitId, List<MonitoringConfiguration> monitoringConfigurations) {
    try {
      final var configurationIds =
          monitoringConfigurations.stream()
              .map(MonitoringConfiguration::getId)
              .collect(Collectors.toList());
      return monitoringUnitApi.detachDesiredConfigurations(monitoringUnitId, configurationIds);
    } catch (ApiException e) {
      log.error(
          "Failed to detach desired configuration from MonitoringUnit {}: {}",
          monitoringUnitId,
          e.getResponseBody());
      throw new RuntimeException(e.getCause());
    }
  }

  @Override
  public Optional<MonitoringUnit> getAvailableByTarget(
      ConfigurationPattern pattern, Target target) {
    try {
      final var availableMonitoringUnits =
          monitoringUnitApi.searchMonitoringUnits(
              pattern, null, target.getSourceSystem(), target.getSourceSystemId(), null);

      if (availableMonitoringUnits.isEmpty()) {
        return Optional.empty();
      }

      return Optional.of(availableMonitoringUnits.get(0));

    } catch (ApiException e) {
      log.error("Failed to get available MonitoringUnit by Target");
      throw new RuntimeException(e.getCause());
    }
  }

  @Override
  public Optional<MonitoringUnit> getAvailableByTargetAndUser(
      ConfigurationPattern pattern, Target target, String userId) {
    return Optional.empty();
  }

  @Override
  public Optional<MonitoringUnit> getAvailableByTargetAndProbe(
      ConfigurationPattern pattern, Target target, String probeId) {
    try {
      final var availableMonitoringUnits =
          monitoringUnitApi.searchMonitoringUnits(
              pattern, null, target.getSourceSystem(), target.getSourceSystemId(), probeId);

      if (availableMonitoringUnits.isEmpty()) {
        return Optional.empty();
      }

      return Optional.of(availableMonitoringUnits.get(0));

    } catch (ApiException e) {
      log.error("Failed to get available MonitoringUnit by Target and Probe");
      throw new RuntimeException(e.getCause());
    }
  }

  @Override
  public Optional<MonitoringUnit> getAvailableByTargetAndUserAndProbe(
      final ConfigurationPattern pattern,
      final Target target,
      final String userId,
      final String probeId) {
    try {
      final var availableMonitoringUnits =
          monitoringUnitApi.searchMonitoringUnits(
              pattern, userId, target.getSourceSystem(), target.getSourceSystemId(), probeId);

      if (availableMonitoringUnits.isEmpty()) {
        return Optional.empty();
      }

      return Optional.of(availableMonitoringUnits.get(0));

    } catch (ApiException e) {
      log.error("Failed to get available MonitoringUnit by Target, User and Probe");
      throw new RuntimeException(e.getCause());
    }
  }

  @Override
  public Optional<MonitoringUnit> getAvailableByTargetSource(
      ConfigurationPattern pattern, String targetSource) {
    return Optional.empty();
  }

  @Override
  public Optional<MonitoringUnit> getAvailableByTargetSourceAndUser(
      ConfigurationPattern pattern, String targetSource, String userId) {
    return Optional.empty();
  }

  @Override
  public Optional<MonitoringUnit> getAvailableByTargetSourceAndProbe(
      ConfigurationPattern pattern, String targetSource, String probeId) {
    return Optional.empty();
  }

  @Override
  public Optional<MonitoringUnit> getAvailableByTargetSourceAndUserAndProbe(
      ConfigurationPattern pattern, String targetSource, String userId, String probeId) {
    return Optional.empty();
  }
}
