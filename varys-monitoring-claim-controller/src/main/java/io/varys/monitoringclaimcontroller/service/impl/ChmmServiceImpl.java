package io.varys.monitoringclaimcontroller.service.impl;

import com.scalified.tree.TreeNode;
import com.scalified.tree.multinode.ArrayMultiTreeNode;
import io.varys.api.jclient.model.Goal;
import io.varys.monitoringclaimcontroller.service.ChmmService;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

@Slf4j
@Singleton
public class ChmmServiceImpl implements ChmmService {

  private final TreeNode<Goal> chmm;

  public ChmmServiceImpl() {
    this.chmm = createTreeNodeFromYaml();
    log.trace("Created ChmmService instance");
  }

  private static TreeNode<Goal> createTreeNodeFromYaml() {
    final Yaml yaml = new Yaml();
    try (InputStream inputStream = ClassLoader.getSystemResourceAsStream("chmm.yml")) {
      Iterable<Object> iterable = yaml.loadAll(inputStream);
      for (Object o : iterable) {
        if (o instanceof Map) {
          Map<?, ?> map = (Map) o;
          for (Map.Entry<?, ?> entry : map.entrySet()) {
            TreeNode<Goal> root =
                new ArrayMultiTreeNode<>(Goal.valueOf(String.valueOf(entry.getKey())));
            createTreeNode(entry.getValue(), root);
            return root;
          }
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return null;
  }

  private static void createTreeNode(final Object o, final TreeNode<Goal> parentNode) {
    if (o instanceof Map) {
      Map<?, ?> map = (LinkedHashMap) o;
      for (Map.Entry<?, ?> entry : map.entrySet()) {
        TreeNode<Goal> node =
            new ArrayMultiTreeNode<>(Goal.valueOf(String.valueOf(entry.getKey())));
        parentNode.add(node);
        createTreeNode(entry.getValue(), node);
      }
    } else if (o instanceof List) {
      List<?> list = (List<?>) o;
      for (Object e : list) {
        createTreeNode(e, parentNode);
      }
    } else if (o instanceof String) {
      TreeNode<Goal> node = new ArrayMultiTreeNode<>(Goal.valueOf(String.valueOf(o)));
      parentNode.add(node);
    }
  }

  @Override
  public List<Goal> expandGoals(final List<Goal> goals) {
    final List<Goal> expandedGoals = new ArrayList<>();
    goals.forEach(
        goal -> {
          TreeNode<Goal> node = this.chmm.find(goal);
          if (node.isLeaf()) {
            expandedGoals.add(node.data());
          } else {
            this.traverse(node, expandedGoals);
          }
        });
    return expandedGoals;
  }

  private void traverse(final TreeNode<Goal> node, final List<Goal> expandedGoals) {
    node.subtrees()
        .forEach(
            subtree -> {
              if (!subtree.isLeaf()) {
                this.traverse(subtree, expandedGoals);
              } else {
                expandedGoals.add(subtree.data());
              }
            });
  }
}
