package io.varys.monitoringclaimcontroller.service.impl;

import io.varys.api.jclient.model.ConfigurationPattern;
import io.varys.api.jclient.model.EnvType;
import io.varys.api.jclient.model.Goal;
import io.varys.api.jclient.model.MonitoringClaim;
import io.varys.api.jclient.model.MonitoringConfiguration;
import io.varys.api.jclient.model.Target;
import io.varys.monitoringclaimcontroller.enums.SharingPolicy;
import io.varys.monitoringclaimcontroller.enums.TargetPolicy;
import io.varys.monitoringclaimcontroller.service.MonitoringClaimService;
import io.varys.monitoringclaimcontroller.service.MonitoringUnitService;
import io.varys.monitoringclaimcontroller.service.PatternService;
import io.varys.monitoringclaimcontroller.service.ProbeService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;

@Slf4j
public abstract class BasePatternService implements PatternService {

  protected final ConfigurationPattern configurationPattern;
  private final SharingPolicy handledSharingPolicy;
  private final TargetPolicy handledTargetPolicy;
  private final EnvType handledExecutionEnvironment;
  protected final ProbeService probeService;
  protected final MonitoringUnitService monitoringUnitService;
  protected final MonitoringClaimService monitoringClaimService;

  public BasePatternService(
      ConfigurationPattern configurationPattern,
      SharingPolicy handledSharingPolicy,
      TargetPolicy handledTargetPolicy,
      EnvType handledExecutionEnvironment,
      ProbeService probeService,
      MonitoringUnitService monitoringUnitService,
      MonitoringClaimService monitoringClaimService) {
    this.configurationPattern = configurationPattern;
    this.handledSharingPolicy = handledSharingPolicy;
    this.handledTargetPolicy = handledTargetPolicy;
    this.handledExecutionEnvironment = handledExecutionEnvironment;
    this.probeService = probeService;
    this.monitoringUnitService = monitoringUnitService;
    this.monitoringClaimService = monitoringClaimService;
  }

  @Override
  public boolean canHandle(
      final SharingPolicy sharingPolicy,
      final TargetPolicy targetPolicy,
      final EnvType executionEnvironment) {
    return handledSharingPolicy.equals(sharingPolicy)
        && handledTargetPolicy.equals(targetPolicy)
        && handledExecutionEnvironment.equals(executionEnvironment);
  }

  @Override
  public void applyForCreated(final MonitoringClaim monitoringClaim) {
    log.debug("Applying {} Pattern for MonitoringClaim {}", this.configurationPattern, monitoringClaim.getId());
    final List<Goal> goals = monitoringClaim.getGoals();
    final Target target = monitoringClaim.getTarget();
    final String userId = monitoringClaim.getUserId();

    createDesiredConfigurations(userId, monitoringClaim.getId(), target, goals);
    log.debug("Applied {} Pattern to create MonitoringClaim {}", this.configurationPattern, monitoringClaim.getId());
  }

  @Override
  public void applyForUpdated(final MonitoringClaim monitoringClaim) {
    final var monitoringClaimId = monitoringClaim.getId();
    log.debug("Applying {} Pattern to update MonitoringClaim {}", this.configurationPattern, monitoringClaimId);
    final var goals = monitoringClaim.getGoals();
    final var target = monitoringClaim.getTarget();
    final var userId = monitoringClaim.getUserId();

    final var monitoringClaimConfigurations =
        monitoringClaimService.getConfigurations(monitoringClaim.getId());
    final var currentGoals =
        monitoringClaimConfigurations.stream()
            .map(MonitoringConfiguration::getGoal)
            .collect(Collectors.toList());

    final var goalsToAdd = new ArrayList<>(CollectionUtils.subtract(goals, currentGoals));
    final var goalsToRemove = new ArrayList<>(CollectionUtils.subtract(currentGoals, goals));

    log.debug("{}({}) indicators removed from MonitoringClaim {}", goalsToRemove.size(), goalsToRemove, monitoringClaimId);
    log.debug("{}({}) indicators added to MonitoringClaim {}", goalsToAdd.size(), goalsToAdd, monitoringClaimId);

    if (!goalsToRemove.isEmpty()) {
      deleteDesiredConfigurations(monitoringClaimConfigurations, goalsToRemove);
    }

    if (!goalsToAdd.isEmpty()) {
      createDesiredConfigurations(userId, monitoringClaim.getId(), target, goalsToAdd);
    }
    log.debug("Applied {} Pattern to update MonitoringClaim {}", this.configurationPattern, monitoringClaimId);
  }

  @Override
  public void applyForDeleted(final MonitoringClaim monitoringClaim) {
    log.debug("Applying {} Pattern to delete MonitoringClaim {}", this.configurationPattern, monitoringClaim.getId());
    final List<Goal> goals = monitoringClaim.getGoals();

    final var monitoringClaimConfigurations =
        monitoringClaimService.getConfigurations(monitoringClaim.getId());
    deleteDesiredConfigurations(monitoringClaimConfigurations, goals);
    log.debug("Applied {} Pattern to delete MonitoringClaim {}", this.configurationPattern, monitoringClaim.getId());
  }

  public abstract void createDesiredConfigurations(String userId, String id, Target target, List<Goal> goals);
  public abstract void deleteDesiredConfigurations(List<MonitoringConfiguration> monitoringConfigurations, List<Goal> goals);
}
