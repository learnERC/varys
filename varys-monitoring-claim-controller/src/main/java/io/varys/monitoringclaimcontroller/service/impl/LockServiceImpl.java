package io.varys.monitoringclaimcontroller.service.impl;

import io.varys.monitoringclaimcontroller.service.LockService;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

@Singleton
@Slf4j
public class LockServiceImpl implements LockService {
  private final RedissonClient redissonClient;

  @Inject
  public LockServiceImpl(RedissonClient redissonClient) {
    this.redissonClient = redissonClient;
    log.trace("Created LockService instance");
  }

  @PreDestroy
  public void onShutdown() {
    redissonClient.shutdown();
  }

  @Override
  public RLock acquire(final String lockId) {
    final RLock lock = this.redissonClient.getLock(lockId);
    lock.lock();
    return lock;
  }

  @Override
  public RLock acquire(final String lockId, final long unlockAfter, final TimeUnit timeUnit) {
    final RLock lock = this.redissonClient.getLock(lockId);
    lock.lock(unlockAfter, timeUnit);
    return lock;
  }

  @Override
  public Optional<RLock> tryAcquisition(
      final String lockId, final long waitFor, final long unlockAfter, final TimeUnit timeUnit)
      throws InterruptedException {
    final RLock lock = this.redissonClient.getLock(lockId);
    final boolean acquired = lock.tryLock(waitFor, unlockAfter, timeUnit);
    if (!acquired) {
      return Optional.empty();
    }
    return Optional.of(lock);
  }

  @Override
  public Optional<RLock> tryAcquisition(
      final String lockId, final long waitFor, final TimeUnit timeUnit) {
    try {
      final RLock lock = this.redissonClient.getLock(lockId);
      final boolean acquired = lock.tryLock(waitFor, timeUnit);
      if (!acquired) {
        return Optional.empty();
      }
      return Optional.of(lock);
    } catch (InterruptedException e) {
      return Optional.empty();
    }
  }

  @Override
  public void release(final RLock lock) {
    lock.unlock();
  }

  @Override
  public boolean forceRelease(final RLock lock) {
    return lock.forceUnlock();
  }
}
