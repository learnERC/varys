package io.varys.monitoringclaimcontroller.service.impl;

import io.varys.api.jclient.model.ConfigurationPattern;
import io.varys.api.jclient.model.EnvType;
import io.varys.api.jclient.model.Goal;
import io.varys.api.jclient.model.MonitoringClaim;
import io.varys.api.jclient.model.MonitoringConfiguration;
import io.varys.api.jclient.model.MonitoringUnitStatus;
import io.varys.api.jclient.model.Target;
import io.varys.monitoringclaimcontroller.enums.SharingPolicy;
import io.varys.monitoringclaimcontroller.enums.TargetPolicy;
import io.varys.monitoringclaimcontroller.service.MonitoringClaimService;
import io.varys.monitoringclaimcontroller.service.MonitoringUnitService;
import io.varys.monitoringclaimcontroller.service.PatternService;
import io.varys.monitoringclaimcontroller.service.ProbeService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;

@Singleton
@Slf4j
public class SharedSidecarSingleProbePatternService extends BasePatternService
    implements PatternService {

  @Inject
  public SharedSidecarSingleProbePatternService(
      ProbeService probeService,
      MonitoringUnitService monitoringUnitService,
      MonitoringClaimService monitoringClaimService) {
    super(ConfigurationPattern.SHARED_SIDECAR_SINGLE_PROBE, SharingPolicy.SHARED, TargetPolicy.SIDECAR, EnvType.CONTAINER, probeService, monitoringUnitService, monitoringClaimService);
    log.trace("Created SharedSidecarSingleProbePatternService instance");
  }

  @Override
  public void deleteDesiredConfigurations(
      List<MonitoringConfiguration> monitoringConfigurations, List<Goal> goals) {
    final var configurationsToDetach =
        monitoringConfigurations.stream()
            .filter(c -> goals.contains(c.getGoal()))
            .collect(Collectors.groupingBy(MonitoringConfiguration::getMonitoringUnitId));

    configurationsToDetach.forEach(monitoringUnitService::detachDesiredConfigurations);
  }

  @Override
  public void createDesiredConfigurations(
      final String userId,
      final String monitoringClaimId,
      final Target target,
      final List<Goal> goals) {
    log.debug("Selecting Probes for Goals {}", goals);
    final Map<String, Goal> selectedProbes = probeService.selectProbes(goals, this.configurationPattern);
    log.debug("Selected {} Probes for Goals {}", selectedProbes.size(), goals);

    selectedProbes.forEach(
        ((probeId, goal) -> {
          final var availableMonitoringUnit =
              monitoringUnitService.getAvailableByTargetAndProbe(
                  this.configurationPattern, target, probeId);

          final String monitoringUnitId;
          final String monitoringUnitETag;
          var isMonitoringUnitFirstConfig = false;

          if (availableMonitoringUnit.isEmpty()) {
            final var createdMonitoringUnit =
                monitoringUnitService.create(this.configurationPattern, target.getSourceSystem(), Map.of());
            monitoringUnitId = createdMonitoringUnit.getId();
            monitoringUnitETag = createdMonitoringUnit.getMeta().getVersion();
            isMonitoringUnitFirstConfig = true;
          } else {
            monitoringUnitId = availableMonitoringUnit.get().getId();
            monitoringUnitETag = availableMonitoringUnit.get().getMeta().getVersion();
          }

          log.debug("Selected MonitoringUnit {}", monitoringUnitId);
          log.debug("Creating DesiredConfigurations for Probe {} and Goal {}", probeId, goal);
          monitoringUnitService.createAndAttachDesiredConfigurations(
              monitoringUnitId, userId, monitoringClaimId, target, Map.of(probeId, goal));
          log.debug("Created DesiredConfigurations for Probe {} and Goal {}", probeId, goal);

          if (isMonitoringUnitFirstConfig) {
            monitoringUnitService.updateStatus(
                monitoringUnitId, monitoringUnitETag, MonitoringUnitStatus.CONFIGURED);
          }
        }));
  }
}
