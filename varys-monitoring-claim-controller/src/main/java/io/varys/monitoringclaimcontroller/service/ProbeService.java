package io.varys.monitoringclaimcontroller.service;

import io.varys.api.jclient.model.ConfigurationPattern;
import io.varys.api.jclient.model.Goal;
import java.util.List;
import java.util.Map;

public interface ProbeService {
  Map<String, Goal> selectProbes(List<Goal> goals, ConfigurationPattern pattern);
}
