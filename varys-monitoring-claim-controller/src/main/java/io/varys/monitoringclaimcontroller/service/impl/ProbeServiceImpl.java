package io.varys.monitoringclaimcontroller.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.endpoint.ProbeApi;
import io.varys.api.jclient.model.ConfigurationPattern;
import io.varys.api.jclient.model.DataOutput;
import io.varys.api.jclient.model.Goal;
import io.varys.monitoringclaimcontroller.service.ChmmService;
import io.varys.monitoringclaimcontroller.service.ProbeService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

@Singleton
@Slf4j
public class ProbeServiceImpl implements ProbeService {

  private static final DataOutput CURRENT_DATA_OUTPUT = DataOutput.ELASTICSEARCH;
  private final ObjectMapper objectMapper;
  private final ProbeApi probeApi;
  private final ChmmService chmmService;

  @Inject
  public ProbeServiceImpl(ObjectMapper objectMapper, ProbeApi probeApi, ChmmService chmmService) {
    this.objectMapper = objectMapper;
    this.probeApi = probeApi;
    this.chmmService = chmmService;
    log.trace("Created ProbeService instance");
  }

  @Override
  public Map<String, Goal> selectProbes(
      final List<Goal> goals, final ConfigurationPattern pattern) {
    final var expandedGoals = this.chmmService.expandGoals(goals);
    final Map<String, Goal> probes = new HashMap<>();

    expandedGoals.forEach(
        goal -> {
          try {
            final var probeList = probeApi.searchProbes(pattern, goal, CURRENT_DATA_OUTPUT);
            if (probeList.isEmpty()) {
              log.warn(
                  "No probe has been found for pattern {}, goal {} and data output{}",
                  pattern,
                  goal,
                  CURRENT_DATA_OUTPUT);
              throw new RuntimeException();
            }
            probes.put(probeList.get(0).getId(), goal);
          } catch (ApiException e) {
            log.error("Failed to get probes");
            throw new RuntimeException(e.getCause());
          }
        });

    return probes;
  }
}
