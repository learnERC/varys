package io.varys.monitoringclaimcontroller.service.impl;

import io.avaje.config.Config;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.endpoint.MonitoringClaimApi;
import io.varys.api.jclient.model.EnvType;
import io.varys.api.jclient.model.MonitoringClaim;
import io.varys.api.jclient.model.MonitoringClaimStatus;
import io.varys.api.jclient.model.MonitoringConfiguration;
import io.varys.api.jclient.model.UpdateMonitoringClaimStatusRequest;
import io.varys.monitoringclaimcontroller.enums.SharingPolicy;
import io.varys.monitoringclaimcontroller.enums.TargetPolicy;
import io.varys.monitoringclaimcontroller.exception.PatternNotFoundException;
import io.varys.monitoringclaimcontroller.exception.UnavailableLock;
import io.varys.monitoringclaimcontroller.service.LockService;
import io.varys.monitoringclaimcontroller.service.MonitoringClaimService;
import io.varys.monitoringclaimcontroller.service.PatternService;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;

@Singleton
@Slf4j
public class MonitoringClaimServiceImpl implements MonitoringClaimService {

  private static final SharingPolicy CURRENT_SHARING_POLICY = Config.getEnum(SharingPolicy.class, "policies.sharing");
  private static final TargetPolicy CURRENT_TARGET_POLICY = Config.getEnum(TargetPolicy.class, "policies.target");
  private final MonitoringClaimApi monitoringClaimApi;
  @Inject List<PatternService> patternServices;
  @Inject LockService lockService;

  public MonitoringClaimServiceImpl(MonitoringClaimApi monitoringClaimApi) {
    this.monitoringClaimApi = monitoringClaimApi;
    log.trace("Created MonitoringClaimService instance");
  }

  @Override
  public MonitoringClaim updateStatus(
      String monitoringClaimId, String eTag, MonitoringClaimStatus status) {
    try {
      return monitoringClaimApi.updateMonitoringClaimStatus(
          monitoringClaimId, eTag, new UpdateMonitoringClaimStatusRequest().status(status));
    } catch (ApiException e) {
      log.error("Failed to update MonitoringClaim {} status to {}", monitoringClaimId, status);
      throw new RuntimeException(e.getCause());
    }
  }

  @Override
  public MonitoringClaim get(String monitoringClaimId) {
    try {
      return monitoringClaimApi.getMonitoringClaim(monitoringClaimId);
    } catch (ApiException e) {
      log.error("Failed to get MonitoringClaim: {}", e.getMessage());
      throw new RuntimeException(e.getCause());
    }
  }

  @Override
  public void handleCreated(final String id) {
    log.info("Handling MonitoringClaim {} created", id);
    final var monitoringClaim = get(id);
    final var etag = monitoringClaim.getMeta().getVersion();
    final var executionEnvironment = monitoringClaim.getTarget().getEnvType();

    try {
      final var patternService =
          selectPatternService(CURRENT_SHARING_POLICY, CURRENT_TARGET_POLICY, executionEnvironment);
      log.debug(
          "Using PatternService {} to process MonitoringClaim {}",
          patternService.getClass().getName(),
          monitoringClaim.getId());
      // TODO: discuss about locking scope to enable an higher concurrency level
      final String lockKey =
          String.format(
              "%s-%s",
              monitoringClaim.getTarget().getSourceSystemId(),
              monitoringClaim.getTarget().getSourceSystem());
      final var lock = obtainLock(lockKey);
      log.debug("Got lock {} to process MonitoringClaim {}", lockKey, monitoringClaim.getId());
      try {
        updateStatus(id, etag, MonitoringClaimStatus.PROCESSING);
        log.info("Processing MonitoringClaim {}", id);
        patternService.applyForCreated(monitoringClaim);
        updateStatus(monitoringClaim.getId(), etag, MonitoringClaimStatus.PROCESSED);
        log.info("Processed MonitoringClaim {}", id);
      } catch (Exception e) {
        // TODO: catch specific exceptions
        log.error("An error occurred applying the pattern: {}", e.getMessage());
        updateStatus(monitoringClaim.getId(), etag, MonitoringClaimStatus.ABORTED);
        log.info("Aborted MonitoringClaim {}", monitoringClaim.getId());
      } finally {
        log.debug("Releasing lock {}", lockKey);
        this.lockService.release(lock);
        log.debug("Released lock {}", lockKey);
      }
    } catch (PatternNotFoundException | UnavailableLock e) {
      updateStatus(monitoringClaim.getId(), etag, MonitoringClaimStatus.ABORTED);
      log.info("Aborted MonitoringClaim {}", monitoringClaim.getId());
    }
  }

  @Override
  public void handleUpdated(final String id) {
    log.info("Handling MonitoringClaim {} updated", id);
    final var monitoringClaim = get(id);
    var etag = monitoringClaim.getMeta().getVersion();
    final var executionEnvironment = monitoringClaim.getTarget().getEnvType();

    try {
      final var patternService =
          selectPatternService(CURRENT_SHARING_POLICY, CURRENT_TARGET_POLICY, executionEnvironment);
      log.debug(
          "Using PatternService {} to process MonitoringClaim {}",
          patternService.getClass().getName(),
          id);
      // TODO: discuss about locking scope to enable an higher concurrency level
      final String lockKey =
          String.format(
              "%s-%s",
              monitoringClaim.getTarget().getSourceSystemId(),
              monitoringClaim.getTarget().getSourceSystem());
      final var lock = obtainLock(lockKey);
      log.debug("Got lock {} to process MonitoringClaim {}", lockKey, id);
      try {
        updateStatus(id, etag, MonitoringClaimStatus.PROCESSING);
        patternService.applyForUpdated(monitoringClaim);
        updateStatus(id, etag, MonitoringClaimStatus.PROCESSED);
      } catch (Exception e) {
        // TODO: catch specific exceptions
        log.error("An error occurred applying the pattern: {}", e.getMessage());
        log.debug("Aborting MonitoringClaim {}", id);
        updateStatus(id, etag, MonitoringClaimStatus.ABORTED);
      } finally {
        log.debug("Releasing lock {}", lockKey);
        this.lockService.release(lock);
        log.debug("Released lock {}", lockKey);
      }
    } catch (PatternNotFoundException | UnavailableLock e) {
      updateStatus(id, etag, MonitoringClaimStatus.ABORTED);
    }
  }

  @Override
  public void handleDeleted(final String id) {
    log.info("Handling MonitoringClaim {} deleted", id);
    final var monitoringClaim = get(id);
    final var executionEnvironment = monitoringClaim.getTarget().getEnvType();

    try {
      final var patternService =
          selectPatternService(CURRENT_SHARING_POLICY, CURRENT_TARGET_POLICY, executionEnvironment);
      log.debug(
          "Using PatternService {} to delete MonitoringClaim {}",
          patternService.getClass().getName(),
          id);
      // TODO: discuss about locking scope to enable an higher concurrency level
      final String lockKey =
          String.format(
              "%s-%s",
              monitoringClaim.getTarget().getSourceSystemId(),
              monitoringClaim.getTarget().getSourceSystem());
      final var lock = obtainLock(lockKey);
      log.debug("Got lock {} to delete MonitoringClaim {}", lockKey, id);
      try {
        patternService.applyForDeleted(monitoringClaim);
      } catch (Exception e) {
        // TODO: catch specific exceptions
        log.error("An error occurred applying the pattern: {}", e.getMessage());
        throw new RuntimeException(e.getCause());
      } finally {
        log.debug("Releasing lock {}", lockKey);
        this.lockService.release(lock);
        log.debug("Released lock {}", lockKey);
      }
    } catch (PatternNotFoundException | UnavailableLock e) {
      throw new RuntimeException(e.getCause());
    }
  }

  @Override
  public List<MonitoringConfiguration> getConfigurations(final String monitoringClaimId) {
    try {
      return monitoringClaimApi.getMonitoringClaimConfigurations(monitoringClaimId);
    } catch (ApiException e) {
      log.error("Failed to get MonitoringClaim configurations: {} ", e.getMessage());
      throw new RuntimeException(e.getCause());
    }
  }

  private PatternService selectPatternService(
      final SharingPolicy sharingPolicy, final TargetPolicy targetPolicy, final EnvType execEnv)
      throws PatternNotFoundException {
    final Optional<PatternService> patternService =
        patternServices.stream()
            .filter(s -> s.canHandle(sharingPolicy, targetPolicy, execEnv))
            .findFirst();

    if (patternService.isEmpty()) {
      log.error(
          "PatternService not found for SharingPolicy {} and TargetPolicy {} and ExecutionEnvironment {}",
          sharingPolicy,
          targetPolicy,
          execEnv);
      throw new PatternNotFoundException(
          String.format(
              "Cannot find a PatternService for sharing policy %s, target policy %s and execution environment %s",
              sharingPolicy, targetPolicy, execEnv));
    }

    return patternService.get();
  }

  private RLock obtainLock(String lockKey) throws UnavailableLock {
    final Optional<RLock> lock = lockService.tryAcquisition(lockKey, 5, TimeUnit.SECONDS);
    if (lock.isEmpty()) {
      // TODO: wait more or retry N times
      log.warn("Lock {} is unavailable", lockKey);
      throw new UnavailableLock(String.format("Cannot get a lock with key %s", lockKey));
    }
    return lock.get();
  }
}
