package io.varys.monitoringclaimcontroller.service.impl;

import io.varys.api.jclient.model.ConfigurationPattern;
import io.varys.api.jclient.model.EnvType;
import io.varys.api.jclient.model.Goal;
import io.varys.api.jclient.model.MonitoringConfiguration;
import io.varys.api.jclient.model.MonitoringUnitStatus;
import io.varys.api.jclient.model.Target;
import io.varys.monitoringclaimcontroller.enums.SharingPolicy;
import io.varys.monitoringclaimcontroller.enums.TargetPolicy;
import io.varys.monitoringclaimcontroller.service.MonitoringClaimService;
import io.varys.monitoringclaimcontroller.service.MonitoringUnitService;
import io.varys.monitoringclaimcontroller.service.PatternService;
import io.varys.monitoringclaimcontroller.service.ProbeService;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

@Singleton
@Slf4j
public class InternalProbesPatternService extends BasePatternService
    implements PatternService {

  @Inject
  public InternalProbesPatternService(
      ProbeService probeService,
      MonitoringUnitService monitoringUnitService,
      MonitoringClaimService monitoringClaimService) {
    super(
        ConfigurationPattern.INTERNAL_PROBES,
        SharingPolicy.SHARED,
        TargetPolicy.SIDECAR,
        EnvType.ACCESSIBLE_VM,
        probeService,
        monitoringUnitService,
        monitoringClaimService);
    log.trace("Created InternalProbesPatternService instance");
  }

  @Override
  public void deleteDesiredConfigurations(
      List<MonitoringConfiguration> monitoringConfigurations, List<Goal> goals) {
    final var configurationsToDetach =
        monitoringConfigurations.stream()
            .filter(c -> goals.contains(c.getGoal()))
            .collect(Collectors.groupingBy(MonitoringConfiguration::getMonitoringUnitId));

    configurationsToDetach.forEach(monitoringUnitService::detachDesiredConfigurations);
  }

  @Override
  public void createDesiredConfigurations(
      final String userId,
      final String monitoringClaimId,
      final Target target,
      final List<Goal> goals) {
    log.debug("Selecting Probes for Goals {}", goals);
    final Map<String, Goal> selectedProbes =
        probeService.selectProbes(goals, this.configurationPattern);
    log.debug("Selected {} Probes for Goals {}", selectedProbes.size(), goals);

    final var availableMonitoringUnit =
        monitoringUnitService.getAvailableByTarget(this.configurationPattern, target);

    final String monitoringUnitId;
    final String monitoringUnitETag;
    var isMonitoringUnitFirstConfig = false;

    if (availableMonitoringUnit.isEmpty()) {
      final Map<String, String> metadata = Map.of("targetSourceSystemId", target.getSourceSystemId());
      final var createdMonitoringUnit =
          monitoringUnitService.create(this.configurationPattern, target.getSourceSystem(), metadata);
      monitoringUnitId = createdMonitoringUnit.getId();
      monitoringUnitETag = createdMonitoringUnit.getMeta().getVersion();
      isMonitoringUnitFirstConfig = true;
    } else {
      monitoringUnitId = availableMonitoringUnit.get().getId();
      monitoringUnitETag = availableMonitoringUnit.get().getMeta().getVersion();
    }

    log.debug("Selected MonitoringUnit {}", monitoringUnitId);
    log.debug("Creating DesiredConfigurations for {}", selectedProbes);
    monitoringUnitService.createAndAttachDesiredConfigurations(
        monitoringUnitId, userId, monitoringClaimId, target, selectedProbes);
    log.debug("Created DesiredConfigurations for {}", selectedProbes);

    if (isMonitoringUnitFirstConfig) {
      monitoringUnitService.updateStatus(
          monitoringUnitId, monitoringUnitETag, MonitoringUnitStatus.CONFIGURED);
    }
  }
}
