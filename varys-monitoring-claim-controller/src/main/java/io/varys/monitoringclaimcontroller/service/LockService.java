package io.varys.monitoringclaimcontroller.service;

import java.util.Optional;
import java.util.concurrent.TimeUnit;
import org.redisson.api.RLock;

public interface LockService {
  RLock acquire(String lockId);

  RLock acquire(String lockId, long unlockAfter, TimeUnit timeUnit);

  Optional<RLock> tryAcquisition(String lockId, long waitFor, long unlockAfter, TimeUnit timeUnit)
      throws InterruptedException;

  Optional<RLock> tryAcquisition(String lockId, long waitFor, TimeUnit timeUnit);

  void release(RLock lock);

  boolean forceRelease(RLock lock);
}
