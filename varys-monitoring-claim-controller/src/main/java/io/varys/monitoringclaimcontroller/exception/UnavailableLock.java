package io.varys.monitoringclaimcontroller.exception;

public class UnavailableLock extends Exception {

  public UnavailableLock() {}

  public UnavailableLock(String message) {
    super(message);
  }

  public UnavailableLock(String message, Throwable cause) {
    super(message, cause);
  }

  public UnavailableLock(Throwable cause) {
    super(cause);
  }
}
