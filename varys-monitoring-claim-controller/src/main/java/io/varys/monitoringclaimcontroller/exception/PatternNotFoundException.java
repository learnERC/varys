package io.varys.monitoringclaimcontroller.exception;

public class PatternNotFoundException extends Exception {

  public PatternNotFoundException() {}

  public PatternNotFoundException(String message) {
    super(message);
  }

  public PatternNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public PatternNotFoundException(Throwable cause) {
    super(cause);
  }
}
