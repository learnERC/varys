#!/bin/bash

printf "\nCreating streams for group 'monitoring-claims-controllers'...\n\n"

printf "Stream monitoring-claims:created "
docker-compose exec redis redis-cli XGROUP CREATE monitoring-claims:created monitoring-claims-controllers $ MKSTREAM
printf "Stream monitoring-claims:updated "
docker-compose exec redis redis-cli XGROUP CREATE monitoring-claims:updated monitoring-claims-controllers $ MKSTREAM
printf "Stream monitoring-claims:deleted "
docker-compose exec redis redis-cli XGROUP CREATE monitoring-claims:deleted monitoring-claims-controllers $ MKSTREAM
printf "Stream monitoring-claims:processing "
docker-compose exec redis redis-cli XGROUP CREATE monitoring-claims:processing monitoring-claims-controllers $ MKSTREAM
printf "Stream monitoring-claims:processed "
docker-compose exec redis redis-cli XGROUP CREATE monitoring-claims:processed monitoring-claims-controllers $ MKSTREAM
printf "Stream monitoring-claims:aborted "
docker-compose exec redis redis-cli XGROUP CREATE monitoring-claims:aborted monitoring-claims-controllers $ MKSTREAM

printf "\nCreating streams for group 'monitoring-units-controllers'...\n\n"

printf "Stream monitoring-units:created "
docker-compose exec redis redis-cli XGROUP CREATE monitoring-units:created monitoring-units-controllers $ MKSTREAM
printf "Stream monitoring-units:configured "
docker-compose exec redis redis-cli XGROUP CREATE monitoring-units:configured monitoring-units-controllers $ MKSTREAM
printf "Stream monitoring-units:queued "
docker-compose exec redis redis-cli XGROUP CREATE monitoring-units:queued monitoring-units-controllers $ MKSTREAM
printf "Stream monitoring-units:syncing "
docker-compose exec redis redis-cli XGROUP CREATE monitoring-units:syncing monitoring-units-controllers $ MKSTREAM
printf "Stream monitoring-units:synced "
docker-compose exec redis redis-cli XGROUP CREATE monitoring-units:synced monitoring-units-controllers $ MKSTREAM
printf "Stream monitoring-units:tombstone "
docker-compose exec redis redis-cli XGROUP CREATE monitoring-units:tombstone monitoring-units-controllers $ MKSTREAM
printf "Stream monitoring-units:failed "
docker-compose exec redis redis-cli XGROUP CREATE monitoring-units:failed monitoring-units-controllers $ MKSTREAM
