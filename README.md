# A Monitoring Framework for Declarative Probes Deployment in the Cloud

## Introduction
Cloud services must be continuously monitored to guarantee that misbehaviours can be timely revealed, compensated, and fixed.
While simple applications can be easily monitored and controlled, monitoring non-trivial cloud systems
with dynamic behaviour requires the operators to be able to rapidly adapt the set of collected indicators.

Although the currently available monitoring frameworks are equipped with a rich set of probes to
virtually collect every indicator, they do not provide the automation capabilities required to
quickly and easily change the probes used to monitor a target system.
Indeed, changing the collected indicators beyond standard platform-level indicators is a manual, error-prone and expensive process.

This work presents a Monitoring-as-a-Service architecture that provides the capability to _automatically_
deploy and undeploy arbitrary probes based on a user-provided set of indicators to be collected.
The life-cycle of the probes is fully governed by the architecture, including the detection and
resolution of the _erroneous states_.
The architecture can be exploited jointly with _existing monitoring frameworks_, without requiring
the adoption of a specific monitoring technology.

We experimented our architecture with clouds based on containers and virtual machines, obtaining
evidence of the efficiency and effectiveness of the proposed solution.

## Architecture
![Architecture](diagrams/architecture_diagram.png)
We designed an architecture that consists of four main services and three repositories as shown in figure.

The four services are:
- stateless _API Service_, which offers a gateway to access and update state information about the monitoring system
- a _Monitoring Claim Controller_, which is responsible of handling the life-cycle of every monitoring claim
- a _Monitoring Unit Controller_, which is responsible of handling the life-cycle of every monitoring unit
- a _Cloud Bridge_, which exploits a plug-in based architecture to interact with different cloud providers and platforms, actuating the operations decided by the other services

The three repositories consist of:
- a repository of _monitoring claims_ submitted by operators
- a repository with the created _monitoring units_ and their _configurations_,
- a _probe catalog_ with all probes and deployable artifacts

## Prototype
We implemented the architecture in an available prototype.

The services are implemented as Java standalone applications. The repositories are implemented as MongoDB collections.
The JSON format is normally used both for communication and to persist information, except for the
Cloud Bridge which exposes a gRPC API that uses Protocol Buffers.
The status update messages are delivered through Redis Streams.

The monitoring system can be deployed both on containers and virtual machines, depending on the hosting environment.

We designed a probe catalog reusing probes from Metricbeat, one of the most popular cloud monitoring framework.
We used Elasticsearch as timeseries database to store the values extracted by the probes.
We implemented plug-ins for the Cloud Bridge to support both Kubernetes and Microsoft Azure as target cloud platforms.

## Experiments
See [the README file](experiments/README.md) within the experiments directory.
