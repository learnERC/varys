package io.varys.api.config;

public class Paths {
  public static final String PROBES = "/probes";
  public static final String USERS = "/users";
  public static final String MONITORING_CLAIMS = "/monitoring-claims";
  public static final String MONITORING_UNITS = "/monitoring-units";
  public static final String FULL_CONFIGURATION = "/full-configuration";
  public static final String DESIRED_CONFIGURATIONS = "/desired-configurations";
  public static final String ONGOING_CONFIGURATIONS = "/ongoing-configurations";
  public static final String CURRENT_CONFIGURATIONS = "/current-configurations";
  public static final String CONFIGURATIONS = "/configurations";
  public static final String GOALS = "/goals";
  public static final String BULK = "/_bulk";
  public static final String SEARCH = "/_search";
  public static final String STATUS = "/status";

  public static final String ID_PARAM = "/:id";

  public static String formatLocationHeader(String path, String id) {
    return String.format("%s/%s", path, id);
  }
}
