package io.varys.api.config;

import io.avaje.config.Config;
import javax.inject.Singleton;
import lombok.Data;

@Singleton
@Data
public class RedisConfig {
  private final String host;
  private final int port;
  private final String password;
  private final String uri;

  public RedisConfig() {
    this.host = Config.get("redis.host");
    this.port = Config.getInt("redis.port");
    this.password = Config.get("redis.password");
    this.uri = String.format("redis://%s@%s:%s", password, host, port);
  }
}
