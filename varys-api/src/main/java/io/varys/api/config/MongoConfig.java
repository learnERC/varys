package io.varys.api.config;

import io.avaje.config.Config;
import javax.inject.Singleton;
import lombok.Data;

@Singleton
@Data
public class MongoConfig { ;
  private final String host;
  private final int port;
  private final String authDb;
  private final String username;
  private final String password;
  private final String database;

  public MongoConfig() {
    this.host = Config.get("mongo.host");
    this.port = Config.getInt("mongo.port");
    this.authDb = Config.get("mongo.authDb");
    this.username = Config.get("mongo.username");
    this.password = Config.get("mongo.password");
    this.database = Config.get("mongo.database");
  }
}
