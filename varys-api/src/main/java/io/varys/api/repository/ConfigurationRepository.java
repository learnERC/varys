package io.varys.api.repository;

import com.mongodb.client.ClientSession;
import io.varys.api.model.persistence.MonitoringConfiguration;
import java.util.List;

public interface ConfigurationRepository extends Repository<MonitoringConfiguration, String> {
  List<MonitoringConfiguration> create(List<MonitoringConfiguration> configurations);

  List<MonitoringConfiguration> create(
      ClientSession clientSession, List<MonitoringConfiguration> configurations);

  List<MonitoringConfiguration> findByMonitoringClaim(String monitoringClaimId);

  List<MonitoringConfiguration> findByMonitoringClaim(
      ClientSession clientSession, String monitoringClaimId);

  List<MonitoringConfiguration> findByMonitoringUnit(String monitoringUnitId);

  List<MonitoringConfiguration> findByMonitoringUnit(
      ClientSession clientSession, String monitoringUnitId);

  void updateDesiredStatus(List<String> ids, boolean desired);

  void updateDesiredStatus(ClientSession clientSession, List<String> ids, boolean desired);

  void updateCurrentStatus(List<String> ids, boolean current);

  void updateCurrentStatus(ClientSession clientSession, List<String> ids, boolean current);

  void updateRemovingStatus(List<String> ids, boolean removing);

  void updateRemovingStatus(ClientSession clientSession, List<String> ids, boolean removing);

  void updateAddingStatus(List<String> ids, boolean adding);

  void updateAddingStatus(ClientSession clientSession, List<String> ids, boolean adding);

  void deleteByMonitoringUnitId(String id);

  void deleteByMonitoringUnitId(ClientSession clientSession, String id);

  void incrementRetries(List<String> ids);

  void incrementRetries(ClientSession clientSession, List<String> ids);

  void setRetriesLimitExceeded(List<String> ids);

  void setRetriesLimitExceeded(ClientSession clientSession, List<String> ids);
}
