package io.varys.api.repository;

import io.varys.api.model.persistence.User;

public interface UserRepository extends Repository<User, String> {}
