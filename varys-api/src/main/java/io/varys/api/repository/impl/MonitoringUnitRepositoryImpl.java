package io.varys.api.repository.impl;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Updates.set;

import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.model.ReturnDocument;
import io.varys.api.config.MongoConfig;
import io.varys.api.model.persistence.MonitoringUnit;
import io.varys.api.model.persistence.MonitoringUnitConfigurationPattern;
import io.varys.api.model.persistence.MonitoringUnitStatus;
import io.varys.api.repository.MonitoringUnitRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

@Singleton
@Slf4j
public class MonitoringUnitRepositoryImpl implements MonitoringUnitRepository {

  private static final String COLLECTION_NAME = "monitoring_units";
  private static final FindOneAndReplaceOptions REPLACE_OPTIONS =
      new FindOneAndReplaceOptions().returnDocument(ReturnDocument.AFTER);
  private static final FindOneAndUpdateOptions UPDATE_OPTIONS =
      new FindOneAndUpdateOptions().returnDocument(ReturnDocument.AFTER);
  private final MongoCollection<MonitoringUnit> monitoringUnits;

  @Inject
  public MonitoringUnitRepositoryImpl(MongoClient mongoClient, MongoConfig mongoConfig) {
    this.monitoringUnits =
        mongoClient
            .getDatabase(mongoConfig.getDatabase())
            .getCollection(COLLECTION_NAME, MonitoringUnit.class);
  }

  @Override
  public List<MonitoringUnit> findAll() {
    final List<MonitoringUnit> result = new ArrayList<>();
    for (var monitoringUnit : monitoringUnits.find()) {
      result.add(monitoringUnit);
    }
    return result;
  }

  @Override
  public List<MonitoringUnit> findAll(ClientSession clientSession) {
    final List<MonitoringUnit> result = new ArrayList<>();
    for (var monitoringUnit : monitoringUnits.find(clientSession)) {
      result.add(monitoringUnit);
    }
    return result;
  }

  @Override
  public Optional<MonitoringUnit> find(String id) {
    return Optional.ofNullable(monitoringUnits.find(eq("_id", new ObjectId(id))).first());
  }

  @Override
  public Optional<MonitoringUnit> find(ClientSession clientSession, String id) {
    return Optional.ofNullable(
        monitoringUnits.find(clientSession, eq("_id", new ObjectId(id))).first());
  }

  @Override
  public MonitoringUnit create(MonitoringUnit entity) {
    entity.setId(ObjectId.get());
    monitoringUnits.insertOne(entity);
    return entity;
  }

  @Override
  public MonitoringUnit create(ClientSession session, MonitoringUnit entity) {
    entity.setId(ObjectId.get());
    monitoringUnits.insertOne(session, entity);
    return entity;
  }

  @Override
  public MonitoringUnit update(String id, MonitoringUnit entity) {
    return monitoringUnits.findOneAndReplace(eq("_id", new ObjectId(id)), entity, REPLACE_OPTIONS);
  }

  @Override
  public MonitoringUnit update(ClientSession clientSession, String id, MonitoringUnit entity) {
    return monitoringUnits.findOneAndReplace(
        clientSession, eq("_id", new ObjectId(id)), entity, REPLACE_OPTIONS);
  }

  @Override
  public MonitoringUnit updateStatus(String id, MonitoringUnitStatus status) {
    return monitoringUnits.findOneAndUpdate(eq("_id", new ObjectId(id)), set("status", status.name()), UPDATE_OPTIONS);
  }

  @Override
  public MonitoringUnit updateStatus(ClientSession clientSession, String id, MonitoringUnitStatus status) {
    return monitoringUnits.findOneAndUpdate(clientSession, eq("_id", new ObjectId(id)), set("status", status.name()), UPDATE_OPTIONS);
  }

  @Override
  public MonitoringUnit setCurrentConfigurations(String id, List<String> configurationIds) {
    final var currentConfigurations = configurationIds
        .stream()
        .map(ObjectId::new)
        .collect(Collectors.toList());
    return monitoringUnits.findOneAndUpdate(eq("_id", new ObjectId(id)), set("currentConfigurations", currentConfigurations), UPDATE_OPTIONS);
  }

  @Override
  public MonitoringUnit setCurrentConfigurations(ClientSession clientSession, String id, List<String> configurationIds) {
    final var currentConfigurations = configurationIds
        .stream()
        .map(ObjectId::new)
        .collect(Collectors.toList());
    return monitoringUnits.findOneAndUpdate(clientSession, eq("_id", new ObjectId(id)), set("currentConfigurations", currentConfigurations), UPDATE_OPTIONS);
  }

  @Override
  public MonitoringUnit setDesiredConfigurations(String id, List<String> configurationIds) {
    final var desiredConfigurations = configurationIds
        .stream()
        .map(ObjectId::new)
        .collect(Collectors.toList());
    return monitoringUnits.findOneAndUpdate(eq("_id", new ObjectId(id)), set("desiredConfigurations", desiredConfigurations), UPDATE_OPTIONS);
  }

  @Override
  public MonitoringUnit setDesiredConfigurations(ClientSession clientSession, String id, List<String> configurationIds) {
    final var desiredConfigurations = configurationIds
        .stream()
        .map(ObjectId::new)
        .collect(Collectors.toList());
    return monitoringUnits.findOneAndUpdate(clientSession, eq("_id", new ObjectId(id)), set("desiredConfigurations", desiredConfigurations), UPDATE_OPTIONS);
  }

  @Override
  public void delete(String id) {
    monitoringUnits.findOneAndDelete(eq("_id", new ObjectId(id)));
  }

  @Override
  public void delete(ClientSession clientSession, String id) {
    monitoringUnits.findOneAndDelete(clientSession, eq("_id", new ObjectId(id)));
  }

  @Override
  public List<MonitoringUnit> findByConfigurationPattern(
      MonitoringUnitConfigurationPattern pattern) {
    final List<MonitoringUnit> result = new ArrayList<>();
    for (var monitoringUnit : monitoringUnits.find(eq("pattern", pattern.name()))) {
      result.add(monitoringUnit);
    }
    return result;
  }

  @Override
  public List<MonitoringUnit> findByConfigurationPattern(
      ClientSession clientSession, MonitoringUnitConfigurationPattern pattern) {
    final List<MonitoringUnit> result = new ArrayList<>();
    for (var monitoringUnit : monitoringUnits.find(clientSession, eq("pattern", pattern.name()))) {
      result.add(monitoringUnit);
    }
    return result;
  }

  @Override
  public List<MonitoringUnit> findByConfigurationPatternAndFilterConfigurations(
      MonitoringUnitConfigurationPattern pattern, Map<String, Object> configurationsFilters) {
    final List<MonitoringUnit> result = new ArrayList<>();

    final var lookupAndMatch =
        buildFindByConfigurationPatternAndFilterConfigurationsLookupAndMatch(
            pattern, configurationsFilters);
    monitoringUnits
        .aggregate(Arrays.asList(lookupAndMatch.get("lookup"), lookupAndMatch.get("match")))
        .forEach(result::add);

    return result;
  }

  @Override
  public List<MonitoringUnit> findByConfigurationPatternAndFilterConfigurations(
      ClientSession clientSession,
      MonitoringUnitConfigurationPattern pattern,
      Map<String, Object> configurationsFilters) {
    final List<MonitoringUnit> result = new ArrayList<>();
    final var lookupAndMatch =
        buildFindByConfigurationPatternAndFilterConfigurationsLookupAndMatch(
            pattern, configurationsFilters);
    monitoringUnits
        .aggregate(
            clientSession, Arrays.asList(lookupAndMatch.get("lookup"), lookupAndMatch.get("match")))
        .forEach(result::add);

    return result;
  }

  private Map<String, Bson> buildFindByConfigurationPatternAndFilterConfigurationsLookupAndMatch(
      MonitoringUnitConfigurationPattern pattern, Map<String, Object> configurationsFilters) {
    final var bsonFilters =
        configurationsFilters.entrySet().stream()
            .map(entry -> Filters.eq(entry.getKey(), entry.getValue()))
            .collect(Collectors.toList());
    bsonFilters.add(eq("pattern", pattern.name()));
    bsonFilters.add(eq("configurations.status.desired", true));
    bsonFilters.add(eq("configurations.status.removing", false));

    final var lookup =
        Aggregates.lookup("configurations", "_id", "monitoringUnitId", "configurations");
    final var match = Aggregates.match(Filters.and(bsonFilters));

    return Map.of("lookup", lookup, "match", match);
  }
}
