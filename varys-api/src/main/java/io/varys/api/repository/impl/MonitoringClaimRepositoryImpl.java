package io.varys.api.repository.impl;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.exists;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.currentDate;
import static com.mongodb.client.model.Updates.set;

import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.ReturnDocument;
import io.varys.api.config.MongoConfig;
import io.varys.api.model.persistence.MonitoringClaim;
import io.varys.api.model.persistence.MonitoringClaimStatus;
import io.varys.api.repository.MonitoringClaimRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;

@Singleton
@Slf4j
public class MonitoringClaimRepositoryImpl implements MonitoringClaimRepository {

  private static final String COLLECTION_NAME = "monitoring_claims";
  private static final FindOneAndReplaceOptions UPDATE_OPTIONS =
      new FindOneAndReplaceOptions().returnDocument(ReturnDocument.AFTER);
  private final MongoCollection<MonitoringClaim> monitoringClaims;

  @Inject
  public MonitoringClaimRepositoryImpl(MongoClient mongoClient, MongoConfig mongoConfig) {
    this.monitoringClaims =
        mongoClient
            .getDatabase(mongoConfig.getDatabase())
            .getCollection(COLLECTION_NAME, MonitoringClaim.class);
  }

  @Override
  public List<MonitoringClaim> findAll() {
    final List<MonitoringClaim> result = new ArrayList<>();
    for (var monitoringClaim : monitoringClaims.find()) {
      result.add(monitoringClaim);
    }
    return result;
  }

  @Override
  public List<MonitoringClaim> findAll(ClientSession clientSession) {
    final List<MonitoringClaim> result = new ArrayList<>();
    for (var monitoringClaim : monitoringClaims.find(clientSession)) {
      result.add(monitoringClaim);
    }
    return result;
  }

  @Override
  public Optional<MonitoringClaim> find(String id) {
    return Optional.ofNullable(monitoringClaims.find(eq("_id", new ObjectId(id))).first());
  }

  @Override
  public Optional<MonitoringClaim> find(ClientSession clientSession, String id) {
    return Optional.ofNullable(
        monitoringClaims.find(clientSession, eq("_id", new ObjectId(id))).first());
  }

  @Override
  public MonitoringClaim create(MonitoringClaim entity) {
    entity.setId(ObjectId.get());
    monitoringClaims.insertOne(entity);
    return entity;
  }

  @Override
  public MonitoringClaim create(ClientSession clientSession, MonitoringClaim entity) {
    entity.setId(ObjectId.get());
    monitoringClaims.insertOne(clientSession, entity);
    return entity;
  }

  @Override
  public MonitoringClaim update(String id, MonitoringClaim entity) {
    return monitoringClaims.findOneAndReplace(eq("_id", new ObjectId(id)), entity, UPDATE_OPTIONS);
  }

  @Override
  public MonitoringClaim update(ClientSession clientSession, String id, MonitoringClaim entity) {
    return monitoringClaims.findOneAndReplace(
        clientSession, eq("_id", new ObjectId(id)), entity, UPDATE_OPTIONS);
  }

  @Override
  public void delete(String id) {
    monitoringClaims.deleteOne(eq("_id", new ObjectId(id)));
  }

  @Override
  public void delete(ClientSession clientSession, String id) {
    monitoringClaims.deleteOne(clientSession, eq("_id", new ObjectId(id)));
  }

  @Override
  public void softDelete(String id) {
    monitoringClaims.updateOne(
        eq("_id", new ObjectId(id)),
        combine(
            set("status", MonitoringClaimStatus.DELETED.name()), currentDate(("meta.deletedAt"))));
  }

  @Override
  public void softDelete(ClientSession clientSession, String id) {
    monitoringClaims.updateOne(
        clientSession,
        eq("_id", new ObjectId(id)),
        combine(
            set("status", MonitoringClaimStatus.DELETED.name()), currentDate(("meta.deletedAt"))));
  }

  @Override
  public long countByUserAndTarget(
      String userId,
      String targetSourceSystem,
      String targetSourceSystemId,
      boolean withSoftDeleted) {
    return monitoringClaims.countDocuments(
        and(
            exists("meta.deletedAt", withSoftDeleted),
            eq("userId", new ObjectId(userId)),
            eq("target.sourceSystem", targetSourceSystem),
            eq("target.sourceSystemId", targetSourceSystemId)));
  }

  @Override
  public long countByUserAndTarget(
      ClientSession clientSession,
      String userId,
      String targetSourceSystem,
      String targetSourceSystemId,
      boolean withSoftDeleted) {
    return monitoringClaims.countDocuments(
        clientSession,
        and(
            exists("meta.deletedAt", withSoftDeleted),
            eq("userId", new ObjectId(userId)),
            eq("target.sourceSystem", targetSourceSystem),
            eq("target.sourceSystemId", targetSourceSystemId)));
  }
}
