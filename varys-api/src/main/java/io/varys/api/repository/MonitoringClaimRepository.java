package io.varys.api.repository;

import com.mongodb.client.ClientSession;
import io.varys.api.model.persistence.MonitoringClaim;

public interface MonitoringClaimRepository extends Repository<MonitoringClaim, String> {
  void softDelete(String id);

  void softDelete(ClientSession clientSession, String id);

  long countByUserAndTarget(
      String userId,
      String targetSourceSystem,
      String targetSourceSystemId,
      boolean withSoftDeleted);

  long countByUserAndTarget(
      ClientSession clientSession,
      String userId,
      String targetSourceSystem,
      String targetSourceSystemId,
      boolean withSoftDeleted);
}
