package io.varys.api.repository;

import com.mongodb.client.ClientSession;
import io.varys.api.model.persistence.MonitoringUnit;
import io.varys.api.model.persistence.MonitoringUnitConfigurationPattern;
import io.varys.api.model.persistence.MonitoringUnitStatus;
import java.util.List;
import java.util.Map;

public interface MonitoringUnitRepository extends Repository<MonitoringUnit, String> {
  List<MonitoringUnit> findByConfigurationPattern(MonitoringUnitConfigurationPattern pattern);

  List<MonitoringUnit> findByConfigurationPattern(
      ClientSession clientSession, MonitoringUnitConfigurationPattern pattern);

  List<MonitoringUnit> findByConfigurationPatternAndFilterConfigurations(
      MonitoringUnitConfigurationPattern pattern, Map<String, Object> configurationsFilters);

  List<MonitoringUnit> findByConfigurationPatternAndFilterConfigurations(
      ClientSession clientSession,
      MonitoringUnitConfigurationPattern pattern,
      Map<String, Object> configurationsFilters);

  MonitoringUnit updateStatus(String id, MonitoringUnitStatus status);

  MonitoringUnit updateStatus(ClientSession clientSession, String id, MonitoringUnitStatus status);

  MonitoringUnit setDesiredConfigurations(ClientSession clientSession, String id, List<String> configurationIds);

  MonitoringUnit setDesiredConfigurations(String id, List<String> configurationIds);

  MonitoringUnit setCurrentConfigurations(ClientSession clientSession, String id, List<String> configurationIds);

  MonitoringUnit setCurrentConfigurations(String id, List<String> configurationIds);
}
