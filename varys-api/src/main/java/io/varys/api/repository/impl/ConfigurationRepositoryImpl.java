package io.varys.api.repository.impl;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Updates.inc;
import static com.mongodb.client.model.Updates.set;

import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.ReturnDocument;
import io.varys.api.config.MongoConfig;
import io.varys.api.model.persistence.MonitoringConfiguration;
import io.varys.api.repository.ConfigurationRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;

@Singleton
@Slf4j
public class ConfigurationRepositoryImpl implements ConfigurationRepository {

  private static final String COLLECTION_NAME = "configurations";
  private static final FindOneAndReplaceOptions UPDATE_OPTIONS =
      new FindOneAndReplaceOptions().returnDocument(ReturnDocument.AFTER);
  private final MongoCollection<MonitoringConfiguration> configurations;

  @Inject
  public ConfigurationRepositoryImpl(MongoClient mongoClient, MongoConfig mongoConfig) {
    this.configurations =
        mongoClient
            .getDatabase(mongoConfig.getDatabase())
            .getCollection(COLLECTION_NAME, MonitoringConfiguration.class);
  }

  @Override
  public List<MonitoringConfiguration> findAll() {
    return null;
  }

  @Override
  public List<MonitoringConfiguration> findAll(ClientSession clientSession) {
    return null;
  }

  @Override
  public Optional<MonitoringConfiguration> find(String id) {
    return Optional.ofNullable(configurations.find(eq("_id", new ObjectId(id))).first());
  }

  @Override
  public Optional<MonitoringConfiguration> find(ClientSession clientSession, String id) {
    return Optional.ofNullable(
        configurations.find(clientSession, eq("_id", new ObjectId(id))).first());
  }

  @Override
  public MonitoringConfiguration create(MonitoringConfiguration entity) {
    return null;
  }

  @Override
  public MonitoringConfiguration create(
      ClientSession clientSession, MonitoringConfiguration entity) {
    return null;
  }

  @Override
  public MonitoringConfiguration update(String id, MonitoringConfiguration entity) {
    return null;
  }

  @Override
  public MonitoringConfiguration update(
      ClientSession clientSession, String id, MonitoringConfiguration entity) {
    return null;
  }

  @Override
  public void delete(String id) {}

  @Override
  public void delete(ClientSession clientSession, String id) {}

  @Override
  public List<MonitoringConfiguration> create(List<MonitoringConfiguration> configurations) {
    configurations.forEach(c -> c.setId(new ObjectId()));
    this.configurations.insertMany(configurations);
    return configurations;
  }

  @Override
  public List<MonitoringConfiguration> create(
      ClientSession clientSession, List<MonitoringConfiguration> configurations) {
    configurations.forEach(c -> c.setId(new ObjectId()));
    this.configurations.insertMany(clientSession, configurations);
    return configurations;
  }

  @Override
  public List<MonitoringConfiguration> findByMonitoringClaim(String monitoringClaimId) {
    final List<MonitoringConfiguration> monitoringClaimConfigurations = new ArrayList<>();
    for (var c : configurations.find(eq("monitoringClaimId", new ObjectId(monitoringClaimId)))) {
      monitoringClaimConfigurations.add(c);
    }
    return monitoringClaimConfigurations;
  }

  @Override
  public List<MonitoringConfiguration> findByMonitoringClaim(
      ClientSession clientSession, String monitoringClaimId) {
    final List<MonitoringConfiguration> monitoringClaimConfigurations = new ArrayList<>();
    for (var c :
        configurations.find(
            clientSession, eq("monitoringClaimId", new ObjectId(monitoringClaimId)))) {
      monitoringClaimConfigurations.add(c);
    }
    return monitoringClaimConfigurations;
  }

  @Override
  public List<MonitoringConfiguration> findByMonitoringUnit(String monitoringUnitId) {
    final List<MonitoringConfiguration> monitoringUnitConfigurations = new ArrayList<>();
    for (var c : configurations.find(eq("monitoringUnitId", new ObjectId(monitoringUnitId)))) {
      monitoringUnitConfigurations.add(c);
    }
    return monitoringUnitConfigurations;
  }

  @Override
  public List<MonitoringConfiguration> findByMonitoringUnit(
      ClientSession clientSession, String monitoringUnitId) {
    final List<MonitoringConfiguration> monitoringUnitConfigurations = new ArrayList<>();
    for (var c :
        configurations.find(
            clientSession, eq("monitoringUnitId", new ObjectId(monitoringUnitId)))) {
      monitoringUnitConfigurations.add(c);
    }
    return monitoringUnitConfigurations;
  }

  @Override
  public void updateDesiredStatus(List<String> ids, boolean desired) {
    final var objectIds = ids.stream().map(ObjectId::new).collect(Collectors.toList());
    configurations.updateMany(in("_id", objectIds), set("status.desired", desired));
  }

  @Override
  public void updateDesiredStatus(ClientSession clientSession, List<String> ids, boolean desired) {
    final var objectIds = ids.stream().map(ObjectId::new).collect(Collectors.toList());
    configurations.updateMany(clientSession, in("_id", objectIds), set("status.desired", desired));
  }

  @Override
  public void updateCurrentStatus(List<String> ids, boolean current) {
    final var objectIds = ids.stream().map(ObjectId::new).collect(Collectors.toList());
    configurations.updateMany(in("_id", objectIds), set("status.current", current));
  }

  @Override
  public void updateCurrentStatus(ClientSession clientSession, List<String> ids, boolean current) {
    final var objectIds = ids.stream().map(ObjectId::new).collect(Collectors.toList());
    configurations.updateMany(clientSession, in("_id", objectIds), set("status.current", current));
  }

  @Override
  public void updateRemovingStatus(List<String> ids, boolean removing) {
    final var objectIds = ids.stream().map(ObjectId::new).collect(Collectors.toList());
    configurations.updateMany(in("_id", objectIds), set("status.removing", removing));
  }

  @Override
  public void updateRemovingStatus(
      ClientSession clientSession, List<String> ids, boolean removing) {
    final var objectIds = ids.stream().map(ObjectId::new).collect(Collectors.toList());
    configurations.updateMany(
        clientSession, in("_id", objectIds), set("status.removing", removing));
  }

  @Override
  public void updateAddingStatus(List<String> ids, boolean adding) {
    final var objectIds = ids.stream().map(ObjectId::new).collect(Collectors.toList());
    configurations.updateMany(in("_id", objectIds), set("status.adding", adding));
  }

  @Override
  public void updateAddingStatus(ClientSession clientSession, List<String> ids, boolean adding) {
    final var objectIds = ids.stream().map(ObjectId::new).collect(Collectors.toList());
    configurations.updateMany(clientSession, in("_id", objectIds), set("status.adding", adding));
  }

  @Override
  public void deleteByMonitoringUnitId(String id) {
    configurations.deleteMany(eq("monitoringUnitId", new ObjectId(id)));
  }

  @Override
  public void deleteByMonitoringUnitId(ClientSession clientSession, String id) {
    configurations.deleteMany(clientSession, eq("monitoringUnitId", new ObjectId(id)));
  }

  @Override
  public void incrementRetries(List<String> ids) {
    final var objectIds = ids.stream().map(ObjectId::new).collect(Collectors.toList());
    configurations.updateMany(in("_id", objectIds), inc("status.retries", 1));
  }

  @Override
  public void incrementRetries(ClientSession clientSession, List<String> ids) {
    final var objectIds = ids.stream().map(ObjectId::new).collect(Collectors.toList());
    configurations.updateMany(clientSession, in("_id", objectIds), inc("status.retries", 1));
  }

  @Override
  public void setRetriesLimitExceeded(List<String> ids) {
    final var objectIds = ids.stream().map(ObjectId::new).collect(Collectors.toList());
    configurations.updateMany(in("_id", objectIds), set("status.retryLimitExceeded", true));
  }

  @Override
  public void setRetriesLimitExceeded(ClientSession clientSession, List<String> ids) {
    final var objectIds = ids.stream().map(ObjectId::new).collect(Collectors.toList());
    configurations.updateMany(
        clientSession, in("_id", objectIds), set("status.retryLimitExceeded", true));
  }
}
