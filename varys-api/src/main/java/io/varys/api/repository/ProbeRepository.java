package io.varys.api.repository;

import com.mongodb.client.ClientSession;
import io.varys.api.model.persistence.Probe;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ProbeRepository extends Repository<Probe, String> {

  List<Probe> search(Map<String, Object> filters);

  Optional<Probe> findByArtifactId(String artifactId);

  Optional<Probe> findByArtifactId(ClientSession clientSession, String artifactId);
}
