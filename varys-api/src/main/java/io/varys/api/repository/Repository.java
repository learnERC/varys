package io.varys.api.repository;

import com.mongodb.client.ClientSession;
import java.util.List;
import java.util.Optional;

public interface Repository<T, I> {
  List<T> findAll();

  List<T> findAll(ClientSession clientSession);

  Optional<T> find(I id);

  Optional<T> find(ClientSession clientSession, I id);

  T create(T entity);

  T create(ClientSession clientSession, T entity);

  T update(I id, T entity);

  T update(ClientSession clientSession, I id, T entity);

  void delete(I id);

  void delete(ClientSession clientSession, I id);
}
