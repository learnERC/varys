package io.varys.api.repository.impl;

import static com.mongodb.client.model.Filters.eq;

import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.ReturnDocument;
import io.varys.api.config.MongoConfig;
import io.varys.api.model.persistence.Probe;
import io.varys.api.repository.ProbeRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.bson.types.ObjectId;

@Singleton
public class ProbeRepositoryImpl implements ProbeRepository {

  private static final String COLLECTION_NAME = "probes";
  private static final FindOneAndReplaceOptions UPDATE_OPTIONS =
      new FindOneAndReplaceOptions().returnDocument(ReturnDocument.AFTER);
  private final MongoCollection<Probe> probes;

  @Inject
  public ProbeRepositoryImpl(MongoClient mongoClient, MongoConfig mongoConfig) {
    this.probes =
        mongoClient
            .getDatabase(mongoConfig.getDatabase())
            .getCollection(COLLECTION_NAME, Probe.class);
    IndexOptions indexOptions = new IndexOptions().unique(true);
    this.probes.createIndex(Indexes.ascending("artifactId"), indexOptions);
  }

  @Override
  public List<Probe> findAll() {
    final List<Probe> result = new ArrayList<>();
    for (var probe : probes.find()) {
      result.add(probe);
    }
    return result;
  }

  @Override
  public List<Probe> findAll(ClientSession clientSession) {
    return null;
  }

  @Override
  public Optional<Probe> find(String id) {
    return Optional.ofNullable(probes.find(eq("_id", new ObjectId(id))).first());
  }

  @Override
  public Optional<Probe> find(ClientSession clientSession, String id) {
    return Optional.empty();
  }

  @Override
  public Probe create(Probe entity) {
    entity.setId(entity.getArtifactId());
    probes.insertOne(entity);
    return entity;
  }

  @Override
  public Probe create(ClientSession clientSession, Probe entity) {
    return null;
  }

  @Override
  public Probe update(String id, Probe entity) {
    return null;
  }

  @Override
  public Probe update(ClientSession clientSession, String id, Probe entity) {
    return null;
  }

  @Override
  public void delete(String id) {}

  @Override
  public void delete(ClientSession clientSession, String id) {}

  @Override
  public List<Probe> search(Map<String, Object> filters) {

    final var bsonFilters =
        filters.entrySet().stream()
            .map(entry -> Filters.eq(entry.getKey(), entry.getValue()))
            .collect(Collectors.toList());

    final var result = probes.find(Filters.and(bsonFilters));
    final List<Probe> probes = new ArrayList<>();

    for (var probe : result) {
      probes.add(probe);
    }

    return probes;
  }

  @Override
  public Optional<Probe> findByArtifactId(String artifactId) {
    return Optional.ofNullable(probes.find(eq("artifactId", artifactId)).first());
  }

  @Override
  public Optional<Probe> findByArtifactId(ClientSession clientSession, String artifactId) {
    return Optional.ofNullable(probes.find(clientSession, eq("artifactId", artifactId)).first());
  }
}
