package io.varys.api.repository.impl;

import static com.mongodb.client.model.Filters.eq;

import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.ReturnDocument;
import io.varys.api.config.MongoConfig;
import io.varys.api.model.persistence.User;
import io.varys.api.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.bson.types.ObjectId;

@Singleton
public class UserRepositoryImpl implements UserRepository {

  private static final String COLLECTION_NAME = "users";
  private static final FindOneAndReplaceOptions UPDATE_OPTIONS =
      new FindOneAndReplaceOptions().returnDocument(ReturnDocument.AFTER);
  private final MongoCollection<User> users;

  @Inject
  public UserRepositoryImpl(MongoClient mongoClient, MongoConfig mongoConfig) {
    this.users =
        mongoClient
            .getDatabase(mongoConfig.getDatabase())
            .getCollection(COLLECTION_NAME, User.class);
  }

  @Override
  public List<User> findAll() {
    final List<User> result = new ArrayList<>();
    for (var user : users.find()) {
      result.add(user);
    }
    return result;
  }

  @Override
  public List<User> findAll(ClientSession clientSession) {
    return null;
  }

  @Override
  public Optional<User> find(String id) {
    return Optional.ofNullable(users.find(eq("_id", new ObjectId(id))).first());
  }

  @Override
  public Optional<User> find(ClientSession clientSession, String id) {
    return Optional.empty();
  }

  @Override
  public User create(User entity) {
    entity.setId(ObjectId.get());
    users.insertOne(entity);
    return entity;
  }

  @Override
  public User create(ClientSession clientSession, User entity) {
    return null;
  }

  @Override
  public User update(String id, User entity) {
    return null;
  }

  @Override
  public User update(ClientSession clientSession, String id, User entity) {
    return null;
  }

  @Override
  public void delete(String id) {}

  @Override
  public void delete(ClientSession clientSession, String id) {}
}
