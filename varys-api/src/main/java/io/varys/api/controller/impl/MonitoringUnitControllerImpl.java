package io.varys.api.controller.impl;

import io.avaje.http.api.BeanParam;
import io.avaje.http.api.Controller;
import io.javalin.core.util.Header;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.varys.api.config.Paths;
import io.varys.api.controller.MonitoringUnitController;
import io.varys.api.model.http.MonitoringUnitFullConfiguration;
import io.varys.api.model.http.MonitoringUnitSearchParams;
import io.varys.api.model.http.UpdateMonitoringUnitStatusRequest;
import io.varys.api.model.persistence.MonitoringConfiguration;
import io.varys.api.model.persistence.MonitoringUnit;
import io.varys.api.model.persistence.MonitoringUnitConfigurationPattern;
import io.varys.api.service.EventProducerService;
import io.varys.api.service.MonitoringUnitService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.eclipse.jetty.http.HttpStatus;

@Controller
@Slf4j
public class MonitoringUnitControllerImpl implements MonitoringUnitController {

  private final EventProducerService eventProducerService;
  private final MonitoringUnitService monitoringUnitService;

  @Inject
  public MonitoringUnitControllerImpl(
      EventProducerService eventProducerService, MonitoringUnitService monitoringUnitService) {
    this.eventProducerService = eventProducerService;
    this.monitoringUnitService = monitoringUnitService;
  }

  @Override
  public List<MonitoringUnit> getAll(Context context) {
    return monitoringUnitService.getAll();
  }

  @Override
  public List<MonitoringUnit> search(
      Context context, @BeanParam MonitoringUnitSearchParams searchParams) {

    if (null == searchParams.getConfigurationPattern()) {
      throw new BadRequestResponse(String.format("Configuration pattern is required"));
    }

    final Map<String, Object> filters = new HashMap<>();

    if (null != searchParams.getUserId()) {
      filters.put("configurations.userId", new ObjectId(searchParams.getUserId()));
    }

    if (null != searchParams.getTargetSourceSystem()) {
      filters.put("configurations.target.sourceSystem", searchParams.getTargetSourceSystem());
    }

    if (null != searchParams.getTargetSourceSystemId()) {
      filters.put("configurations.target.sourceSystemId", searchParams.getTargetSourceSystemId());
    }

    if (null != searchParams.getProbeId()) {
      filters.put("configurations.probeId", searchParams.getProbeId());
    }

    return monitoringUnitService.search(
        MonitoringUnitConfigurationPattern.valueOf(searchParams.getConfigurationPattern()),
        filters);
  }

  @Override
  public MonitoringUnit get(String id, Context context) {
    return findOrFail(id);
  }

  @Override
  public MonitoringUnitFullConfiguration getMonitoringUnitFullConfiguration(
      String id, Context context) {
    final var monitoringUnit = findOrFail(id);
    final var configurations = monitoringUnitService.getFullConfiguration(monitoringUnit);

    return MonitoringUnitFullConfiguration.builder().configurations(configurations).build();
  }

  @Override
  public MonitoringUnit create(MonitoringUnit monitoringUnit, Context context) {
    if (monitoringUnit.getId() != null) {
      throw new BadRequestResponse(
          String.format(
              "Unable to create a new monitoring unit with existing id: %s", monitoringUnit));
    }
    final var createdMonitoringUnit = monitoringUnitService.create(monitoringUnit);

    eventProducerService.dispatch(
        "monitoring-units:created",
        Map.of(
            "id",
            createdMonitoringUnit.getId().toString(),
            "eTag",
            createdMonitoringUnit.getMeta().getVersion()));

    context
        .status(HttpStatus.CREATED_201)
        .header(Header.ETAG, createdMonitoringUnit.getMeta().getVersion())
        .header(
            Header.LOCATION,
            Paths.formatLocationHeader(
                Paths.MONITORING_UNITS, createdMonitoringUnit.getId().toString()));

    return createdMonitoringUnit;
  }

  @Override
  public void delete(String id, Context context) {
    monitoringUnitService.delete(id);
    context.status(HttpStatus.NO_CONTENT_204);
  }

  @Override
  public MonitoringUnit createAndAttachDesiredConfigurations(String id, Context context) {
    final var monitoringUnit = findOrFail(id);
    final var configurations = Arrays.asList(context.bodyAsClass(MonitoringConfiguration[].class));
    return monitoringUnitService.createAndAttachDesiredConfigurations(
        monitoringUnit, configurations);
  }

  @Override
  public void incrementDesiredConfigurationsStatusRetries(String id, Context context) {
    final var monitoringUnit = findOrFail(id);
    final var configurationIds = context.queryParams("configurationIds");
    monitoringUnitService.incrementDesiredConfigurationsStatusRetries(
        monitoringUnit, configurationIds);
  }

  @Override
  public void setDesiredConfigurationsStatusRetryLimitExceeded(String id, Context context) {
    final var monitoringUnit = findOrFail(id);
    final var configurationIds = context.queryParams("configurationIds");
    monitoringUnitService.setDesiredConfigurationsStatusRetryLimitExceeded(
        monitoringUnit, configurationIds);
  }

  @Override
  public void setDesiredConfigurationsStatusAdding(String id, Context context) {
    final var monitoringUnit = findOrFail(id);
    final var configurationIds = context.queryParams("configurationIds");
    monitoringUnitService.setDesiredConfigurationsStatusAdding(monitoringUnit, configurationIds);
  }

  @Override
  public MonitoringUnit detachDesiredConfigurations(String id, Context context) {
    final var monitoringUnit = findOrFail(id);
    final var configurationIds = context.queryParams("configurationIds");
    return monitoringUnitService.detachDesiredConfigurations(monitoringUnit, configurationIds);
  }

  @Override
  public MonitoringUnit attachCurrentConfigurations(String id, Context context) {
    final var monitoringUnit = findOrFail(id);
    final var configurationIds = context.queryParams("configurationIds");
    return monitoringUnitService.attachCurrentConfigurations(monitoringUnit, configurationIds);
  }

  @Override
  public MonitoringUnit detachCurrentConfigurations(String id, Context context) {
    final var monitoringUnit = findOrFail(id);
    final var configurationIds = context.queryParams("configurationIds");
    return monitoringUnitService.detachCurrentConfigurations(monitoringUnit, configurationIds);
  }

  @Override
  public MonitoringUnit setCurrentConfigurations(String id, Context context) {
    final var monitoringUnit = findOrFail(id);
    final var configurationIds = (List<String>) context.bodyAsClass(List.class);
    return monitoringUnitService.setCurrentConfigurations(monitoringUnit, configurationIds);
  }

  @Override
  public void setCurrentConfigurationsStatusRemoving(String id, Context context) {
    final var monitoringUnit = findOrFail(id);
    final var configurationIds = context.queryParams("configurationIds");
    monitoringUnitService.setCurrentConfigurationsStatusRemoving(monitoringUnit, configurationIds);
  }

  @Override
  public MonitoringUnit updateStatus(
      String id, UpdateMonitoringUnitStatusRequest status, Context context) {
    final var monitoringUnit = findOrFail(id);
    monitoringUnitService.updateStatus(monitoringUnit, status.getStatus());

    eventProducerService.dispatch(
        String.format("monitoring-units:%s", status.getStatus().name().toLowerCase()),
        Map.of(
            "id",
            monitoringUnit.getId().toString(),
            "eTag",
            monitoringUnit.getMeta().getVersion()));

    context.header(Header.ETAG, monitoringUnit.getMeta().getVersion());
    return monitoringUnit;
  }

  private MonitoringUnit findOrFail(String id) {
    final var monitoringUnit = monitoringUnitService.get(id);
    return monitoringUnit.orElseThrow(
        () -> {
          throw new NotFoundResponse(
              String.format("A monitoring unit with id '%s' is not found", id));
        });
  }
}
