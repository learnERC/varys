package io.varys.api.controller.impl;

import io.avaje.http.api.Controller;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.varys.api.config.Paths;
import io.varys.api.controller.UserController;
import io.varys.api.model.persistence.User;
import io.varys.api.service.EventProducerService;
import io.varys.api.service.UserService;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.jetty.http.HttpStatus;

@Controller
public class UserControllerImpl implements UserController {

  private final UserService userService;

  @Inject
  public UserControllerImpl(UserService userService) {
    this.userService = userService;
  }

  @Override
  public List<User> getAll(final Context context) {
    return userService.getAll();
  }

  @Override
  public User get(final String id, final Context context) {
    final var user = userService.get(id);

    return user.orElseThrow(
        () -> {
          throw new NotFoundResponse(String.format("A user with id '%s' is not found", id));
        });
  }

  @Override
  public User create(final User user, final Context context) {
    if (user.getId() != null) {
      throw new BadRequestResponse(
          String.format("Unable to create a new user with existing id: %s", user));
    }
    final var createdUser = userService.create(user);

    context
        .status(HttpStatus.CREATED_201)
        .header(
            HttpHeader.LOCATION.name(),
            Paths.formatLocationHeader(Paths.USERS, createdUser.getId().toString()));

    return user;
  }

  @Override
  public User update(final String id, final User user, final Context context) {
    if (user.getId() != null && !user.getId().toHexString().equals(id)) {
      throw new BadRequestResponse("Id update is not allowed");
    }
    findOrFail(id);
    return userService.update(id, user);
  }

  @Override
  public void delete(final String id, final Context context) {
    userService.delete(id);
    context.status(HttpStatus.NO_CONTENT_204);
  }

  private User findOrFail(String id) {
    final var user = userService.get(id);
    return user.orElseThrow(
        () -> {
          throw new NotFoundResponse(String.format("A user with id '%s' is not found", id));
        });
  }
}
