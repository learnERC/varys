package io.varys.api.controller;

import io.avaje.http.api.BeanParam;
import io.avaje.http.api.Delete;
import io.avaje.http.api.Get;
import io.avaje.http.api.Patch;
import io.avaje.http.api.Path;
import io.avaje.http.api.Post;
import io.avaje.http.api.Put;
import io.javalin.http.Context;
import io.varys.api.config.Paths;
import io.varys.api.model.http.MonitoringUnitFullConfiguration;
import io.varys.api.model.http.MonitoringUnitSearchParams;
import io.varys.api.model.http.UpdateMonitoringUnitStatusRequest;
import io.varys.api.model.persistence.MonitoringUnit;
import java.util.List;

@Path(Paths.MONITORING_UNITS)
public interface MonitoringUnitController {

  @Get
  List<MonitoringUnit> getAll(Context context);

  @Get(Paths.SEARCH)
  List<MonitoringUnit> search(Context context, @BeanParam MonitoringUnitSearchParams searchParams);

  @Get(Paths.ID_PARAM)
  MonitoringUnit get(String id, Context context);

  @Get(Paths.ID_PARAM + Paths.FULL_CONFIGURATION)
  MonitoringUnitFullConfiguration getMonitoringUnitFullConfiguration(String id, Context context);

  @Post
  MonitoringUnit create(MonitoringUnit monitoringUnit, Context context);

  @Delete(Paths.ID_PARAM)
  void delete(String id, Context context);

  @Post(Paths.ID_PARAM + Paths.DESIRED_CONFIGURATIONS + Paths.BULK)
  MonitoringUnit createAndAttachDesiredConfigurations(String id, Context context);

  @Post(Paths.ID_PARAM + Paths.DESIRED_CONFIGURATIONS + Paths.STATUS + "/retries")
  void incrementDesiredConfigurationsStatusRetries(String id, Context context);

  @Put(Paths.ID_PARAM + Paths.DESIRED_CONFIGURATIONS + Paths.STATUS + "/retry-limit-exceeded")
  void setDesiredConfigurationsStatusRetryLimitExceeded(String id, Context context);

  @Put(Paths.ID_PARAM + Paths.DESIRED_CONFIGURATIONS + Paths.STATUS + "/adding")
  void setDesiredConfigurationsStatusAdding(String id, Context context);

  @Delete(Paths.ID_PARAM + Paths.DESIRED_CONFIGURATIONS)
  MonitoringUnit detachDesiredConfigurations(String id, Context context);

  @Patch(Paths.ID_PARAM + Paths.CURRENT_CONFIGURATIONS)
  MonitoringUnit attachCurrentConfigurations(String id, Context context);

  @Delete(Paths.ID_PARAM + Paths.CURRENT_CONFIGURATIONS)
  MonitoringUnit detachCurrentConfigurations(String id, Context context);

  @Put(Paths.ID_PARAM + Paths.CURRENT_CONFIGURATIONS)
  MonitoringUnit setCurrentConfigurations(String id, Context context);

  @Put(Paths.ID_PARAM + Paths.CURRENT_CONFIGURATIONS + Paths.STATUS + "/removing")
  void setCurrentConfigurationsStatusRemoving(String id, Context context);

  @Patch(Paths.ID_PARAM + "/status")
  MonitoringUnit updateStatus(String id, UpdateMonitoringUnitStatusRequest status, Context context);
}
