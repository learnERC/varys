package io.varys.api.controller;

import io.avaje.http.api.BeanParam;
import io.avaje.http.api.Get;
import io.avaje.http.api.Path;
import io.avaje.http.api.Post;
import io.javalin.http.Context;
import io.varys.api.config.Paths;
import io.varys.api.model.http.ProbeSearchParams;
import io.varys.api.model.persistence.Probe;
import java.util.List;

@Path(Paths.PROBES)
public interface ProbeController {

  @Post
  Probe create(Probe probe, Context context);

  @Get(Paths.SEARCH)
  List<Probe> search(@BeanParam ProbeSearchParams probeSearchParams, Context context);
}
