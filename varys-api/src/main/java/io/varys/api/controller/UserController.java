package io.varys.api.controller;

import io.avaje.http.api.Delete;
import io.avaje.http.api.Get;
import io.avaje.http.api.Patch;
import io.avaje.http.api.Path;
import io.avaje.http.api.Post;
import io.javalin.http.Context;
import io.varys.api.config.Paths;
import io.varys.api.model.persistence.User;
import java.util.List;

@Path(Paths.USERS)
public interface UserController {

  @Get
  List<User> getAll(Context context);

  @Get(Paths.ID_PARAM)
  User get(String id, Context context);

  @Post
  User create(User user, Context context);

  @Patch(Paths.ID_PARAM)
  User update(String id, User user, Context context);

  @Delete(Paths.ID_PARAM)
  void delete(String id, Context context);
}
