package io.varys.api.controller.impl;

import io.avaje.http.api.Controller;
import io.javalin.core.util.Header;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.ConflictResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.varys.api.config.Paths;
import io.varys.api.controller.MonitoringClaimController;
import io.varys.api.exception.MonitoringClaimAlreadyExistsException;
import io.varys.api.model.http.UpdateGoalsRequest;
import io.varys.api.model.http.UpdateMonitoringClaimStatusRequest;
import io.varys.api.model.persistence.MonitoringClaim;
import io.varys.api.model.persistence.MonitoringClaimStatus;
import io.varys.api.model.persistence.MonitoringConfiguration;
import io.varys.api.service.EventProducerService;
import io.varys.api.service.MonitoringClaimService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.http.HttpStatus;

@Controller
@Slf4j
public class MonitoringClaimControllerImpl implements MonitoringClaimController {

  private final MonitoringClaimService monitoringClaimService;
  private final EventProducerService eventProducerService;

  @Inject
  public MonitoringClaimControllerImpl(
      MonitoringClaimService monitoringClaimService, EventProducerService eventProducerService) {
    this.monitoringClaimService = monitoringClaimService;
    this.eventProducerService = eventProducerService;
  }

  @Override
  public MonitoringClaim get(final String id, final Context context) {
    return findOrFail(id);
  }

  @Override
  public List<MonitoringConfiguration> getConfigurations(String id, Context context) {
    final var monitoringClaim = findOrFail(id);
    return monitoringClaimService.getConfigurations(monitoringClaim);
  }

  @Override
  public MonitoringClaim create(final MonitoringClaim monitoringClaim, final Context context) {
    if (monitoringClaim.getId() != null) {
      throw new BadRequestResponse(
          String.format(
              "Unable to create a new monitoring claim with existing id: %s", monitoringClaim));
    }
    try {
      final var createdMonitoringClaim = monitoringClaimService.create(monitoringClaim);
      eventProducerService.dispatch(
          "monitoring-claims:created",
          Map.of(
              "id",
              createdMonitoringClaim.getId().toString(),
              "eTag",
              createdMonitoringClaim.getMeta().getVersion()));

      context
          .status(HttpStatus.CREATED_201)
          .header(Header.ETAG, createdMonitoringClaim.getMeta().getVersion())
          .header(
              Header.LOCATION,
              Paths.formatLocationHeader(
                  Paths.MONITORING_CLAIMS, createdMonitoringClaim.getId().toString()));

      return createdMonitoringClaim;
    } catch (MonitoringClaimAlreadyExistsException e) {
      throw new ConflictResponse(e.getMessage());
    }
  }

  @Override
  public List<MonitoringClaim> createBulk(final Context context) {
    final List<MonitoringClaim> monitoringClaims = Arrays.asList(context.bodyAsClass(MonitoringClaim[].class));
    monitoringClaims.forEach(
        monitoringClaim -> {
          if (monitoringClaim.getId() != null) {
            throw new BadRequestResponse(
                String.format(
                    "Unable to create a new monitoring claim with existing id: %s",
                    monitoringClaim));
          }
        });

    final List<MonitoringClaim> createdMonitoringClaims = new ArrayList<>();
    final List<MonitoringClaim> failedMonitoringClaims = new ArrayList<>();
    monitoringClaims.forEach(
        monitoringClaim -> {
          try {

            createdMonitoringClaims.add(monitoringClaimService.create(monitoringClaim));
          } catch (MonitoringClaimAlreadyExistsException e) {
            log.error("Failed to create a new MonitoringClaim during bulk creation: {}", monitoringClaim);
            failedMonitoringClaims.add(monitoringClaim);
          }
        });

    createdMonitoringClaims.forEach(
        monitoringClaim -> {
          eventProducerService.dispatch(
              "monitoring-claims:created",
              Map.of(
                  "id",
                  monitoringClaim.getId().toString(),
                  "eTag",
                  monitoringClaim.getMeta().getVersion()));
          context.header(
              Header.LOCATION,
              Paths.formatLocationHeader(
                  Paths.MONITORING_CLAIMS, monitoringClaim.getId().toString()));
        });

    if (!failedMonitoringClaims.isEmpty()) {
      context.status(HttpStatus.MULTI_STATUS_207);
    } else {
      context.status(HttpStatus.OK_200);
    }
    return createdMonitoringClaims;
  }

  @Override
  public MonitoringClaim updateGoals(
      final String id, final UpdateGoalsRequest updateGoalsRequest, final Context context) {
    final var monitoringClaim = findOrFail(id);

    if (!monitoringClaim.getMeta().getVersion().equals(context.header("If-Match"))) {
      throw new ConflictResponse("The If-Match header does not match the monitoring claim version");
    }

    if (MonitoringClaimStatus.PROCESSING.equals(monitoringClaim.getStatus())
        || MonitoringClaimStatus.DELETED.equals(monitoringClaim.getStatus())) {
      throw new ConflictResponse("Cannot update when on state PROCESSING or DELETED");
    }

    final var updatedMonitoringClaim =
        monitoringClaimService.updateGoals(monitoringClaim, updateGoalsRequest.getGoals());

    eventProducerService.dispatch(
        "monitoring-claims:updated",
        Map.of("id", id, "eTag", updatedMonitoringClaim.getMeta().getVersion()));

    context.header(Header.ETAG, updatedMonitoringClaim.getMeta().getVersion());
    return updatedMonitoringClaim;
  }

  @Override
  public void delete(final String id, final Context context) {
    final var monitoringClaim = findOrFail(id);
    if (!monitoringClaim.getMeta().getVersion().equals(context.header("If-Match"))) {
      throw new ConflictResponse("The If-Match header does not match the monitoring claim version");
    }
    monitoringClaimService.delete(id);
    eventProducerService.dispatch("monitoring-claims:deleted", Map.of("id", id));
    context.status(HttpStatus.NO_CONTENT_204);
  }

  @Override
  public MonitoringClaim updateStatus(
      final String id, final UpdateMonitoringClaimStatusRequest status, final Context context) {
    final var monitoringClaim = findOrFail(id);
    final var updatedMonitoringClaim =
        monitoringClaimService.updateStatus(monitoringClaim, status.getStatus());

    eventProducerService.dispatch(
        String.format("monitoring-claims:%s", status.getStatus().name().toLowerCase()),
        Map.of("id", id, "eTag", updatedMonitoringClaim.getMeta().getVersion()));

    context.header(Header.ETAG, updatedMonitoringClaim.getMeta().getVersion());
    return updatedMonitoringClaim;
  }

  private MonitoringClaim findOrFail(String id) {
    final var monitoringClaim = monitoringClaimService.get(id);
    return monitoringClaim.orElseThrow(
        () -> {
          throw new NotFoundResponse(
              String.format("A monitoring claim with id '%s' is not found", id));
        });
  }
}
