package io.varys.api.controller;

import io.avaje.http.api.Delete;
import io.avaje.http.api.Get;
import io.avaje.http.api.Patch;
import io.avaje.http.api.Path;
import io.avaje.http.api.Post;
import io.javalin.http.Context;
import io.varys.api.config.Paths;
import io.varys.api.model.http.UpdateGoalsRequest;
import io.varys.api.model.http.UpdateMonitoringClaimStatusRequest;
import io.varys.api.model.persistence.MonitoringClaim;
import io.varys.api.model.persistence.MonitoringConfiguration;
import java.util.List;

@Path(Paths.MONITORING_CLAIMS)
public interface MonitoringClaimController {

  @Get(Paths.ID_PARAM)
  MonitoringClaim get(String id, Context context);

  @Get(Paths.ID_PARAM + Paths.CONFIGURATIONS)
  List<MonitoringConfiguration> getConfigurations(String id, Context context);

  @Post
  MonitoringClaim create(MonitoringClaim monitoringClaim, Context context);

  @Post(Paths.BULK)
  List<MonitoringClaim> createBulk(Context context);

  @Patch(Paths.ID_PARAM + Paths.GOALS)
  MonitoringClaim updateGoals(String id, UpdateGoalsRequest updateGoalsRequest, Context context);

  @Delete(Paths.ID_PARAM)
  void delete(String id, Context context);

  @Patch(Paths.ID_PARAM + Paths.STATUS)
  MonitoringClaim updateStatus(
      String id, UpdateMonitoringClaimStatusRequest status, Context context);
}
