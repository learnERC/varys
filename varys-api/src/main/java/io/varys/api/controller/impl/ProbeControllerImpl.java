package io.varys.api.controller.impl;

import io.avaje.http.api.BeanParam;
import io.avaje.http.api.Controller;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.varys.api.controller.ProbeController;
import io.varys.api.model.http.ProbeSearchParams;
import io.varys.api.model.persistence.Probe;
import io.varys.api.service.ProbeService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

@Controller
public class ProbeControllerImpl implements ProbeController {

  private final ProbeService probeService;

  @Inject
  public ProbeControllerImpl(ProbeService probeService) {
    this.probeService = probeService;
  }

  @Override
  public Probe create(Probe probe, Context context) {
    if (probe.getId() != null) {
      throw new BadRequestResponse(
          String.format("Unable to create a new probe with existing id: %s", probe));
    }

    if (probeService.getByArtifactId(probe.getArtifactId()).isPresent()) {
      throw new BadRequestResponse("Another probe with the same artifact id is already present");
    }

    return probeService.create(probe);
  }

  @Override
  public List<Probe> search(@BeanParam ProbeSearchParams probeSearchParams, Context context) {

    final Map<String, Object> filters = new HashMap<>();

    if (null != probeSearchParams.getSupportedGoal()) {
      filters.put("supportedGoals", probeSearchParams.getSupportedGoal());
    }

    if (null != probeSearchParams.getSupportedDataOutput()) {
      filters.put("supportedDataOutputs", probeSearchParams.getSupportedDataOutput());
    }

    if (null != probeSearchParams.getSupportedPattern()) {
      filters.put("supportedPatterns", probeSearchParams.getSupportedPattern());
    }

    if (filters.isEmpty()) {
      return probeService.getAll();
    }

    return probeService.search(filters);
  }
}
