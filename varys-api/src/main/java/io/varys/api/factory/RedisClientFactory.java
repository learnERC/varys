package io.varys.api.factory;

import io.avaje.inject.Bean;
import io.avaje.inject.Factory;
import io.lettuce.core.RedisClient;
import io.varys.api.config.RedisConfig;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

@Factory
@Slf4j
public class RedisClientFactory {

  private final RedisConfig redisConfig;

  @Inject
  public RedisClientFactory(RedisConfig redisConfig) {
    this.redisConfig = redisConfig;
    log.trace("Created RedisClientFactory instance");
  }

  @Bean
  RedisClient buildRedisClient() {
    return RedisClient.create(redisConfig.getUri());
  }
}
