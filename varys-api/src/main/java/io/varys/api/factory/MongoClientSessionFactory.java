package io.varys.api.factory;

import com.mongodb.ClientSessionOptions;
import com.mongodb.ReadConcern;
import com.mongodb.ReadPreference;
import com.mongodb.TransactionOptions;
import com.mongodb.WriteConcern;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoClient;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public class MongoClientSessionFactory {
  private final MongoClient mongoClient;
  private final TransactionOptions defaultTransactionOptions;

  @Inject
  public MongoClientSessionFactory(MongoClient mongoClient) {
    this.mongoClient = mongoClient;
    this.defaultTransactionOptions =
        TransactionOptions.builder()
            .readPreference(ReadPreference.primary())
            .readConcern(ReadConcern.MAJORITY)
            .writeConcern(WriteConcern.MAJORITY)
            .build();
    log.trace("Created MongoClientSessionFactory instance");
  }

  public ClientSession getSession() {
    return mongoClient.startSession(
        ClientSessionOptions.builder()
            .defaultTransactionOptions(defaultTransactionOptions)
            .build());
  }
}
