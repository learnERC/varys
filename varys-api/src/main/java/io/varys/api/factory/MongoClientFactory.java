package io.varys.api.factory;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ReadConcern;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import io.avaje.inject.Bean;
import io.avaje.inject.Factory;
import io.varys.api.config.MongoConfig;
import java.util.List;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.bson.codecs.pojo.PojoCodecProvider;

@Factory
@Slf4j
public class MongoClientFactory {

  private final MongoConfig mongoConfig;

  @Inject
  public MongoClientFactory(MongoConfig mongoConfig) {
    this.mongoConfig = mongoConfig;
    log.trace("Created MongoClientFactory instance");
  }

  @Bean
  MongoClient buildMongoClient() {
    final var pojoCodecRegistry =
        fromProviders(PojoCodecProvider.builder().automatic(true).build());
    final var codecRegistry =
        fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
    final var credential =
        MongoCredential.createCredential(
            this.mongoConfig.getUsername(), "admin", this.mongoConfig.getPassword().toCharArray());

    return MongoClients.create(
        MongoClientSettings.builder()
            .applyToClusterSettings(
                builder ->
                    builder.hosts(
                        List.of(
                            new ServerAddress(
                                this.mongoConfig.getHost(), this.mongoConfig.getPort()))))
            .credential(credential)
            .codecRegistry(codecRegistry)
            .readConcern(ReadConcern.MAJORITY)
            .writeConcern(WriteConcern.MAJORITY)
            .build());
  }
}
