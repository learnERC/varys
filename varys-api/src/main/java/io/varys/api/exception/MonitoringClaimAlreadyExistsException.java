package io.varys.api.exception;

public class MonitoringClaimAlreadyExistsException extends RuntimeException {
  public MonitoringClaimAlreadyExistsException() {
    super();
  }

  public MonitoringClaimAlreadyExistsException(String errorMessage) {
    super(errorMessage);
  }
}
