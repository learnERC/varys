package io.varys.api.service;

import io.varys.api.model.persistence.MonitoringConfiguration;
import io.varys.api.model.persistence.MonitoringUnit;
import io.varys.api.model.persistence.MonitoringUnitConfigurationPattern;
import io.varys.api.model.persistence.MonitoringUnitStatus;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface MonitoringUnitService {
  List<MonitoringUnit> getAll();

  List<MonitoringUnit> search(
      MonitoringUnitConfigurationPattern pattern, Map<String, Object> filters);

  Optional<MonitoringUnit> get(String id);

  List<MonitoringConfiguration> getFullConfiguration(MonitoringUnit monitoringUnit);

  MonitoringUnit create(MonitoringUnit monitoringUnit);

  MonitoringUnit updateStatus(MonitoringUnit monitoringUnit, MonitoringUnitStatus status);

  void delete(String id);

  MonitoringUnit createAndAttachDesiredConfigurations(
      MonitoringUnit monitoringUnit, List<MonitoringConfiguration> configurations);

  void incrementDesiredConfigurationsStatusRetries(
      MonitoringUnit monitoringUnit, List<String> configurationIds);

  void setDesiredConfigurationsStatusRetryLimitExceeded(
      MonitoringUnit monitoringUnit, List<String> configurationIds);

  void setDesiredConfigurationsStatusAdding(
      MonitoringUnit monitoringUnit, List<String> configurationIds);

  MonitoringUnit detachDesiredConfigurations(
      MonitoringUnit monitoringUnit, List<String> configurationIds);

  MonitoringUnit attachCurrentConfigurations(
      MonitoringUnit monitoringUnit, List<String> configurationIds);

  MonitoringUnit detachCurrentConfigurations(
      MonitoringUnit monitoringUnit, List<String> configurationIds);

  MonitoringUnit setCurrentConfigurations(
      MonitoringUnit monitoringUnit, List<String> configurationIds);

  void setCurrentConfigurationsStatusRemoving(
      MonitoringUnit monitoringUnit, List<String> configurationIds);
}
