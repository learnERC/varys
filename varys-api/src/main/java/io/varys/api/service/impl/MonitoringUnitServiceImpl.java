package io.varys.api.service.impl;

import com.mongodb.MongoException;
import com.mongodb.client.TransactionBody;
import io.varys.api.factory.MongoClientSessionFactory;
import io.varys.api.model.persistence.Meta;
import io.varys.api.model.persistence.MonitoringConfiguration;
import io.varys.api.model.persistence.MonitoringConfigurationStatus;
import io.varys.api.model.persistence.MonitoringUnit;
import io.varys.api.model.persistence.MonitoringUnitConfigurationPattern;
import io.varys.api.model.persistence.MonitoringUnitStatus;
import io.varys.api.repository.ConfigurationRepository;
import io.varys.api.repository.MonitoringUnitRepository;
import io.varys.api.service.MonitoringUnitService;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;

@Slf4j
@Singleton
public class MonitoringUnitServiceImpl implements MonitoringUnitService {
  private final MonitoringUnitRepository monitoringUnitRepository;
  private final ConfigurationRepository configurationRepository;
  private final MongoClientSessionFactory sessionFactory;
  private final int RETRY_LIMIT = 5;

  @Inject
  public MonitoringUnitServiceImpl(
      final MongoClientSessionFactory sessionFactory,
      final MonitoringUnitRepository monitoringUnitRepository,
      final ConfigurationRepository configurationRepository) {
    this.monitoringUnitRepository = monitoringUnitRepository;
    this.configurationRepository = configurationRepository;
    this.sessionFactory = sessionFactory;
  }

  @Override
  public List<MonitoringUnit> getAll() {
    return monitoringUnitRepository.findAll();
  }

  @Override
  public List<MonitoringUnit> search(
      final MonitoringUnitConfigurationPattern pattern, final Map<String, Object> filters) {
    if (filters.isEmpty() && null != pattern) {
      return monitoringUnitRepository.findByConfigurationPattern(pattern);
    }
    return monitoringUnitRepository.findByConfigurationPatternAndFilterConfigurations(
        pattern, filters);
  }

  @Override
  public Optional<MonitoringUnit> get(final String id) {
    return monitoringUnitRepository.find(id);
  }

  @Override
  public List<MonitoringConfiguration> getFullConfiguration(MonitoringUnit monitoringUnit) {
    return configurationRepository.findByMonitoringUnit(monitoringUnit.getId().toHexString());
  }

  @Override
  public MonitoringUnit create(final MonitoringUnit monitoringUnit) {
    final var now = new Date();
    final var meta =
        Meta.builder().version(UUID.randomUUID().toString()).createdAt(now).updatedAt(now).build();
    monitoringUnit.setMeta(meta);
    monitoringUnit.setStatus(MonitoringUnitStatus.CREATED);
    return monitoringUnitRepository.create(monitoringUnit);
  }

  @Override
  public MonitoringUnit updateStatus(
      final MonitoringUnit monitoringUnit, final MonitoringUnitStatus status) {
    try (final var session = sessionFactory.getSession()) {
      final TransactionBody<MonitoringUnit> txnBody =
          () -> monitoringUnitRepository.updateStatus(session, monitoringUnit.getId().toHexString(), status);
      return session.withTransaction(txnBody);
    } catch (MongoException e) {
      log.error(
          "Failed to update MonitoringUnit {} status: transaction aborted. Caught exception during transaction.",
          monitoringUnit.getId());
      throw e;
    }
  }

  @Override
  public void delete(final String id) {
    try (final var session = sessionFactory.getSession()) {
      session.withTransaction(
          () -> {
            configurationRepository.deleteByMonitoringUnitId(session, id);
            monitoringUnitRepository.delete(session, id);
            return null;
          });
    } catch (MongoException e) {
      log.error(
          "Failed to delete MonitoringUnit {}: transaction aborted. Caught exception during transaction.",
          id);
      throw e;
    }
  }

  @Override
  public MonitoringUnit createAndAttachDesiredConfigurations(
      final MonitoringUnit monitoringUnit, final List<MonitoringConfiguration> configurations) {

    try (final var session = sessionFactory.getSession()) {
      final TransactionBody<MonitoringUnit> txnBody =
          () -> {
            final var monitoringUnitInSession = monitoringUnitRepository.find(session, monitoringUnit.getId().toHexString()).get();
            configurations.forEach(
                c -> c.setStatus(MonitoringConfigurationStatus.builder().desired(true).build()));
            final var createdConfigurations =
                configurationRepository.create(session, configurations);
            final var configurationIds =
                createdConfigurations.stream()
                    .map(MonitoringConfiguration::getId)
                    .collect(Collectors.toList());
            final var desiredConfigurations = monitoringUnitInSession.getDesiredConfigurations();
            if (null == desiredConfigurations) {
              monitoringUnitInSession.setDesiredConfigurations(configurationIds);
            } else {
              monitoringUnitInSession.getDesiredConfigurations().addAll(configurationIds);
            }
            final var desiredConfigurationIds = monitoringUnitInSession
                .getDesiredConfigurations()
                .stream()
                .map(ObjectId::toHexString)
                .collect(Collectors.toList());
            return monitoringUnitRepository.setDesiredConfigurations(session, monitoringUnitInSession.getId().toHexString(), desiredConfigurationIds);
          };
      return session.withTransaction(txnBody);
    } catch (MongoException e) {
      log.error(
          "Failed to create and attach desired configurations to MonitoringUnit {}: transaction aborted. Caught exception during transaction.",
          monitoringUnit.getId());
      throw e;
    }
  }

  @Override
  public void incrementDesiredConfigurationsStatusRetries(
      final MonitoringUnit monitoringUnit, final List<String> configurationIds) {
    final var monitoringUnitId = monitoringUnit.getId();
    try (final var session = sessionFactory.getSession()) {
      session.withTransaction(
          () -> {
            log.debug(
                "Incrementing MonitoringUnit {} desired configurations ({}) retries",
                monitoringUnitId,
                configurationIds);
            configurationRepository.incrementRetries(session, configurationIds);
            final var retriesExceededConfigurationIds = configurationIds
                .stream()
                .filter(id -> RETRY_LIMIT <= configurationRepository.find(session, id).get().getStatus().getRetries())
                .collect(Collectors.toList());
            configurationRepository.setRetriesLimitExceeded(session, retriesExceededConfigurationIds);
            final var desiredConfigurations =
                monitoringUnit.getDesiredConfigurations().stream()
                    .filter(
                        desiredConfigurationId ->
                            !retriesExceededConfigurationIds.contains(desiredConfigurationId.toHexString()))
                    .collect(Collectors.toList());
            configurationRepository.updateDesiredStatus(session, configurationIds, false);
            monitoringUnit.setDesiredConfigurations(desiredConfigurations);
            monitoringUnit.getMeta().setVersion(UUID.randomUUID().toString());
            monitoringUnit.getMeta().setUpdatedAt(new Date());
            monitoringUnitRepository.update(
                session, monitoringUnit.getId().toHexString(), monitoringUnit);
            return null;
          });
    } catch (MongoException e) {
      log.error(
          "Failed to increment desired configurations retries for MonitoringUnit {}: transaction aborted. Caught exception during transaction.",
          monitoringUnitId);
      throw e;
    }
  }

  @Override
  public void setDesiredConfigurationsStatusRetryLimitExceeded(
      final MonitoringUnit monitoringUnit, final List<String> configurationIds) {
    try (final var session = sessionFactory.getSession()) {
      session.withTransaction(
          () -> {
            log.debug(
                "Setting MonitoringUnit {} desired configurations ({}) retry limit exceeded",
                monitoringUnit.getId(),
                configurationIds);
            configurationRepository.setRetriesLimitExceeded(session, configurationIds);
            final var desiredConfigurations =
                monitoringUnit.getDesiredConfigurations().stream()
                    .filter(
                        desiredConfigurationId ->
                            !configurationIds.contains(desiredConfigurationId.toHexString()))
                    .collect(Collectors.toList());
            configurationRepository.updateDesiredStatus(session, configurationIds, false);
            monitoringUnit.setDesiredConfigurations(desiredConfigurations);
            monitoringUnit.getMeta().setVersion(UUID.randomUUID().toString());
            monitoringUnit.getMeta().setUpdatedAt(new Date());
            monitoringUnitRepository.update(
                session, monitoringUnit.getId().toHexString(), monitoringUnit);
            return null;
          });
    } catch (MongoException e) {
      log.error(
          "Failed to set desired configurations retry limit exceeded for MonitoringUnit {}: transaction aborted. Caught exception during transaction.",
          monitoringUnit.getId());
      throw e;
    }
  }

  @Override
  public void setDesiredConfigurationsStatusAdding(
      final MonitoringUnit monitoringUnit, final List<String> configurationIds) {
    log.debug(
        "Setting MonitoringUnit {} desired configurations ({}) status adding",
        monitoringUnit.getId(),
        configurationIds);
    configurationRepository.updateAddingStatus(configurationIds, true);
  }

  @Override
  public MonitoringUnit detachDesiredConfigurations(
      final MonitoringUnit monitoringUnit, final List<String> configurationIds) {
    if (null == monitoringUnit.getDesiredConfigurations()) {
      return monitoringUnit;
    }

    try (final var session = sessionFactory.getSession()) {
      final TransactionBody<MonitoringUnit> txnBody =
          () -> {
            final var desiredConfigurations =
                monitoringUnit.getDesiredConfigurations().stream()
                    .filter(
                        desiredConfigurationId ->
                            !configurationIds.contains(desiredConfigurationId.toHexString()))
                    .collect(Collectors.toList());

            configurationRepository.updateDesiredStatus(session, configurationIds, false);

            monitoringUnit.setDesiredConfigurations(desiredConfigurations);
            monitoringUnit.getMeta().setVersion(UUID.randomUUID().toString());
            monitoringUnit.getMeta().setUpdatedAt(new Date());
            return monitoringUnitRepository.update(
                session, monitoringUnit.getId().toHexString(), monitoringUnit);
          };
      return session.withTransaction(txnBody);
    } catch (MongoException e) {
      log.error(
          "Failed to detach desired configurations from MonitoringUnit {}: transaction aborted. Caught exception during transaction.",
          monitoringUnit.getId());
      throw e;
    }
  }

  @Override
  public MonitoringUnit attachCurrentConfigurations(
      final MonitoringUnit monitoringUnit, final List<String> configurationIds) {
    final var configurationObjectIds =
        configurationIds.stream().map(ObjectId::new).collect(Collectors.toList());

    if (null == monitoringUnit.getCurrentConfigurations()) {
      monitoringUnit.setCurrentConfigurations(configurationObjectIds);
    } else {
      monitoringUnit.getCurrentConfigurations().addAll(configurationObjectIds);
    }
    final var currentConfigurations = monitoringUnit.getCurrentConfigurations().stream().map(ObjectId::toHexString).collect(
        Collectors.toList());
    return monitoringUnitRepository.setCurrentConfigurations(monitoringUnit.getId().toHexString(), currentConfigurations);
  }

  @Override
  public MonitoringUnit detachCurrentConfigurations(
      final MonitoringUnit monitoringUnit, final List<String> configurationIds) {
    if (null == monitoringUnit.getCurrentConfigurations()) {
      return monitoringUnit;
    }

    try (final var session = sessionFactory.getSession()) {
      final TransactionBody<MonitoringUnit> txnBody =
          () -> {
            configurationRepository.updateCurrentStatus(session, configurationIds, false);

            final var currentConfigurations =
                monitoringUnit.getCurrentConfigurations().stream()
                    .filter(
                        currentConfigurationId ->
                            !configurationIds.contains(currentConfigurationId.toHexString()))
                    .collect(Collectors.toList());
            monitoringUnit.setCurrentConfigurations(currentConfigurations);
            return monitoringUnitRepository.update(
                session, monitoringUnit.getId().toHexString(), monitoringUnit);
          };
      return session.withTransaction(txnBody);
    } catch (MongoException e) {
      log.error(
          "Failed to detach current configurations from MonitoringUnit {}: transaction aborted. Caught exception during transaction.",
          monitoringUnit.getId());
      throw e;
    }
  }

  @Override
  public MonitoringUnit setCurrentConfigurations(
      final MonitoringUnit monitoringUnit, final List<String> configurationIds) {

    try (final var session = sessionFactory.getSession()) {
      final TransactionBody<MonitoringUnit> txnBody =
          () -> {
            configurationRepository.updateCurrentStatus(session, configurationIds, true);
            configurationRepository.updateAddingStatus(session, configurationIds, false);
            return monitoringUnitRepository.setCurrentConfigurations(
                session, monitoringUnit.getId().toHexString(), configurationIds);
          };
      return session.withTransaction(txnBody);
    } catch (MongoException e) {
      log.error(
          "Failed to set current configurations to MonitoringUnit {}: transaction aborted. Caught exception during transaction.",
          monitoringUnit.getId());
      throw e;
    }
  }

  @Override
  public void setCurrentConfigurationsStatusRemoving(
      final MonitoringUnit monitoringUnit, final List<String> configurationIds) {
    log.debug(
        "Setting MonitoringUnit {} current configurations ({}) status removing",
        monitoringUnit.getId(),
        configurationIds);
    configurationRepository.updateRemovingStatus(configurationIds, true);
  }
}
