package io.varys.api.service.impl;

import com.mongodb.MongoException;
import com.mongodb.client.TransactionBody;
import io.varys.api.exception.MonitoringClaimAlreadyExistsException;
import io.varys.api.factory.MongoClientSessionFactory;
import io.varys.api.model.persistence.Goal;
import io.varys.api.model.persistence.Meta;
import io.varys.api.model.persistence.MonitoringClaim;
import io.varys.api.model.persistence.MonitoringClaimStatus;
import io.varys.api.model.persistence.MonitoringConfiguration;
import io.varys.api.repository.ConfigurationRepository;
import io.varys.api.repository.MonitoringClaimRepository;
import io.varys.api.service.MonitoringClaimService;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public class MonitoringClaimServiceImpl implements MonitoringClaimService {

  private final MongoClientSessionFactory sessionFactory;
  private final MonitoringClaimRepository monitoringClaimRepository;
  private final ConfigurationRepository configurationRepository;

  @Inject
  public MonitoringClaimServiceImpl(
      MongoClientSessionFactory sessionFactory,
      MonitoringClaimRepository monitoringClaimRepository,
      ConfigurationRepository configurationRepository) {
    this.sessionFactory = sessionFactory;
    this.monitoringClaimRepository = monitoringClaimRepository;
    this.configurationRepository = configurationRepository;
  }

  @Override
  public Optional<MonitoringClaim> get(final String id) {
    return monitoringClaimRepository.find(id);
  }

  @Override
  public List<MonitoringConfiguration> getConfigurations(final MonitoringClaim monitoringClaim) {
    return configurationRepository.findByMonitoringClaim(monitoringClaim.getId().toHexString());
  }

  @Override
  public MonitoringClaim create(MonitoringClaim monitoringClaim)
      throws MonitoringClaimAlreadyExistsException {

    try (final var session = sessionFactory.getSession()) {
      final TransactionBody<MonitoringClaim> txnBody =
          () -> {
            if (monitoringClaimRepository.countByUserAndTarget(
                    session,
                    monitoringClaim.getUserId().toHexString(),
                    monitoringClaim.getTarget().getSourceSystem(),
                    monitoringClaim.getTarget().getSourceSystemId(),
                    false)
                > 0) {
              throw new MonitoringClaimAlreadyExistsException(
                  String.format(
                      "A monitoring claim for user %s and target %s (source: %s) already exists",
                      monitoringClaim.getUserId(),
                      monitoringClaim.getTarget().getSourceSystemId(),
                      monitoringClaim.getTarget().getSourceSystem()));
            }
            final var now = new Date();
            final var meta =
                Meta.builder()
                    .version(UUID.randomUUID().toString())
                    .createdAt(now)
                    .updatedAt(now)
                    .build();
            monitoringClaim.setMeta(meta);
            monitoringClaim.setStatus(MonitoringClaimStatus.CREATED);
            return monitoringClaimRepository.create(session, monitoringClaim);
          };
      return session.withTransaction(txnBody);
    } catch (MongoException e) {
      log.error(
          "Failed to create MonitoringClaim {}: transaction aborted. Caught exception during transaction.",
          monitoringClaim.toString());
      throw e;
    }
  }

  @Override
  public MonitoringClaim updateGoals(MonitoringClaim monitoringClaim, List<Goal> goals) {
    monitoringClaim.setGoals(goals);
    monitoringClaim.setStatus(MonitoringClaimStatus.UPDATED);
    monitoringClaim.getMeta().setUpdatedAt(new Date());
    monitoringClaim.getMeta().setVersion(UUID.randomUUID().toString());

    return monitoringClaimRepository.update(monitoringClaim.getId().toHexString(), monitoringClaim);
  }

  @Override
  public void delete(String id) {
    monitoringClaimRepository.softDelete(id);
  }

  @Override
  public MonitoringClaim updateStatus(
      MonitoringClaim monitoringClaim, MonitoringClaimStatus status) {
    monitoringClaim.setStatus(status);
    return monitoringClaimRepository.update(monitoringClaim.getId().toHexString(), monitoringClaim);
  }
}
