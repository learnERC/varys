package io.varys.api.service;

import io.varys.api.model.persistence.User;
import java.util.List;
import java.util.Optional;

public interface UserService {
  List<User> getAll();

  Optional<User> get(String id);

  User create(User user);

  User update(String id, User user);

  void delete(String id);
}
