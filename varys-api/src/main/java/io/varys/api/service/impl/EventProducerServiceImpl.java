package io.varys.api.service.impl;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.varys.api.config.RedisConfig;
import io.varys.api.service.EventProducerService;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public class EventProducerServiceImpl implements EventProducerService {

  private final RedisConfig redisConfig;
  private RedisCommands<String, String> syncCommands;
  private RedisClient redisClient;
  private StatefulRedisConnection<String, String> redisConnection;

  @Inject
  public EventProducerServiceImpl(RedisConfig redisConfig) {
    this.redisConfig = redisConfig;
  }

  public String dispatch(String topic, Map<String, String> message) {
    final var messageId = syncCommands.xadd(topic, message);
    log.debug("Dispatched message {} on stream {}: {}", messageId, topic, message);
    return messageId;
  }

  @PostConstruct
  public void onStartup() {
    this.redisClient = RedisClient.create(this.redisConfig.getUri());
    this.redisConnection = redisClient.connect();
    this.syncCommands = redisConnection.sync();
  }

  @PreDestroy
  public void onShutdown() {
    this.redisConnection.close();
    this.redisClient.shutdown();
  }
}
