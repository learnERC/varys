package io.varys.api.service;

import io.varys.api.model.persistence.Probe;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ProbeService {

  List<Probe> getAll();

  Optional<Probe> getByArtifactId(String artifactId);

  Probe create(Probe user);

  List<Probe> search(Map<String, Object> filters);
}
