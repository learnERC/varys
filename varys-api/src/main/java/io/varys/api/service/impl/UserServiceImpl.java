package io.varys.api.service.impl;

import io.varys.api.model.persistence.User;
import io.varys.api.repository.UserRepository;
import io.varys.api.service.UserService;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  @Inject
  public UserServiceImpl(final UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public List<User> getAll() {
    return userRepository.findAll();
  }

  @Override
  public Optional<User> get(String id) {
    return userRepository.find(id);
  }

  @Override
  public User create(User user) {
    return userRepository.create(user);
  }

  @Override
  public User update(String id, User user) {
    return userRepository.update(id, user);
  }

  @Override
  public void delete(String id) {
    userRepository.delete(id);
  }
}
