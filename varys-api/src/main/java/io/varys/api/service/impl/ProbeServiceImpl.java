package io.varys.api.service.impl;

import io.varys.api.model.persistence.Probe;
import io.varys.api.repository.ProbeRepository;
import io.varys.api.service.ProbeService;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public class ProbeServiceImpl implements ProbeService {

  private final ProbeRepository probeRepository;

  @Inject
  public ProbeServiceImpl(final ProbeRepository probeRepository) {
    this.probeRepository = probeRepository;
  }

  @Override
  public List<Probe> getAll() {
    return probeRepository.findAll();
  }

  @Override
  public Optional<Probe> getByArtifactId(String artifactId) {
    return probeRepository.findByArtifactId(artifactId);
  }

  @Override
  public Probe create(final Probe probe) {
    return probeRepository.create(probe);
  }

  @Override
  public List<Probe> search(Map<String, Object> filters) {
    return probeRepository.search(filters);
  }
}
