package io.varys.api.service;

import java.util.Map;

public interface EventProducerService {
  String dispatch(String topic, Map<String, String> message);
}
