package io.varys.api.service;

import io.varys.api.exception.MonitoringClaimAlreadyExistsException;
import io.varys.api.model.persistence.Goal;
import io.varys.api.model.persistence.MonitoringClaim;
import io.varys.api.model.persistence.MonitoringClaimStatus;
import io.varys.api.model.persistence.MonitoringConfiguration;
import java.util.List;
import java.util.Optional;

public interface MonitoringClaimService {
  Optional<MonitoringClaim> get(String id);

  List<MonitoringConfiguration> getConfigurations(MonitoringClaim monitoringClaim);

  MonitoringClaim create(MonitoringClaim monitoringClaim)
      throws MonitoringClaimAlreadyExistsException;

  MonitoringClaim updateGoals(MonitoringClaim monitoringClaim, List<Goal> goals);

  void delete(String id);

  MonitoringClaim updateStatus(MonitoringClaim monitoringClaim, MonitoringClaimStatus status);
}
