package io.varys.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mongodb.MongoException;
import io.avaje.config.Config;
import io.avaje.http.api.WebRoutes;
import io.avaje.inject.SystemContext;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;
import io.javalin.core.util.RouteOverviewPlugin;
import io.javalin.http.InternalServerErrorResponse;
import io.javalin.http.staticfiles.Location;
import io.javalin.plugin.json.JavalinJackson;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;
import io.varys.api.model.http.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.http.MimeTypes;

@Slf4j
public class App {

  public static void main(String[] args) {
    var app = Javalin.create(App::config);
    var webRoutes = SystemContext.getBeans(WebRoutes.class);

    app.routes(() -> webRoutes.forEach(WebRoutes::registerRoutes));
    app.exception(
            MongoException.class,
            (e, ctx) -> {
              log.error("Something really bad happened with the database: {}", e.getMessage());
              ctx.status(500);
            })
        .error(
            500,
            ctx -> {
              throw new InternalServerErrorResponse("Something really bad happened :(");
            });

    Runtime.getRuntime().addShutdownHook(new Thread(app::stop));
    app.start(Config.getInt("app.port"));
  }

  public static void config(JavalinConfig config) {
    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new JavaTimeModule());
    JavalinJackson.configure(mapper);

    config.logIfServerNotStarted = false;
    config.showJavalinBanner = false;
    config.defaultContentType = MimeTypes.Type.APPLICATION_JSON_UTF_8.asString();
    config.enableCorsForAllOrigins();
    config.enableDevLogging();
    // config.registerPlugin(new OpenApiPlugin(getOpenApiOptions()));
    config.registerPlugin(new RouteOverviewPlugin("/"));

    config.addStaticFiles("public", Location.CLASSPATH);
  }

  private static OpenApiOptions getOpenApiOptions() {
    final var applicationInfo =
        new Info().title("VARYS").version("1.0").description("VARYS API Documentation");
    return new OpenApiOptions(applicationInfo)
        .path("/openapi.json")
        .activateAnnotationScanningFor("io.varys.api")
        .swagger(new SwaggerOptions("/swagger-ui").title("VARYS API Documentation"))
        .defaultDocumentation(
            doc -> {
              doc.json("500", ErrorResponse.class);
              doc.json("503", ErrorResponse.class);
            });
  }
}
