package io.varys.api.model.http;

import io.varys.api.model.persistence.MonitoringUnitStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateMonitoringUnitStatusRequest {

  private MonitoringUnitStatus status;
}
