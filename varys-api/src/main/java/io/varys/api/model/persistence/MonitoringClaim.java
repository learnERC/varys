package io.varys.api.model.persistence;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MonitoringClaim {
  @JsonSerialize(using = ToStringSerializer.class)
  private ObjectId id;

  private Meta meta;

  @JsonSerialize(using = ToStringSerializer.class)
  private ObjectId userId;

  private Target target;
  private List<Goal> goals;
  private MonitoringClaimStatus status;
}
