package io.varys.api.model.persistence;

public enum MonitoringClaimStatus {
  CREATED,
  UPDATED,
  QUEUED,
  PROCESSING,
  PROCESSED,
  ABORTED,
  DELETED;
}
