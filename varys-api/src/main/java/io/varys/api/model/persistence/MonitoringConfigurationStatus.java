package io.varys.api.model.persistence;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MonitoringConfigurationStatus {
  @Builder.Default private boolean desired = true;
  @Builder.Default private boolean current = false;
  @Builder.Default private boolean removing = false;
  @Builder.Default private boolean adding = false;
  @Builder.Default private boolean retryLimitExceeded = false;
  @Builder.Default private int retries = 0;
  private Date lastTryAt;
}
