package io.varys.api.model.persistence;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MonitoringConfiguration {

  @JsonSerialize(using = ToStringSerializer.class)
  private ObjectId id;

  @JsonSerialize(using = ToStringSerializer.class)
  private ObjectId monitoringUnitId;

  @JsonSerialize(using = ToStringSerializer.class)
  private ObjectId monitoringClaimId;

  @JsonSerialize(using = ToStringSerializer.class)
  private ObjectId userId;

  private Target target;

  private Goal goal;

  private String probeId;

  private MonitoringConfigurationStatus status;
}
