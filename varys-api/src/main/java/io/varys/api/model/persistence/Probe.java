package io.varys.api.model.persistence;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.types.ObjectId;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Probe {
  // @JsonSerialize(using = ToStringSerializer.class)
  //private ObjectId id;
  @BsonId
  private String id;

  private String artifactId;
  private List<Goal> supportedGoals;
  private List<MonitoringUnitConfigurationPattern> supportedPatterns;
  private List<DataOutput> supportedDataOutputs;
}
