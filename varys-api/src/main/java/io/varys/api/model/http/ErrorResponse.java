package io.varys.api.model.http;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {
  public String title;
  public int status;
  public String type;
  public Map<String, String> details;
}
