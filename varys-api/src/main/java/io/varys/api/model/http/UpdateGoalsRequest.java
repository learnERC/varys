package io.varys.api.model.http;

import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import io.varys.api.model.persistence.Goal;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateGoalsRequest {

  @ArraySchema(schema = @Schema(implementation = Goal.class))
  private List<Goal> goals;
}
