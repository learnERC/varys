package io.varys.api.model.persistence;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MonitoringUnit {
  @JsonSerialize(using = ToStringSerializer.class)
  private ObjectId id;

  private Meta meta;
  private MonitoringUnitConfigurationPattern pattern;
  private String destinationSystem;

  @JsonSerialize(contentUsing = ToStringSerializer.class)
  private List<ObjectId> desiredConfigurations;

  @JsonSerialize(contentUsing = ToStringSerializer.class)
  private List<ObjectId> currentConfigurations;

  private MonitoringUnitStatus status;

  private Map<String, String> metadata;
}
