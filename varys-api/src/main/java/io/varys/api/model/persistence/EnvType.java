package io.varys.api.model.persistence;

public enum EnvType {
  CONTAINER,
  ACCESSIBLE_VM,
  INACCESSIBLE_VM,
}
