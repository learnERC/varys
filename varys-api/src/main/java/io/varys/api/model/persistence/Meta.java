package io.varys.api.model.persistence;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Meta {
  private String version;
  private Date createdAt;
  private Date updatedAt;
  private Date deletedAt;
}
