package io.varys.api.model.persistence;

public enum DataOutput {
  PROMETHEUS,
  ELASTICSEARCH,
}
