package io.varys.api.model.http;

import io.varys.api.model.persistence.MonitoringConfiguration;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class MonitoringUnitFullConfiguration {
  private List<MonitoringConfiguration> configurations;
}
