package io.varys.api.model.http;

import io.javalin.http.HttpResponseException;
import java.util.Map;
import org.eclipse.jetty.http.HttpStatus;

public class PreconditionFailedResponse extends HttpResponseException {

  public PreconditionFailedResponse(String message) {
    super(HttpStatus.PRECONDITION_FAILED_412, message, Map.of());
  }

  public PreconditionFailedResponse(String message, Map<String, String> details) {
    super(HttpStatus.PRECONDITION_FAILED_412, message, details);
  }
}
