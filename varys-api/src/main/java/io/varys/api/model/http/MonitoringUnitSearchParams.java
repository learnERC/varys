package io.varys.api.model.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MonitoringUnitSearchParams {
  private String configurationPattern;
  private String userId;
  private String targetSourceSystem;
  private String targetSourceSystemId;
  private String probeId;
}
