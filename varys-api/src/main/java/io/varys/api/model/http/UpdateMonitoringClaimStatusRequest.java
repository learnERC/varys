package io.varys.api.model.http;

import io.varys.api.model.persistence.MonitoringClaimStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateMonitoringClaimStatusRequest {

  private MonitoringClaimStatus status;
}
