package io.varys.api.model.persistence;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Target {
  private String sourceSystem;
  private String sourceSystemId;
  private EnvType envType;
}
