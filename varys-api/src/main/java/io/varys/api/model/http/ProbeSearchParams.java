package io.varys.api.model.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProbeSearchParams {
  private String supportedPattern;
  private String supportedGoal;
  private String supportedDataOutput;
}
