package io.varys.api.model.persistence;

public enum MonitoringUnitStatus {
  CREATED,
  CONFIGURED,
  QUEUED,
  SYNCING,
  SYNCED,
  TOMBSTONE,
  FAILED,
}
