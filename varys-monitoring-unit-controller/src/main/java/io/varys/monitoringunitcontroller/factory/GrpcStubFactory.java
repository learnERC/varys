package io.varys.monitoringunitcontroller.factory;

import io.avaje.inject.Bean;
import io.avaje.inject.Factory;
import io.grpc.ManagedChannelBuilder;
import io.varys.cloudbridge.grpc.service.MonitoringUnitServiceGrpc;
import io.varys.cloudbridge.grpc.service.MonitoringUnitServiceGrpc.MonitoringUnitServiceBlockingStub;
import io.varys.monitoringunitcontroller.config.VarysCloudBridgeConfig;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

@Factory
@Slf4j
public class GrpcStubFactory {
  private final VarysCloudBridgeConfig varysCloudBridgeConfig;

  @Inject
  public GrpcStubFactory(VarysCloudBridgeConfig varysCloudBridgeConfig) {
    this.varysCloudBridgeConfig = varysCloudBridgeConfig;
    log.trace("Created GrpcStubFactory instance");
  }

  @Bean
  public MonitoringUnitServiceBlockingStub buildMonitoringUnitServiceClient() {
    final var host = varysCloudBridgeConfig.getHost();
    final var port = varysCloudBridgeConfig.getPort();
    final var channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();
    return MonitoringUnitServiceGrpc.newBlockingStub(channel);
  }
}
