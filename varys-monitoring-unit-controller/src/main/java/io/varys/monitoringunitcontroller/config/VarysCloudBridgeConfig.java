package io.varys.monitoringunitcontroller.config;

import io.avaje.config.Config;
import javax.inject.Singleton;
import lombok.Data;

@Singleton
@Data
public class VarysCloudBridgeConfig {
  private final String host;
  private final int port;

  public VarysCloudBridgeConfig() {
    this.host = Config.get("varys.cloudBridge.host");
    this.port = Config.getInt("varys.cloudBridge.port");
  }
}
