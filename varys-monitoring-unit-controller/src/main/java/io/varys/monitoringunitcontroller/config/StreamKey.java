package io.varys.monitoringunitcontroller.config;

public class StreamKey {
  public static final String CREATED = "monitoring-units:created";
  public static final String QUEUED = "monitoring-units:queued";
  public static final String SYNCING = "monitoring-units:syncing";
  public static final String SYNCED = "monitoring-units:synced";
  public static final String TOMBSTONE = "monitoring-units:tombstone";
  public static final String FAILED = "monitoring-units:failed";
  public static final String CONFIGURED = "monitoring-units:configured";
}
