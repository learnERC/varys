package io.varys.monitoringunitcontroller.config;

import io.avaje.config.Config;
import java.util.List;
import javax.inject.Singleton;
import lombok.Data;

@Singleton
@Data
public class RedisConfig {
  private String host;
  private int port;
  private String password;
  private String uri;
  private String consumerGroup;
  private String consumerName;
  private List<String> streamKeys;

  public RedisConfig() {
    this.host = Config.get("redis.host");
    this.port = Config.getInt("redis.port");
    this.password = Config.get("redis.password");
    this.uri = String.format("redis://%s@%s:%s", password, host, port);
    this.consumerGroup = Config.get("redis.consumerGroupName");
    this.consumerName = Config.get("redis.consumerName");
    this.streamKeys = Config.getList().of("redis.streamKeys");
  }
}
