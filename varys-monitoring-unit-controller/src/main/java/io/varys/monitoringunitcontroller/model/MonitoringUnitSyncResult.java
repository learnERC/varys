package io.varys.monitoringunitcontroller.model;

import io.varys.monitoringunitcontroller.enums.MonitoringUnitSyncStatus;
import io.varys.monitoringunitcontroller.enums.ProbeSyncStatus;
import java.util.Map;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MonitoringUnitSyncResult {
  private MonitoringUnitSyncStatus monitoringUnitSyncStatus;
  private Map<String, ProbeSyncStatus> probeSyncStatuses;
}
