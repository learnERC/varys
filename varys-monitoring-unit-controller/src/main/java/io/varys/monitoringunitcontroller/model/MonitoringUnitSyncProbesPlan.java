package io.varys.monitoringunitcontroller.model;

import io.varys.api.jclient.model.MonitoringConfiguration;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MonitoringUnitSyncProbesPlan {
  private Map<String, List<MonitoringConfiguration>> probesToAdd;
  private Map<String, List<MonitoringConfiguration>> probesToUpdate;
  private Map<String, List<MonitoringConfiguration>> probesToDelete;
}
