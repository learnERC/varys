package io.varys.monitoringunitcontroller.service.impl;

import com.google.protobuf.Timestamp;
import io.varys.api.jclient.model.ConfigurationPattern;
import io.varys.api.jclient.model.Goal;
import io.varys.api.jclient.model.MonitoringConfiguration;
import io.varys.api.jclient.model.Target;
import io.varys.cloudbridge.grpc.message.Environment;
import io.varys.cloudbridge.grpc.message.MonitoringUnit;
import io.varys.cloudbridge.grpc.message.ProbeConfiguration;
import io.varys.cloudbridge.grpc.service.MonitoringUnitServiceGrpc.MonitoringUnitServiceBlockingStub;
import io.varys.cloudbridge.grpc.service.SynchronizeReply;
import io.varys.cloudbridge.grpc.service.SynchronizeRequest;
import io.varys.monitoringunitcontroller.enums.MonitoringUnitSyncStatus;
import io.varys.monitoringunitcontroller.enums.ProbeSyncStatus;
import io.varys.monitoringunitcontroller.model.MonitoringUnitSyncProbesPlan;
import io.varys.monitoringunitcontroller.model.MonitoringUnitSyncResult;
import io.varys.monitoringunitcontroller.service.CloudBridgeService;
import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

@Singleton
@Slf4j
public class CloudBridgeServiceImpl implements CloudBridgeService {

  private final MonitoringUnitServiceBlockingStub monitoringUnitServiceBlockingStub;

  @Inject
  public CloudBridgeServiceImpl(
      MonitoringUnitServiceBlockingStub monitoringUnitServiceBlockingStub) {
    this.monitoringUnitServiceBlockingStub = monitoringUnitServiceBlockingStub;
  }

  @Override
  public MonitoringUnitSyncResult trySynchronization(
      io.varys.api.jclient.model.MonitoringUnit monitoringUnit,
      MonitoringUnitSyncProbesPlan syncProbesPlan)
      throws IOException {

    final var monitoringUnitProto = buildMonitoringUnit(monitoringUnit);
    final var probesToAddConfigurationMap =
        buildProbesConfigurationMap(syncProbesPlan.getProbesToAdd());
    final var probesToUpdateConfigurationMap =
        buildProbesConfigurationMap(syncProbesPlan.getProbesToUpdate());
    final var probesToDeleteConfigurationMap =
        buildProbesConfigurationMap(syncProbesPlan.getProbesToDelete());

    final var synchronizeRequest =
        buildSynchronizeRequest(
            monitoringUnitProto,
            probesToAddConfigurationMap,
            probesToUpdateConfigurationMap,
            probesToDeleteConfigurationMap);

    final var synchronizeResult = monitoringUnitServiceBlockingStub.synchronize(synchronizeRequest);
    return buildMonitoringUnitSyncResult(synchronizeResult);
  }

  private SynchronizeRequest buildSynchronizeRequest(
      final MonitoringUnit monitoringUnit,
      final Map<String, ProbeConfiguration> probesToAdd,
      final Map<String, ProbeConfiguration> probesToUpdate,
      final Map<String, ProbeConfiguration> probesToDelete) {
    return SynchronizeRequest.newBuilder()
        .setMonitoringUnit(monitoringUnit)
        .putAllProbesToAdd(probesToAdd)
        .putAllProbesToUpdate(probesToUpdate)
        .putAllProbesToDelete(probesToDelete)
        .build();
  }

  private MonitoringUnitSyncResult buildMonitoringUnitSyncResult(
      final SynchronizeReply synchronizeReply) {
    return MonitoringUnitSyncResult.builder()
        .monitoringUnitSyncStatus(
            MonitoringUnitSyncStatus.valueOf(synchronizeReply.getStatus().name()))
        .probeSyncStatuses(
            buildProbeSyncStatusMap(synchronizeReply.getProbeDeploymentStatusesMap()))
        .build();
  }

  private Map<String, ProbeSyncStatus> buildProbeSyncStatusMap(
      final Map<String, io.varys.cloudbridge.grpc.message.ProbeSyncStatus> probeSyncStatusMap) {
    final Map<String, ProbeSyncStatus> syncStatusMap = new HashMap<>();
    probeSyncStatusMap.forEach(
        (id, status) -> syncStatusMap.put(id, ProbeSyncStatus.valueOf(status.name())));
    return syncStatusMap;
  }

  private MonitoringUnit buildMonitoringUnit(
      final io.varys.api.jclient.model.MonitoringUnit monitoringUnit) {
    final var instant = Instant.now();
    final var lastSyncTryAt =
        Timestamp.newBuilder()
            .setSeconds(instant.getEpochSecond())
            .setNanos(instant.getNano())
            .build();
    return MonitoringUnit.newBuilder()
        .setId(monitoringUnit.getId())
        .setPattern(io.varys.cloudbridge.grpc.message.Pattern.valueOf(monitoringUnit.getPattern().getValue()))
        .setDestinationSystem(monitoringUnit.getDestinationSystem())
        .setLastSyncTryAt(lastSyncTryAt)
        .putAllMetadata(monitoringUnit.getMetadata())
        .build();
  }

  private Map<String, ProbeConfiguration> buildProbesConfigurationMap(
      final Map<String, List<MonitoringConfiguration>> probesMap) {
    final Map<String, ProbeConfiguration> configurationMap = new HashMap<>();

    probesMap.forEach(
        (id, configurations) -> {
          final var configurationsByGoal =
              configurations.stream()
                  .collect(Collectors.groupingBy(MonitoringConfiguration::getGoal));
          configurationsByGoal.forEach(
              (goal, _configurations) -> {
                configurationMap.put(id, buildProbeConfiguration(goal, _configurations));
              });
        });

    return configurationMap;
  }

  private ProbeConfiguration buildProbeConfiguration(
      final Goal goal, final List<MonitoringConfiguration> configurations) {
    final var probeConfigurationBuilder = ProbeConfiguration.newBuilder();
    probeConfigurationBuilder.setGoal(goal.getValue());
    configurations.forEach(
        configuration ->
            probeConfigurationBuilder.addTargets(buildTarget(configuration.getTarget())));
    return probeConfigurationBuilder.build();
  }

  private io.varys.cloudbridge.grpc.message.Target buildTarget(final Target target) {
    return io.varys.cloudbridge.grpc.message.Target.newBuilder()
        .setSourceSystem(target.getSourceSystem())
        .setSourceSystemId(target.getSourceSystemId())
        .setEnvironment(Environment.valueOf(target.getEnvType().getValue()))
        .build();
  }
}
