package io.varys.monitoringunitcontroller.service.impl;

import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;
import io.varys.api.jclient.model.MonitoringConfiguration;
import io.varys.api.jclient.model.MonitoringUnit;
import io.varys.api.jclient.model.MonitoringUnitFullConfiguration;
import io.varys.api.jclient.model.MonitoringUnitStatus;
import io.varys.api.jclient.model.UpdateMonitoringUnitStatusRequest;
import io.varys.monitoringunitcontroller.enums.ProbeSyncStatus;
import io.varys.monitoringunitcontroller.exception.MonitoringConfigurationsReconciliationException;
import io.varys.monitoringunitcontroller.model.MonitoringUnitSyncProbesPlan;
import io.varys.monitoringunitcontroller.service.CloudBridgeService;
import io.varys.monitoringunitcontroller.service.MonitoringUnitService;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

@Singleton
@Slf4j
public class MonitoringUnitServiceImpl implements MonitoringUnitService {

  private final MonitoringUnitApi monitoringUnitApi;
  private final CloudBridgeService cloudBridgeService;

  @Inject
  public MonitoringUnitServiceImpl(
      MonitoringUnitApi monitoringUnitApi,
      CloudBridgeService cloudBridgeService) {
    this.monitoringUnitApi = monitoringUnitApi;
    this.cloudBridgeService = cloudBridgeService;
    log.trace("Created MonitoringUnitService instance");
  }

  @Override
  public MonitoringUnit get(final String monitoringUnitId) {
    try {
      return monitoringUnitApi.getMonitoringUnit(monitoringUnitId);
    } catch (ApiException e) {
      log.error("Failed to get MonitoringUnit: {}", e.getMessage());
      throw new RuntimeException(e.getCause());
    }
  }

  @Override
  public void enqueue(String monitoringUnitId) {
    final var monitoringUnit = get(monitoringUnitId);
    updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.QUEUED);
  }

  @Override
  public MonitoringUnit updateStatus(final String monitoringUnitId, final String eTag, final MonitoringUnitStatus status) {
    final var updateStatusRequest = new UpdateMonitoringUnitStatusRequest().status(status);
    try {
      return monitoringUnitApi.updateMonitoringUnitStatus(monitoringUnitId, eTag, updateStatusRequest);
    } catch (ApiException e) {
      log.error(
          "Failed to update MonitoringUnit {} status to {}: {}",
          monitoringUnitId,
          status,
          e.getResponseBody());
      throw new RuntimeException(e.getCause());
    }
  }

  @Override
  public void sync(final String monitoringUnitId) {
    final var monitoringUnit = get(monitoringUnitId);

    updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.SYNCING);
    log.info("Syncing MonitoringUnit {} ", monitoringUnitId);

    final var desiredConfigurationIds = monitoringUnit.getDesiredConfigurations();

    if (null == desiredConfigurationIds || desiredConfigurationIds.isEmpty()) {
      log.debug("MonitoringUnit {} has no DesiredConfigurations: it is going to be tombstone", monitoringUnitId);
      updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.TOMBSTONE);
      log.info("Tombstone MonitoringUnit {} ", monitoringUnitId);
      return;
    }

    log.info("Computing desired-current configurations diff for Monitoring Unit {}", monitoringUnitId);

    final List<String> currentConfigurationIds = monitoringUnit
        .getCurrentConfigurations() != null ? monitoringUnit
        .getCurrentConfigurations() : Collections.emptyList();

    final var configurationIdsToAdd = desiredConfigurationIds
        .stream()
        .filter(c -> !currentConfigurationIds.contains(c))
        .collect(Collectors.toList());

    final var configurationIdsToRemove = currentConfigurationIds
        .stream()
        .filter(c -> !desiredConfigurationIds.contains(c))
        .collect(Collectors.toList());

    log.info("Computed desired-current configurations diff for MonitoringUnit {}: {} new desired configurations to honor, {} current configurations to dismiss",
        monitoringUnitId, configurationIdsToAdd.size(), configurationIdsToRemove.size());

    if (configurationIdsToAdd.isEmpty() && configurationIdsToRemove.isEmpty()) {
      log.debug("MonitoringUnit {} is up-to-date: no operations need to be performed", monitoringUnitId);
      updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.SYNCED);
      log.info("Synced MonitoringUnit {}", monitoringUnitId);
      return;
    }

    MonitoringUnitFullConfiguration monitoringUnitFullConfiguration;
    try {
      log.debug("Getting MonitoringUnit {} full configuration", monitoringUnitId);
      monitoringUnitFullConfiguration = monitoringUnitApi
          .getMonitoringUnitFullConfiguration(monitoringUnitId);
    } catch (ApiException e) {
      log.error("Failed to get MonitoringUnit {} full configuration: {}", monitoringUnitId, e.getMessage());
      updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.QUEUED);
      log.info("MonitoringUnit {} enqueued again", monitoringUnitId);
      return;
    }

    log.info("Computing execution plan for MonitoringUnit {}", monitoringUnitId);

    final Supplier<Stream<MonitoringConfiguration>> configurationsStream = monitoringUnitFullConfiguration.getConfigurations()::stream;
      final var desiredConfigurations = configurationsStream
          .get()
          .filter(c -> desiredConfigurationIds.contains(c.getId()))
          .collect(Collectors.toList());
      final var currentConfigurations = configurationsStream
          .get()
          .filter(c -> currentConfigurationIds.contains(c.getId()))
          .collect(Collectors.toList());

      final var monitoringUnitSyncProbesPlan = computeSyncProbesPlan(desiredConfigurations, currentConfigurations);
      log.info("Computed execution plan for MonitoringUnit {}: {} Probes to add, {} Probes to update, {} Probes to delete",
          monitoringUnitId, monitoringUnitSyncProbesPlan.getProbesToAdd().size(), monitoringUnitSyncProbesPlan.getProbesToUpdate().size(), monitoringUnitSyncProbesPlan.getProbesToUpdate().size());

      try {
        monitoringUnitApi.setDesiredConfigurationsStatusAdding(monitoringUnitId, configurationIdsToAdd);
      } catch (ApiException e) {
        log.error("Failed to set desired configurations status adding for MonitoringUnit {}", monitoringUnitId);
      }

      try {
        monitoringUnitApi.setCurrentConfigurationsStatusRemoving(monitoringUnitId, configurationIdsToRemove);
      } catch (ApiException e) {
        log.error("Failed to set current configurations status removing for MonitoringUnit {}", monitoringUnitId);
      }

      try {
        log.info("Synchronizing MonitoringUnit {} via Cloud Bridge", monitoringUnitId);
        final var monitoringUnitSyncResult =
            cloudBridgeService.trySynchronization(monitoringUnit, monitoringUnitSyncProbesPlan);

        switch (monitoringUnitSyncResult.getMonitoringUnitSyncStatus()) {
          case SUCCESS -> {
            log.info("Synchronized MonitoringUnit {} via Cloud Bridge", monitoringUnitId);
            try {
              reconcileAfterSyncSuccess(monitoringUnitId, desiredConfigurationIds);
            } catch (MonitoringConfigurationsReconciliationException e) {
              log.error("Failed configuration reconciliation after synchronization: {}", e.getCause().getMessage());
              //FIXME: WHAT COULD I DO HERE?!...
            }
            updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.SYNCED);
            log.info("MonitoringUnit {} synced", monitoringUnitId);
          }
          case FAILURE -> {
            log.info("MonitoringUnit {} synchronized with soft errors via Cloud Bridge", monitoringUnitId);
            final var failedProbeIds = monitoringUnitSyncResult
                .getProbeSyncStatuses()
                .entrySet()
                .stream()
                .filter(e -> !ProbeSyncStatus.SYNCED.equals(e.getValue()))
                .map(Entry::getKey)
                .collect(Collectors.toList());
            try {
              reconcileAfterSyncFailure(monitoringUnitId, desiredConfigurations, currentConfigurations, failedProbeIds);
            } catch (MonitoringConfigurationsReconciliationException e) {
              log.error("Failed configuration reconciliation after synchronization soft error: {}", e.getCause().getMessage());
              //FIXME: WHAT COULD I DO HERE?!...
            }
            updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.SYNCED);
            log.info("MonitoringUnit {} synced with soft errors, some configurations will be retried on the next sync", monitoringUnitId);
          }
          case DIRTY_FAILURE, TIMED_OUT -> {
            log.info("MonitoringUnit {} not synchronized due to hard errors or timeout via Cloud Bridge", monitoringUnitId);
            final var failedProbeIds = monitoringUnitSyncResult
                .getProbeSyncStatuses()
                .entrySet()
                .stream()
                .filter(e -> !ProbeSyncStatus.SYNCED.equals(e.getValue()))
                .map(Entry::getKey)
                .collect(Collectors.toList());
            try {
              reconcileAfterSyncDirtyFailure(monitoringUnitId, desiredConfigurations, currentConfigurations, failedProbeIds);
              updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.FAILED);
              log.info("MonitoringUnit {} failed", monitoringUnitId);
            } catch (MonitoringConfigurationsReconciliationException e) {
              log.error("Failed configuration reconciliation after synchronization dirty failure: {}", e.getCause().getMessage());
              updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.FAILED);
              log.info("MonitoringUnit {} failed", monitoringUnitId);
            }
          }
        }
      } catch (IOException e) {
        log.error("Cloud Bridge error: {}", e.getMessage());
        log.error("Failed to synchronize MonitoringUnit {}", monitoringUnitId);
        updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.QUEUED);
        log.info("MonitoringUnit {} enqueued again", monitoringUnitId);
      }
  }

  @Override
  public void delete(String monitoringUnitId) {
    final var monitoringUnit = get(monitoringUnitId);
    log.info("Deleting MonitoringUnit {}", monitoringUnitId);
    try {
      log.debug("Getting MonitoringUnit {} full configuration", monitoringUnitId);
      final var monitoringUnitFullConfiguration = monitoringUnitApi
          .getMonitoringUnitFullConfiguration(monitoringUnitId);

      log.debug("Computing execution plan for MonitoringUnit {}", monitoringUnitId);
      final var monitoringUnitSyncProbesPlan = computeSyncProbesPlan(List.of(), monitoringUnitFullConfiguration.getConfigurations());
      log.debug("Computed execution plan for MonitoringUnit {}: {} Probes to add, {} Probes to update, {} Probes to delete",
          monitoringUnitId, monitoringUnitSyncProbesPlan.getProbesToAdd().size(), monitoringUnitSyncProbesPlan.getProbesToUpdate().size(), monitoringUnitSyncProbesPlan.getProbesToDelete().size());

      try {
        log.debug("Deleting MonitoringUnit {} via Cloud Bridge", monitoringUnitId);
        final var monitoringUnitSyncResult =
            cloudBridgeService.trySynchronization(monitoringUnit, monitoringUnitSyncProbesPlan);
        switch (monitoringUnitSyncResult.getMonitoringUnitSyncStatus()) {
          case SUCCESS -> {
            log.info("Deleted MonitoringUnit {} via Cloud Bridge", monitoringUnitId);
            monitoringUnitApi.deleteMonitoringUnit(monitoringUnitId);
            log.info("Deleted MonitoringUnit {}", monitoringUnitId);
          }
          case FAILURE, DIRTY_FAILURE, TIMED_OUT -> {
            log.error("Failed to delete MonitoringUnit {} via Cloud Bridge", monitoringUnitId);
            updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.TOMBSTONE);
            log.info("Tombstone MonitoringUnit {}", monitoringUnitId);
          }
        }
      } catch (IOException e) {
        log.error("Cloud Bridge error: {}", e.getMessage());
        log.error("Failed to delete MonitoringUnit {}: {}", monitoringUnitId, e.getMessage());
        updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.TOMBSTONE);
        log.info("Tombstone MonitoringUnit {}", monitoringUnitId);
      }
    } catch (ApiException e) {
      log.error("API Service error: {}", e.getMessage());
      log.error("Failed to delete MonitoringUnit {}: {}", monitoringUnitId, e.getMessage());
      updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.TOMBSTONE);
      log.info("Tombstone MonitoringUnit {}", monitoringUnitId);
    }
  }

  @Override
  public void cleanFailed(String monitoringUnitId) {
    final var monitoringUnit = get(monitoringUnitId);
    log.info("Cleaning failed MonitoringUnit {}", monitoringUnitId);
    try {
      log.debug("Getting MonitoringUnit {} full configuration", monitoringUnitId);
      final var monitoringUnitFullConfiguration = monitoringUnitApi
          .getMonitoringUnitFullConfiguration(monitoringUnitId);

      log.debug("Computing execution plan for MonitoringUnit {}", monitoringUnitId);
      final var monitoringUnitSyncProbesPlan = computeSyncProbesPlan(List.of(), monitoringUnitFullConfiguration.getConfigurations());
      log.debug("Computed execution plan for MonitoringUnit {}: {} Probes to add, {} Probes to update, {} Probes to delete",
          monitoringUnitId, monitoringUnitSyncProbesPlan.getProbesToAdd().size(), monitoringUnitSyncProbesPlan.getProbesToUpdate().size(), monitoringUnitSyncProbesPlan.getProbesToDelete().size());

      try {
        log.info("Cleaning failed MonitoringUnit {} via Cloud Bridge", monitoringUnitId);
        final var monitoringUnitSyncResult =
            cloudBridgeService.trySynchronization(monitoringUnit, monitoringUnitSyncProbesPlan);
        switch (monitoringUnitSyncResult.getMonitoringUnitSyncStatus()) {
          case SUCCESS -> {
            log.info("Cleaned failed MonitoringUnit {} via CloudBridge", monitoringUnitId);
            try {
              monitoringUnitApi.setCurrentConfigurations(monitoringUnitId, List.of());
            } catch (ApiException e) {
              log.error("Failed to set current configurations for MonitoringUnit {}", monitoringUnitId);
              log.error("Failed to clean MonitoringUnit {}", monitoringUnitId);
              updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.FAILED);
              log.info("Failed MonitoringUnit {}", monitoringUnitId);
            }
            updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.QUEUED);
            log.info("MonitoringUnit {} cleaned and queued for next sync", monitoringUnitId);
          }
          case FAILURE, DIRTY_FAILURE, TIMED_OUT -> {
            log.error("Failed MonitoringUnit {} cannot be cleaned via Cloud Bridge", monitoringUnitId);
            updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.FAILED);
            log.info("Failed MonitoringUnit {}", monitoringUnitId);
          }
        }
      } catch (IOException e) {
        log.error("Cloud Bridge error: {}", e.getMessage());
        log.error("Failed to clean MonitoringUnit {}", monitoringUnitId);
        updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.FAILED);
        log.info("Failed MonitoringUnit {}", monitoringUnitId);
      }
    } catch (ApiException e) {
      log.error("API Service error: {}", e.getMessage());
      log.error("Failed to clean MonitoringUnit {}", monitoringUnitId);
      updateStatus(monitoringUnitId, monitoringUnit.getMeta().getVersion(), MonitoringUnitStatus.FAILED);
      log.info("Failed MonitoringUnit {}", monitoringUnitId);
    }
  }

  private MonitoringUnitSyncProbesPlan computeSyncProbesPlan(List<MonitoringConfiguration> desiredConfigurations, List<MonitoringConfiguration> currentConfigurations) {

    final Supplier<Stream<MonitoringConfiguration>> desiredConfigurationsStream = desiredConfigurations::stream;

    final var probesToAdd = desiredConfigurationsStream.get()
        .filter(
            desiredConfiguration ->
                !desiredConfiguration.getStatus().getRetryLimitExceeded() &&
                currentConfigurations
                    .stream()
                    .noneMatch(
                        currentConfiguration -> currentConfiguration.getProbeId().equals(desiredConfiguration.getProbeId())))
        .collect(Collectors.groupingBy(MonitoringConfiguration::getProbeId));

    final var probesToDelete = currentConfigurations.stream()
        .filter(
            currentConfiguration ->
                desiredConfigurations.stream()
                    .noneMatch(
                        desiredConfiguration -> desiredConfiguration.getProbeId().equals(currentConfiguration.getProbeId())))
        .collect(Collectors.groupingBy(MonitoringConfiguration::getProbeId));

    final var probesToUpdate =  desiredConfigurationsStream.get()
        .filter(
            desiredConfiguration ->
                !desiredConfiguration.getStatus().getRetryLimitExceeded() &&
                currentConfigurations.stream()
                    .anyMatch(
                        currentConfiguration -> desiredConfiguration.getProbeId().equals(currentConfiguration.getProbeId())
                            && (!desiredConfiguration.getTarget().equals(currentConfiguration.getTarget())
                            || !desiredConfiguration.getGoal().equals(currentConfiguration.getGoal()))))
        .collect(Collectors.groupingBy(MonitoringConfiguration::getProbeId));

    return MonitoringUnitSyncProbesPlan
        .builder()
        .probesToAdd(probesToAdd)
        .probesToUpdate(probesToUpdate)
        .probesToDelete(probesToDelete)
        .build();
  }

  private void reconcileAfterSyncSuccess(final String monitoringUnitId, final List<String> desiredConfigurationIds)
      throws MonitoringConfigurationsReconciliationException {
    try {
      monitoringUnitApi.setCurrentConfigurations(monitoringUnitId, desiredConfigurationIds);
    } catch (ApiException e) {
      log.error("Failed to set current configurations for MonitoringUnit {}", monitoringUnitId);
      throw new MonitoringConfigurationsReconciliationException(e.getCause());
    }
  }

  private void reconcileAfterSyncFailure(
      final String monitoringUnitId,
      final List<MonitoringConfiguration> desiredConfigurations,
      final List<MonitoringConfiguration> currentConfigurations,
      final List<String> failedProbeIds) throws MonitoringConfigurationsReconciliationException {
    final var failedConfigurations = computeFailedConfigurations(desiredConfigurations, currentConfigurations, failedProbeIds);
    try {
      final var failedConfigurationIds = failedConfigurations
          .stream()
          .map(MonitoringConfiguration::getId)
          .collect(Collectors.toList());
       monitoringUnitApi.incrementDesiredConfigurationsStatusRetries(monitoringUnitId, failedConfigurationIds);
    } catch (ApiException e) {
      log.error("Failed to increment desired configurations retries for MonitoringUnit {}", monitoringUnitId);
      throw new MonitoringConfigurationsReconciliationException(e.getCause());
    }
    final var successfulConfigurations = computeSuccessfulConfigurations(failedConfigurations, desiredConfigurations);
    final var currentConfigurationIds = successfulConfigurations.stream().map(MonitoringConfiguration::getId).collect(Collectors.toList());
    try {
      monitoringUnitApi.setCurrentConfigurations(monitoringUnitId, currentConfigurationIds);
    } catch (ApiException e) {
      log.error("Failed to set current configurations for MonitoringUnit {}", monitoringUnitId);
      throw new MonitoringConfigurationsReconciliationException(e.getCause());
    }
  }

  private void reconcileAfterSyncDirtyFailure(
      String monitoringUnitId, final List<MonitoringConfiguration> desiredConfigurations,
      final List<MonitoringConfiguration> currentConfigurations,
      final List<String> failedProbeIds) throws MonitoringConfigurationsReconciliationException {    final var failedConfigurations = computeFailedConfigurations(desiredConfigurations, currentConfigurations, failedProbeIds);
    try {
      final var failedConfigurationIds = failedConfigurations
          .stream()
          .map(MonitoringConfiguration::getId)
          .collect(Collectors.toList());

      // If current configurations is empty, this is the first time we are synchronizing this MonitoringUnit,
      // thus we want to make retries like it was a soft error
      if (currentConfigurations.isEmpty()) {
        monitoringUnitApi.incrementDesiredConfigurationsStatusRetries(monitoringUnitId, failedConfigurationIds);
      } else {
        monitoringUnitApi.setDesiredConfigurationsStatusRetryLimitExceeded(monitoringUnitId, failedConfigurationIds);
      }
    } catch (ApiException e) {
      log.error("Failed to set desired configurations retry limit exceeded or increment retries for MonitoringUnit {}", monitoringUnitId);
      throw new MonitoringConfigurationsReconciliationException(e.getCause());
    }
  }

  private List<MonitoringConfiguration> computeFailedConfigurations(
      final List<MonitoringConfiguration> desiredConfigurations,
      final List<MonitoringConfiguration> currentConfigurations,
      final List<String> failedProbes) {

    return desiredConfigurations.stream()
        .filter(
            desiredConfiguration ->
                failedProbes.contains(desiredConfiguration.getProbeId())
                    && currentConfigurations.stream()
                    .noneMatch(
                        currentConfiguration -> desiredConfiguration.getId().equals(currentConfiguration.getId())))
        .collect(Collectors.toList());
  }

  private List<MonitoringConfiguration> computeSuccessfulConfigurations(
      final List<MonitoringConfiguration> failedConfigurations,
      final List<MonitoringConfiguration> desiredConfigurations) {
    return desiredConfigurations.stream()
        .filter(
            desiredConfiguration -> failedConfigurations.stream()
                .noneMatch(
                    failedConfiguration -> desiredConfiguration.getId().equals(failedConfiguration.getId())))
        .collect(Collectors.toList());
  }
}
