package io.varys.monitoringunitcontroller.service;

import io.varys.api.jclient.model.MonitoringUnit;
import io.varys.api.jclient.model.MonitoringUnitStatus;

public interface MonitoringUnitService {

  MonitoringUnit get(String monitoringUnitId);

  void enqueue(String monitoringUnitId);

  MonitoringUnit updateStatus(String monitoringUnitId, String eTag, MonitoringUnitStatus status);

  void sync(String monitoringUnitId);

  void delete(String monitoringUnitId);

  void cleanFailed(String monitoringUnitId);
}
