package io.varys.monitoringunitcontroller.service;

import io.varys.api.jclient.model.ConfigurationPattern;
import io.varys.api.jclient.model.MonitoringUnit;
import io.varys.monitoringunitcontroller.model.MonitoringUnitSyncProbesPlan;
import io.varys.monitoringunitcontroller.model.MonitoringUnitSyncResult;
import java.io.IOException;

public interface CloudBridgeService {
  MonitoringUnitSyncResult trySynchronization(
      MonitoringUnit monitoringUnit,
      MonitoringUnitSyncProbesPlan syncProbesPlan)
      throws IOException;
}
