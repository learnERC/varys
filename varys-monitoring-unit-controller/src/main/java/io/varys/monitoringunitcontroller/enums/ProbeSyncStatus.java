package io.varys.monitoringunitcontroller.enums;

public enum ProbeSyncStatus {
  SYNCED,
  OUT_OF_SYNC,
  DIRTY,
}
