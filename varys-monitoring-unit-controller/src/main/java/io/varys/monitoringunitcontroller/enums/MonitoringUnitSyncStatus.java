package io.varys.monitoringunitcontroller.enums;

public enum MonitoringUnitSyncStatus {
  SUCCESS,
  FAILURE,
  DIRTY_FAILURE,
  TIMED_OUT
}
