package io.varys.monitoringunitcontroller.exception;

public class MonitoringConfigurationsReconciliationException extends Exception {

  public MonitoringConfigurationsReconciliationException() {}

  public MonitoringConfigurationsReconciliationException(String message) {
    super(message);
  }

  public MonitoringConfigurationsReconciliationException(String message, Throwable cause) {
    super(message, cause);
  }

  public MonitoringConfigurationsReconciliationException(Throwable cause) {
    super(cause);
  }
}