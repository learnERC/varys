package io.varys.monitoringunitcontroller;

import io.lettuce.core.Consumer;
import io.lettuce.core.RedisClient;
import io.lettuce.core.StreamMessage;
import io.lettuce.core.XReadArgs.StreamOffset;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.varys.monitoringunitcontroller.config.RedisConfig;
import io.varys.monitoringunitcontroller.config.StreamKey;
import io.varys.monitoringunitcontroller.service.MonitoringUnitService;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public class Controller {

  private final RedisConfig redisConfig;
  private final RedisClient redisClient;
  private final MonitoringUnitService monitoringUnitService;
  private StatefulRedisConnection<String, String> redisConnection;
  private RedisCommands<String, String> syncCommands;

  @Inject
  public Controller(
      RedisConfig redisConfig,
      RedisClient redisClient,
      MonitoringUnitService monitoringUnitService) {
    this.redisConfig = redisConfig;
    this.redisClient = redisClient;
    this.monitoringUnitService = monitoringUnitService;
    log.trace("Created Controller instance");
  }

  @PostConstruct
  public void onStartup() {
    log.trace("Controller connecting to Redis");
    this.redisConnection = redisClient.connect();
    this.syncCommands = redisConnection.sync();
    log.trace("Controller connected to Redis");
  }

  @PreDestroy
  public void onShutdown() {
    log.trace("Controller closing connection to Redis");
    redisConnection.close();
    redisClient.shutdown();
    log.trace("Controller closed connection to Redis");
  }

  public void run() {
    final StreamOffset<String>[] streams =
        redisConfig.getStreamKeys().stream()
            .map(StreamOffset::lastConsumed)
            .toArray(StreamOffset[]::new);

    log.info("Controller started: waiting for new messages on streams {}", redisConfig.getStreamKeys());

    while (true) {

      List<StreamMessage<String, String>> messages =
          syncCommands.xreadgroup(
              Consumer.from(redisConfig.getConsumerGroup(), redisConfig.getConsumerName()),
              streams);

      if (!messages.isEmpty()) {
        for (StreamMessage<String, String> message : messages) {
          final var stream = message.getStream();
          final var id = message.getId();
          final var body = message.getBody();

          log.debug("Read message with id {} on stream {}", id, stream);
          try {
            switch (stream) {
              case StreamKey.CREATED:
              case StreamKey.SYNCING: {
                  final var monitoringUnitId = body.get("id");
                  // Confirm that the message has been processed using XACK
                  syncCommands.xack(stream, redisConfig.getConsumerGroup(), id);
                  log.debug("Ack message with id {} on stream {}", id, stream);
                  break;
                }
              case StreamKey.CONFIGURED:
              case StreamKey.SYNCED: {
                  final var monitoringUnitId = body.get("id");
                  monitoringUnitService.enqueue(monitoringUnitId);
                  // Confirm that the message has been processed using XACK
                  syncCommands.xack(stream, redisConfig.getConsumerGroup(), id);
                  log.debug("Ack message with id {} on stream {}", id, stream);
                  break;
                }
              case StreamKey.QUEUED:
                {
                  final var monitoringUnitId = body.get("id");
                  // Confirm that the message has been processed using XACK
                  syncCommands.xack(stream, redisConfig.getConsumerGroup(), id);
                  log.debug("Ack message with id {} on stream {}", id, stream);
                  monitoringUnitService.sync(monitoringUnitId);
                  break;
                }
              case StreamKey.TOMBSTONE:
                {
                  final var monitoringUnitId = body.get("id");
                  monitoringUnitService.delete(monitoringUnitId);
                  // Confirm that the message has been processed using XACK
                  syncCommands.xack(stream, redisConfig.getConsumerGroup(), id);
                  log.debug("Ack message with id {} on stream {}", id, stream);
                  break;
                }
              case StreamKey.FAILED:
                {
                  final var monitoringUnitId = body.get("id");
                  monitoringUnitService.cleanFailed(monitoringUnitId);
                  // Confirm that the message has been processed using XACK
                  syncCommands.xack(stream, redisConfig.getConsumerGroup(), id);
                  log.debug("Ack message with id {} on stream {}", id, stream);
                  break;
                }
              default:
                // Confirm that the message has been processed using XACK
                syncCommands.xack(stream, redisConfig.getConsumerGroup(), id);
                log.debug("Ack message with id {} on stream {}", id, stream);
                break;
            }
          } catch (Exception e) {
            log.error(
                "Something really bad happened: {}", e.getMessage());
          }
        }
      }
    }
  }
}
