# A Monitoring Framework for Declarative Probes Deployment in the Cloud

## Requirements
- Docker and Docker Compose
- Ansible
- JDK 11 and JDK 15 (if you plan to manually run components)
- Maven (if you plan to manually run components)
- Microsoft Azure account (for VM-based experiments)
- Kubernetes cluster (for container-based experiments)
- Elasticsearch 7
- PostgreSQL 9.5
- Python 3 and the `requests` package
- jq (see [how to install](https://stedolan.github.io/jq/download/))
- Kubernetes CLI (kubectl) (see [how to install](https://kubernetes.io/docs/tasks/tools/install-kubectl/))
- Azure CLI (see [how to install](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli))

## Getting started
1. Create an SSH keypair 
2. Create a VM on Microsoft Azure to host PostgreSQL
    1. Use the SSH keypair created before
    2. Install PostgreSQL 9.5
    3. Make it accessible remotely
    4. Create a PostgreSQL super role:
        1. username: `varys`
        2. password: `varys`
    5. Create a database named `varys`
3. Create a PostgreSQL instance on your Kubernetes cluster:
    1. Use the `postgresql.yml` manifest provided within `experiments/scripts/kubernetes` directory
4. Create an Elasticsearch instance without authN/authZ (this must be reachable by your VMs and containers platforms)
5. Setup the Cloud Bridge component to access the Cloud systems:
    1. Copy the SSH keypair within `varys-cloud-bridge/src/main/resources`
    2. Create the `azureauth.properties` file within `varys-cloud-bridge/src/main/resources` to access your Microsoft Azure resources
    3. Create a `kubeconfig` file within `varys-cloud-bridge/src/main/resources` to access your Kubernetes cluster
    4. Create an `application.yml` file within `varys-cloud-bridge/src/main/resources` and change the values
 accordingly to your setup and credentials (see `application.yml.example` in the same directory)
6. Create `.env` files within `varys-api`, `varys-monitoring-claim-controller`, `varys-monitoring-unit-controller`, `varys-cloud-bridge` (see `.env.example` in each of the directories)
(__REQUIRED ONLY IF YOU DECIDED TO RUN ALL THE COMPONENTS AS DOCKER CONTAINERS__)
7. Build the components with Maven or with Docker (example for Docker, for Maven run a simple `mvn install`):
    1. Build `varys-api-jclient` and `varys-cloud-bridge-proto`:
       ```bash
       $ cd varys-cloud-bridge-proto && docker build -t varys/cloud-bridge-proto:1.0.0-SNAPSHOT . && cd ..
       $ cd varys-api-jclient && docker build -t varys/api-jclient:1.0.0-SNAPSHOT . && cd ..
       ```
    2. Build all the other components (example for Docker, for Maven run a simple `mvn install` in each of the directories):
       ```bash
       $ docker-compose build
       ```
8. Install Python `requests` package (e.g., `pip3 install requests`)
9. Install jq, see [how to install](https://stedolan.github.io/jq/download/)

## Experiments 

### Common steps
```bash
$ docker-compose up -d mongo redis
$ ./mongodb-init-rs.sh
$ ./redis-init-streams.sh
$ docker-compose up -d api monitoring-claim-controller monitoring-unit-controller cloud-bridge
```

You can now follow component logs to view messages for collecting timings:
```bash
$ docker-compose logs --follow {monitoring-claim-controller|monitoring-unit-controller|cloud-bridge}
```
If you run the components manually (e.g., `java -jar api-1.0.0-SNAPSHOT.jar`), you will have all the logs
printed to the stdout.

When you will run the experiments as described below, you can follow the process specified by the `INFO`
level messages.

> N.B.: We strongly suggest you to stop the `monitoring-unit-controller` when it starts looping fast
because it synced the unit(s). So that, you can collect timings easily.
>
> Then, you can run the `monitoring-unit-controller` again when the _clean-*.sh_ script has been executed.

### Probes deployment experiments
#### Azure
```bash
$ cd experiments/scripts/azure
$ ./run-probes-deployment.sh YOUR_API_URL
```

```bash
$ ./clean-run-probes-deployment.sh YOUR_POSTGRESQL_IP YOUR_ELASTICSEARCH_IP
```

#### Kubernetes
```bash
$ cd experiments/scripts/kubernetes
$ ./run-probes-deployment.sh YOUR_API_URL
```

```bash
$ ./clean-run-probes-deployment.sh YOUR_ELASTICSEARCH_IP
```

### Error handling experiments
#### Azure
```bash
$ cd experiments/scripts/azure
$ ./run-error-handling.sh YOUR_API_URL
```

```bash
$ ./clean-run-error-handling.sh YOUR_POSTGRESQL_IP YOUR_ELASTICSEARCH_IP
```

#### Kubernetes
```bash
$ cd experiments/scripts/kubernetes
$ ./run-error-handling.sh YOUR_API_URL
```
> N.B.: This experiment does not require to stop the `monitoring-unit-controller` to read the timings
> easily.
>
> The error handling procedure completely remove the unit after 5 attempts, and the controller returns on idle.

```bash
$ ./clean-run-error-handling.sh YOUR_ELASTICSEARCH_IP
```

### Scalability experiments
#### Azure
##### Operators
```bash
$ cd experiments/scripts/azure
$ python3 run-operators-scalability.py YOUR_API_URL NUMBER_OPERATORS
```

##### Indicators
```bash
$ cd experiments/scripts/azure
$ python3 run-indicators-scalability.py YOUR_API_URL NUMBER_INDICATORS
```

```bash
$ ./clean-run-probes-deployment.sh YOUR_POSTGRESQL_IP YOUR_ELASTICSEARCH_IP
```

#### Kubernetes
##### Operators
```bash
$ cd experiments/scripts/kubernetes
$ python3 run-operators-scalability.py YOUR_API_URL NUMBER_OPERATORS
```

##### Indicators
```bash
$ cd experiments/scripts/kubernetes
$ python3 run-indicators-scalability.py YOUR_API_URL NUMBER_INDICATORS
```

```bash
$ ./clean-run-probes-deployment.sh YOUR_ELASTICSEARCH_IP
```


### JCatascopia experiments
We made some modifications to [JCatascopia](https://github.com/dtrihinas/JCatascopia) in order to make it
buildable with newer versions of Java JDK, and we also developed a simple JCatascopia Agent Client taking inspiration from their example class available
[here](https://github.com/dtrihinas/JCatascopia/blob/master/JCatascopia/JCatascopia-Agent/src/main/java/eu/celarcloud/jcatascopia/agent/client/JCatascopiaAgentClient.java).
The client has been used for running experiments to deploy JCatascopia probes at runtime.

Moreover, we created Ansible roles to easily deploy JCatascopia components and our agent client
from a private git repository. Thus, you can exploit them to setup a working JCatascopia setup.

We conveniently upload our forked repository as zip archive (`JCatascopia-master.zip`) to the experiments directory.

#### Setup
In our experiments, we deploy the JCatascopia Server in a dedicated VM, and then we deploy the JCatascopia Agent, 
the JCatascopia-Probe-Repo and the JCatascopia Agent Client in the PostgreSQL VM.

Feel free to use the Ansible roles we provided to recreate the same setup, or you can do it manually.

#### Run
Assuming you are connected to the PostgreSQL VM where the JCatascopia Angent is running, you can execute:

```bash
java -jar AGENT_CLIENT_JAR localhost 4243 YOUR_POSTGRES_PROBE_JAR_FULL_PATH PostgresProbe
```
where`AGENT_CLIENT_JAR` and `YOUR_POSTGRES_PROBE_JAR_FULL_PATH` should be properly fixed with the correct paths
(e.g., `/home/user/JCatascopia/JCatascopia-Probe-Repo/JCatascopia-Probe-Repo/PostgresProbe/target/PostgresProbe-0.0.1-SNAPSHOT.jar`
for the `YOUR_POSTGRES_PROBE_JAR_FULL_PATH`).

If the JCatascopia Agent deployed the probe successfully you will have an `OK` response and you can
compute the timings by the JCatascopia Agent Client log messages printed to stdout.



