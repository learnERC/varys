#!/bin/bash

help()
{
   # Display Help
   printf "Clean a Kubernetes error handling run.\n\n"
   printf "Usage: clean-run-error-handling.sh [-h]\n\n"
   printf "Options:\n"
   printf "h     Print this help.\n"
}

# main script
while getopts ":h" option; do
   case $option in
      h) # display help
         help
         exit;;
     \?) # incorrect option
         printf "Error: Invalid option"
         exit;;
   esac
done

printf "Cleaning MongoDB...\n\n"
./../common/mongodb-drop-db.sh
printf "\nMongoDB cleaned\n"

printf "Cleaning Redis streams...\n\n"
./../common/clean-redis.sh
printf "\nRedis cleaned\n"