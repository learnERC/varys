#!/usr/bin/env python3

import random
import requests
import sys

indicator_probe_prefixes = {
  'CPU_CONSUMPTION': 'metricbeat_cpu',
  'MEMORY_CONSUMPTION': 'metricbeat_memory',
  'DATABASE': 'metricbeat_postgresql'
}

def create_user(api_url):
  payload = {
    'username': 'varys-test1'
  }
  url = '{}/users'.format(api_url)
  r = requests.post(url, json=payload)
  user_id = r.json()['id']
  return user_id


def init_probes_catalog(api_url, indicators_probes):
  url = '{}/probes'.format(api_url)
  for item in indicators_probes.items():
    payload = {
      'artifactId': item[1],
      'supportedPatterns': [
        'INTERNAL_PROBES',
        'SHARED_SIDECAR_SINGLE_PROBE'
      ],
      'supportedGoals': [
        item[0]
      ],
      'supportedDataOutputs': [
        'ELASTICSEARCH'
      ]
    }
    requests.post(url, json=payload)


def build_indicators_probes():
  indicators_probes = {}
  for i in range(10):
    for item in indicator_probe_prefixes.items():
      indicator = "{}_{}".format(item[0], i+1)
      probe = "{}_{}".format(item[1], i+1)
      indicators_probes[indicator] = probe
  return indicators_probes

def build_indicators(number_of_indicators, indicators_probes):
  indicators = []
  all_indicators = list(indicators_probes.keys())
  for i in range(number_of_indicators):
    indicators.append(all_indicators[i])
  return indicators

def main():
  if len(sys.argv) != 3:
    sys.exit('Missing required arguments: API_URL NUMBER_INDICATORS')

  api_url = sys.argv[1]
  number_of_indicators = int(sys.argv[2])

  if number_of_indicators > 30:
    sys.exit('NUMBER_INDICATORS cannot be > 30.')

  indicators_probes = build_indicators_probes()
  init_probes_catalog(api_url, indicators_probes)
  user_id = create_user(api_url)

  indicators = build_indicators(number_of_indicators, indicators_probes)
  monitoring_claim = {
    'userId': user_id,
    'goals': indicators,
    'target': {
      'sourceSystemId': 'postgres',
      'sourceSystem': 'kubernetes',
      'envType': 'CONTAINER'
    }
  }

  url = '{}/monitoring-claims'.format(api_url)
  requests.post(url, json=monitoring_claim)


if __name__ == '__main__':
    main()