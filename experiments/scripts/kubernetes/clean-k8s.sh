#!/bin/bash

printf "\nAre you sure? This script is going to delete ALL the resources with label app.kubernetes.io/part-of=varys in ALL the namespaces!\n\n"

select yn in "Yes" "No"; do
    case $yn in
        Yes )
          printf "\nReally? This command is going to be executed: kubectl delete \"\$(kubectl api-resources --namespaced=true --verbs=delete -o name | tr \"\n\" \",\" | sed -e 's/,$//')\" --all-namespaces -l app.kubernetes.io/part-of=varys \n\n"
          select yn in "Yes" "No"; do
              case $yn in
                  Yes )
                    kubectl delete "$(kubectl api-resources --namespaced=true --verbs=delete -o name | tr "\n" "," | sed -e 's/,$//')" --all-namespaces -l app.kubernetes.io/part-of=varys
                    break
                    ;;
                  No )
                    break
                    ;;
                  * )
                    echo "Invalid entry: select 1 (Yes) or 2 (No)"
                    break
                    ;;
              esac
          done
          break
          ;;
        No )
          break
          ;;
        * )
          echo "Invalid entry: select 1 (Yes) or 2 (No)"
          break
          ;;
    esac
done