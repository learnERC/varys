#!/bin/bash

help()
{
   # Display Help
   printf "Clean a Kubernetes probes deployment run.\n\n"
   printf "Usage: clean-run-probes-deployment.sh [-h] YOUR_ELASTICSEARCH_IP\n\n"
   printf "Options:\n"
   printf "h     Print this help.\n"
}

# main script
while getopts ":h" option; do
   case $option in
      h) # display help
         help
         exit;;
     \?) # incorrect option
         printf "Error: Invalid option"
         exit;;
   esac
done

if [ "$#" -ne 1 ]
then
  printf "Missing required argument!\n"
  printf "Usage: clean-run-probes-deployment.sh [-h] YOUR_ELASTICSEARCH_IP\n"
  exit 1
fi

printf "Cleaning Kubernetes...\n"
./clean-k8s.sh
printf "\nKubernetes cleaned\n\n"

printf "Cleaning Elasticsearch...\n\n"
./../common/clean-elasticsearch.sh $1
printf "\n\nElasticsearch cleaned\n\n"

printf "Cleaning MongoDB...\n\n"
./../common/mongodb-drop-db.sh
printf "\nMongoDB cleaned\n"

printf "Cleaning Redis streams...\n\n"
./../common/clean-redis.sh
printf "\nRedis cleaned\n"