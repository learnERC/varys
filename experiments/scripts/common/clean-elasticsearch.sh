#!/bin/bash

if [ "$#" -ne 1 ]
then
  printf "Missing required argument!\n"
  printf "Usage: clean-elasticsearch.sh [-h] YOUR_ELASTICSEARCH_IP\n"
  exit 1
fi

curl -X DELETE "http://$1:9200/_all"