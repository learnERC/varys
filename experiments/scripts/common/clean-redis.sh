#!/bin/bash

printf "\nCleaning streams for group 'monitoring-claims-controllers'...\n\n"
docker exec varys_redis_1 redis-cli XTRIM monitoring-claims:created MAXLEN 0
docker exec varys_redis_1 redis-cli XTRIM monitoring-claims:updated MAXLEN 0
docker exec varys_redis_1 redis-cli XTRIM monitoring-claims:deleted MAXLEN 0
docker exec varys_redis_1 redis-cli XTRIM monitoring-claims:processing MAXLEN 0
docker exec varys_redis_1 redis-cli XTRIM monitoring-claims:processed MAXLEN 0
docker exec varys_redis_1 redis-cli XTRIM monitoring-claims:aborted MAXLEN 0

printf "\nCleaning streams for group 'monitoring-units-controllers'...\n\n"
docker exec varys_redis_1 redis-cli XTRIM monitoring-units:created MAXLEN 0
docker exec varys_redis_1 redis-cli XTRIM monitoring-units:configured MAXLEN 0
docker exec varys_redis_1 redis-cli XTRIM monitoring-units:queued MAXLEN 0
docker exec varys_redis_1 redis-cli XTRIM monitoring-units:syncing MAXLEN 0
docker exec varys_redis_1 redis-cli XTRIM monitoring-units:synced MAXLEN 0
docker exec varys_redis_1 redis-cli XTRIM monitoring-units:tombstone MAXLEN 0
docker exec varys_redis_1 redis-cli XTRIM monitoring-units:failed MAXLEN 0