#!/bin/bash

if [ "$#" -ne 1 ]
then
  printf "Missing required argument API_URL!\n"
  exit 1
fi

API_URL=$1

printf "Creating probes...\n\n"

curl --location --request POST "$API_URL/probes" \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
  "artifactId": "metricbeat_cpu",
  "supportedPatterns": [
    "INTERNAL_PROBES",
    "SHARED_SIDECAR_SINGLE_PROBE"
  ],
  "supportedGoals": [
    "CPU_CONSUMPTION"
  ],
  "supportedDataOutputs": [
    "ELASTICSEARCH"
  ]
}'
printf "\n\n"

curl --location --request POST "$API_URL/probes" \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
  "artifactId": "metricbeat_memory",
  "supportedPatterns": [
    "INTERNAL_PROBES",
    "SHARED_SIDECAR_SINGLE_PROBE"
  ],
  "supportedGoals": [
    "MEMORY_CONSUMPTION"
  ],
  "supportedDataOutputs": [
    "ELASTICSEARCH"
  ]
}'

printf "\n\n"

curl --location --request POST "$API_URL/probes" \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
  "artifactId": "metricbeat_postgresql",
  "supportedPatterns": [
    "INTERNAL_PROBES",
    "SHARED_SIDECAR_SINGLE_PROBE"
  ],
  "supportedGoals": [
    "DATABASE"
  ],
  "supportedDataOutputs": [
    "ELASTICSEARCH"
  ]
}'

printf "\n\nCreated probes\n\n"