#!/bin/bash

sudo systemctl stop metricbeat_*
sudo systemctl daemon-reload
sudo rm -f /etc/systemd/system/metricbeat_*
rm -rf /home/varys/metricbeat_*