#!/bin/bash

help()
{
   # Display Help
   printf "Run an Azure probes deployment run.\n\n"
   printf "Usage: run-azure-probes-deployment.sh [-h]\n\n"
   printf "Options:\n"
   printf "h     Print this help.\n"
}

# main script
while getopts ":h" option; do
   case $option in
      h) # display help
         help
         exit;;
     \?) # incorrect option
         printf "Error: Invalid option"
         exit;;
   esac
done

if [ "$#" -ne 1 ]
then
  printf "Missing required argument!\n"
  printf "Usage: run-probes-deployment.sh [-h] API_URL\n\n"
  exit 1
fi

API_URL=$1

./../common/init-probes-catalog.sh "$API_URL"

USER_ID=$( curl --location --request POST "$API_URL/users" \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "varys-test1"
}' | jq --raw-output '.id' )

curl --location --request POST "$API_URL/monitoring-claims" \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
    "userId": "'$USER_ID'",
    "goals": [
        "CPU_CONSUMPTION",
        "MEMORY_CONSUMPTION",
        "DATABASE"
    ],
    "target": {
        "sourceSystemId": "postgres",
        "sourceSystem": "azure",
        "envType": "ACCESSIBLE_VM"
    }
}'