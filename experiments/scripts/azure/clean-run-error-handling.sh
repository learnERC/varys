#!/bin/bash

help()
{
   # Display Help
   printf "Clean an Azure hard error handling run.\n\n"
   printf "Usage: clean-run-error-handling.sh [-h] YOUR_TARGET_VM_IP YOUR_ELASTICSEARCH_IP\n\n"
   printf "Options:\n"
   printf "h     Print this help.\n"
}

# main script
while getopts ":h" option; do
   case $option in
      h) # display help
         help
         exit;;
     \?) # incorrect option
         printf "Error: Invalid option"
         exit;;
   esac
done

if [ "$#" -ne 2 ]
then
  printf "Missing required arguments!\n"
  printf "Usage: clean-run-error-handling.sh [-h] YOUR_TARGET_VM_IP YOUR_ELASTICSEARCH_IP\n"
  exit 1
fi

printf "Cleaning target VM...\n\n"
scp -i ../../../varys-cloud-bridge/src/main/resources/varys-ssh-key clean-target-vm.sh varys@$1:/home/varys/clean.sh
ssh -i ../../../varys-cloud-bridge/src/main/resources/varys-ssh-key varys@$1 'bash /home/varys/clean.sh'
printf "\n\nTarget VM cleaned\n\n"

printf "Cleaning Elasticsearch...\n\n"
./../common/clean-elasticsearch.sh $2
printf "\n\nElasticsearch cleaned\n\n"

printf "Cleaning MongoDB...\n\n"
./../common/mongodb-drop-db.sh
printf "\nMongoDB cleaned\n"

printf "Cleaning Redis streams...\n\n"
./../common/clean-redis.sh
printf "\nRedis cleaned\n"