#!/usr/bin/env python3

import requests
import sys

indicators_probes = {
  'CPU_CONSUMPTION': 'metricbeat_cpu',
  'MEMORY_CONSUMPTION': 'metricbeat_memory',
  'DATABASE': 'metricbeat_postgresql'
}

def create_users(api_url, number_of_operators):
  user_ids = []
  url = '{}/users'.format(api_url)
  for i in range(number_of_operators):
    payload = {
      'username': 'varys-test' + str(i+1)
    }
    r = requests.post(url, json=payload)
    user = r.json()
    user_ids.append(user['id'])
  return user_ids

def init_probes_catalog(api_url):
  url = '{}/probes'.format(api_url)
  for item in indicators_probes.items():
    payload = {
      'artifactId': item[1],
      'supportedPatterns': [
        'INTERNAL_PROBES',
        'SHARED_SIDECAR_SINGLE_PROBE'
      ],
      'supportedGoals': [
        item[0]
      ],
      'supportedDataOutputs': [
        'ELASTICSEARCH'
      ]
    }
    requests.post(url, json=payload)


def main():
  if len(sys.argv) != 3:
    sys.exit('Missing required argument: API_URL NUMBER_OPERATORS')

  api_url = sys.argv[1]
  number_of_operators = int(sys.argv[2])

  user_ids = create_users(api_url, number_of_operators)
  init_probes_catalog(api_url)
  monitoring_request = []
  for user_id in user_ids:
    monitoring_request.append({
      'userId': user_id,
      'goals': ['DATABASE'],
      'target': {
        'sourceSystemId': 'postgres',
        'sourceSystem': 'azure',
        'envType': 'ACCESSIBLE_VM'
      }
    })

  requests.post(api_url + '/monitoring-claims/_bulk', json=monitoring_request)


if __name__ == '__main__':
  main()
