/*
 * VARYS
 * VARYS API Documentation
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package io.varys.api.jclient.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.varys.api.jclient.model.EnvType;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for Target
 */
public class TargetTest {
    private final Target model = new Target();

    /**
     * Model tests for Target
     */
    @Test
    public void testTarget() {
        // TODO: test Target
    }

    /**
     * Test the property 'sourceSystem'
     */
    @Test
    public void sourceSystemTest() {
        // TODO: test sourceSystem
    }

    /**
     * Test the property 'sourceSystemId'
     */
    @Test
    public void sourceSystemIdTest() {
        // TODO: test sourceSystemId
    }

    /**
     * Test the property 'envType'
     */
    @Test
    public void envTypeTest() {
        // TODO: test envType
    }

}
