# DefaultApi

All URIs are relative to *http://localhost:5000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get**](DefaultApi.md#get) | **GET** / | Get an overview of all the routes in the application



## get

> String get()

Get an overview of all the routes in the application

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.DefaultApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        DefaultApi apiInstance = new DefaultApi(defaultClient);
        try {
            String result = apiInstance.get();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling DefaultApi#get");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/html, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |

