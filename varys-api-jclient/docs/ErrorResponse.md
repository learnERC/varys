

# ErrorResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  |  [optional]
**status** | **Integer** |  |  [optional]
**type** | **String** |  |  [optional]
**details** | **Map&lt;String, String&gt;** |  |  [optional]



