

# UpdateMonitoringClaimStatusRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**MonitoringClaimStatus**](MonitoringClaimStatus.md) |  |  [optional]



