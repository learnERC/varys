

# Goal

## Enum


* `ALL` (value: `"ALL"`)

* `PERFORMANCE` (value: `"PERFORMANCE"`)

* `CPU_CONSUMPTION` (value: `"CPU_CONSUMPTION"`)

* `MEMORY_CONSUMPTION` (value: `"MEMORY_CONSUMPTION"`)

* `RESOURCE_CONSUMPTION` (value: `"RESOURCE_CONSUMPTION"`)

* `DATABASE` (value: `"DATABASE"`)

* `CPU_CONSUMPTION_1` (value: `"CPU_CONSUMPTION_1"`)

* `CPU_CONSUMPTION_2` (value: `"CPU_CONSUMPTION_2"`)

* `CPU_CONSUMPTION_3` (value: `"CPU_CONSUMPTION_3"`)

* `CPU_CONSUMPTION_4` (value: `"CPU_CONSUMPTION_4"`)

* `CPU_CONSUMPTION_5` (value: `"CPU_CONSUMPTION_5"`)

* `CPU_CONSUMPTION_6` (value: `"CPU_CONSUMPTION_6"`)

* `CPU_CONSUMPTION_7` (value: `"CPU_CONSUMPTION_7"`)

* `CPU_CONSUMPTION_8` (value: `"CPU_CONSUMPTION_8"`)

* `CPU_CONSUMPTION_9` (value: `"CPU_CONSUMPTION_9"`)

* `CPU_CONSUMPTION_10` (value: `"CPU_CONSUMPTION_10"`)

* `MEMORY_CONSUMPTION_1` (value: `"MEMORY_CONSUMPTION_1"`)

* `MEMORY_CONSUMPTION_2` (value: `"MEMORY_CONSUMPTION_2"`)

* `MEMORY_CONSUMPTION_3` (value: `"MEMORY_CONSUMPTION_3"`)

* `MEMORY_CONSUMPTION_4` (value: `"MEMORY_CONSUMPTION_4"`)

* `MEMORY_CONSUMPTION_5` (value: `"MEMORY_CONSUMPTION_5"`)

* `MEMORY_CONSUMPTION_6` (value: `"MEMORY_CONSUMPTION_6"`)

* `MEMORY_CONSUMPTION_7` (value: `"MEMORY_CONSUMPTION_7"`)

* `MEMORY_CONSUMPTION_8` (value: `"MEMORY_CONSUMPTION_8"`)

* `MEMORY_CONSUMPTION_9` (value: `"MEMORY_CONSUMPTION_9"`)

* `MEMORY_CONSUMPTION_10` (value: `"MEMORY_CONSUMPTION_10"`)

* `DATABASE_1` (value: `"DATABASE_1"`)

* `DATABASE_2` (value: `"DATABASE_2"`)

* `DATABASE_3` (value: `"DATABASE_3"`)

* `DATABASE_4` (value: `"DATABASE_4"`)

* `DATABASE_5` (value: `"DATABASE_5"`)

* `DATABASE_6` (value: `"DATABASE_6"`)

* `DATABASE_7` (value: `"DATABASE_7"`)

* `DATABASE_8` (value: `"DATABASE_8"`)

* `DATABASE_9` (value: `"DATABASE_9"`)

* `DATABASE_10` (value: `"DATABASE_10"`)



