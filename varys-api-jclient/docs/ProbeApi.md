# ProbeApi

All URIs are relative to *http://localhost:5000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProbe**](ProbeApi.md#createProbe) | **POST** /probes | Create a probe
[**searchProbes**](ProbeApi.md#searchProbes) | **GET** /probes/_search | Search probes



## createProbe

> Probe createProbe(probe)

Create a probe

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.ProbeApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        ProbeApi apiInstance = new ProbeApi(defaultClient);
        Probe probe = new Probe(); // Probe | 
        try {
            Probe result = apiInstance.createProbe(probe);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling ProbeApi#createProbe");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **probe** | [**Probe**](Probe.md)|  | [optional]

### Return type

[**Probe**](Probe.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Created |  -  |
| **400** | Bad Request |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## searchProbes

> List&lt;Probe&gt; searchProbes(supportedPattern, supportedGoal, supportedDataOutput)

Search probes

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.ProbeApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        ProbeApi apiInstance = new ProbeApi(defaultClient);
        ConfigurationPattern supportedPattern = new ConfigurationPattern(); // ConfigurationPattern | 
        Goal supportedGoal = new Goal(); // Goal | 
        DataOutput supportedDataOutput = new DataOutput(); // DataOutput | 
        try {
            List<Probe> result = apiInstance.searchProbes(supportedPattern, supportedGoal, supportedDataOutput);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling ProbeApi#searchProbes");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supportedPattern** | [**ConfigurationPattern**](.md)|  | [optional] [enum: INTERNAL_PROBES, RESERVED_SIDECAR_SINGLE_PROBE, SHARED_SIDECAR_SINGLE_PROBE, RESERVED_SIDECAR_MULTI_PROBE, SHARED_SIDECAR_MULTI_PROBE, RESERVED_GLOBAL_UNIT_SINGLE_PROBE, SHARED_GLOBAL_UNIT_SINGLE_PROBE, RESERVED_GLOBAL_UNIT_MULTI_PROBE, SHARED_GLOBAL_UNIT_MULTI_PROBE]
 **supportedGoal** | [**Goal**](.md)|  | [optional] [enum: ALL, PERFORMANCE, CPU_CONSUMPTION, MEMORY_CONSUMPTION, RESOURCE_CONSUMPTION, DATABASE, CPU_CONSUMPTION_1, CPU_CONSUMPTION_2, CPU_CONSUMPTION_3, CPU_CONSUMPTION_4, CPU_CONSUMPTION_5, CPU_CONSUMPTION_6, CPU_CONSUMPTION_7, CPU_CONSUMPTION_8, CPU_CONSUMPTION_9, CPU_CONSUMPTION_10, MEMORY_CONSUMPTION_1, MEMORY_CONSUMPTION_2, MEMORY_CONSUMPTION_3, MEMORY_CONSUMPTION_4, MEMORY_CONSUMPTION_5, MEMORY_CONSUMPTION_6, MEMORY_CONSUMPTION_7, MEMORY_CONSUMPTION_8, MEMORY_CONSUMPTION_9, MEMORY_CONSUMPTION_10, DATABASE_1, DATABASE_2, DATABASE_3, DATABASE_4, DATABASE_5, DATABASE_6, DATABASE_7, DATABASE_8, DATABASE_9, DATABASE_10]
 **supportedDataOutput** | [**DataOutput**](.md)|  | [optional] [enum: ELASTICSEARCH, PROMETHEUS]

### Return type

[**List&lt;Probe&gt;**](Probe.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **400** | Bad Request |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |

