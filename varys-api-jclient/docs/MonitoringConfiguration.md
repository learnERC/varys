

# MonitoringConfiguration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**monitoringUnitId** | **String** |  | 
**monitoringClaimId** | **String** |  | 
**userId** | **String** |  | 
**target** | [**Target**](Target.md) |  | 
**goal** | [**Goal**](Goal.md) |  | 
**probeId** | **String** |  | 
**status** | [**MonitoringConfigurationStatus**](MonitoringConfigurationStatus.md) |  |  [optional]



