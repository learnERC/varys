

# Probe

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**artifactId** | **String** |  |  [optional]
**supportedGoals** | [**List&lt;Goal&gt;**](Goal.md) |  |  [optional]
**supportedPatterns** | [**List&lt;ConfigurationPattern&gt;**](ConfigurationPattern.md) |  |  [optional]
**supportedDataOutputs** | [**List&lt;DataOutput&gt;**](DataOutput.md) |  |  [optional]



