# MonitoringUnitApi

All URIs are relative to *http://localhost:5000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**attachCurrentConfigurations**](MonitoringUnitApi.md#attachCurrentConfigurations) | **PATCH** /monitoring-units/{id}/current-configurations | Attach current configurations
[**createAndAttachDesiredConfigurations**](MonitoringUnitApi.md#createAndAttachDesiredConfigurations) | **POST** /monitoring-units/{id}/desired-configurations/_bulk | Create and attach desired configurations (bulk)
[**createMonitoringUnit**](MonitoringUnitApi.md#createMonitoringUnit) | **POST** /monitoring-units | Create monitoring unit
[**deleteMonitoringUnit**](MonitoringUnitApi.md#deleteMonitoringUnit) | **DELETE** /monitoring-units/{id} | Delete monitoring unit
[**detachCurrentConfigurations**](MonitoringUnitApi.md#detachCurrentConfigurations) | **DELETE** /monitoring-units/{id}/current-configurations | Detach current configurations
[**detachDesiredConfigurations**](MonitoringUnitApi.md#detachDesiredConfigurations) | **DELETE** /monitoring-units/{id}/desired-configurations | Detach desired configurations
[**getMonitoringUnit**](MonitoringUnitApi.md#getMonitoringUnit) | **GET** /monitoring-units/{id} | Get monitoring unit
[**getMonitoringUnitFullConfiguration**](MonitoringUnitApi.md#getMonitoringUnitFullConfiguration) | **GET** /monitoring-units/{id}/full-configuration | Get monitoring unit full configuration
[**getMonitoringUnits**](MonitoringUnitApi.md#getMonitoringUnits) | **GET** /monitoring-units | Get all monitoring units
[**incrementDesiredConfigurationsStatusRetries**](MonitoringUnitApi.md#incrementDesiredConfigurationsStatusRetries) | **POST** /monitoring-units/{id}/desired-configurations/status/retries | Increment desired configurations retries
[**searchMonitoringUnits**](MonitoringUnitApi.md#searchMonitoringUnits) | **GET** /monitoring-units/_search | Search monitoring units
[**setCurrentConfigurations**](MonitoringUnitApi.md#setCurrentConfigurations) | **PUT** /monitoring-units/{id}/current-configurations | Set monitoring unit current configurations
[**setCurrentConfigurationsStatusRemoving**](MonitoringUnitApi.md#setCurrentConfigurationsStatusRemoving) | **PUT** /monitoring-units/{id}/current-configurations/status/removing | Set current configurations status removing to true
[**setDesiredConfigurationsStatusAdding**](MonitoringUnitApi.md#setDesiredConfigurationsStatusAdding) | **PUT** /monitoring-units/{id}/desired-configurations/status/adding | Set desired configurations status adding to true
[**setDesiredConfigurationsStatusRetryLimitExceeded**](MonitoringUnitApi.md#setDesiredConfigurationsStatusRetryLimitExceeded) | **PUT** /monitoring-units/{id}/desired-configurations/status/retry-limit-exceeded | Set desired configurations status retry limit exceeded to true
[**updateMonitoringUnitStatus**](MonitoringUnitApi.md#updateMonitoringUnitStatus) | **PATCH** /monitoring-units/{id}/status | Update monitoring unit status



## attachCurrentConfigurations

> MonitoringUnit attachCurrentConfigurations(id, configurationIds)

Attach current configurations

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        String id = "id_example"; // String | The monitoring unit id
        List<String> configurationIds = Arrays.asList(); // List<String> | 
        try {
            MonitoringUnit result = apiInstance.attachCurrentConfigurations(id, configurationIds);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#attachCurrentConfigurations");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring unit id |
 **configurationIds** | [**List&lt;String&gt;**](String.md)|  |

### Return type

[**MonitoringUnit**](MonitoringUnit.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## createAndAttachDesiredConfigurations

> MonitoringUnit createAndAttachDesiredConfigurations(id, monitoringConfiguration)

Create and attach desired configurations (bulk)

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        String id = "id_example"; // String | The monitoring unit id
        List<MonitoringConfiguration> monitoringConfiguration = Arrays.asList(); // List<MonitoringConfiguration> | 
        try {
            MonitoringUnit result = apiInstance.createAndAttachDesiredConfigurations(id, monitoringConfiguration);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#createAndAttachDesiredConfigurations");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring unit id |
 **monitoringConfiguration** | [**List&lt;MonitoringConfiguration&gt;**](MonitoringConfiguration.md)|  | [optional]

### Return type

[**MonitoringUnit**](MonitoringUnit.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Created |  -  |
| **400** | Bad Request |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## createMonitoringUnit

> MonitoringUnit createMonitoringUnit(monitoringUnit)

Create monitoring unit

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        MonitoringUnit monitoringUnit = new MonitoringUnit(); // MonitoringUnit | 
        try {
            MonitoringUnit result = apiInstance.createMonitoringUnit(monitoringUnit);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#createMonitoringUnit");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **monitoringUnit** | [**MonitoringUnit**](MonitoringUnit.md)|  | [optional]

### Return type

[**MonitoringUnit**](MonitoringUnit.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Created |  -  |
| **400** | Bad Request |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## deleteMonitoringUnit

> deleteMonitoringUnit(id)

Delete monitoring unit

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        String id = "id_example"; // String | The monitoring unit id
        try {
            apiInstance.deleteMonitoringUnit(id);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#deleteMonitoringUnit");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring unit id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | No Content |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## detachCurrentConfigurations

> MonitoringUnit detachCurrentConfigurations(id, configurationIds)

Detach current configurations

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        String id = "id_example"; // String | The monitoring unit id
        List<String> configurationIds = Arrays.asList(); // List<String> | 
        try {
            MonitoringUnit result = apiInstance.detachCurrentConfigurations(id, configurationIds);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#detachCurrentConfigurations");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring unit id |
 **configurationIds** | [**List&lt;String&gt;**](String.md)|  |

### Return type

[**MonitoringUnit**](MonitoringUnit.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## detachDesiredConfigurations

> MonitoringUnit detachDesiredConfigurations(id, configurationIds)

Detach desired configurations

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        String id = "id_example"; // String | The monitoring unit id
        List<String> configurationIds = Arrays.asList(); // List<String> | 
        try {
            MonitoringUnit result = apiInstance.detachDesiredConfigurations(id, configurationIds);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#detachDesiredConfigurations");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring unit id |
 **configurationIds** | [**List&lt;String&gt;**](String.md)|  |

### Return type

[**MonitoringUnit**](MonitoringUnit.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## getMonitoringUnit

> MonitoringUnit getMonitoringUnit(id)

Get monitoring unit

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        String id = "id_example"; // String | The monitoring unit id
        try {
            MonitoringUnit result = apiInstance.getMonitoringUnit(id);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#getMonitoringUnit");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring unit id |

### Return type

[**MonitoringUnit**](MonitoringUnit.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## getMonitoringUnitFullConfiguration

> MonitoringUnitFullConfiguration getMonitoringUnitFullConfiguration(id)

Get monitoring unit full configuration

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        String id = "id_example"; // String | The monitoring unit id
        try {
            MonitoringUnitFullConfiguration result = apiInstance.getMonitoringUnitFullConfiguration(id);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#getMonitoringUnitFullConfiguration");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring unit id |

### Return type

[**MonitoringUnitFullConfiguration**](MonitoringUnitFullConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## getMonitoringUnits

> List&lt;MonitoringUnit&gt; getMonitoringUnits()

Get all monitoring units

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        try {
            List<MonitoringUnit> result = apiInstance.getMonitoringUnits();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#getMonitoringUnits");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**List&lt;MonitoringUnit&gt;**](MonitoringUnit.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## incrementDesiredConfigurationsStatusRetries

> incrementDesiredConfigurationsStatusRetries(id, configurationIds)

Increment desired configurations retries

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        String id = "id_example"; // String | The monitoring unit id
        List<String> configurationIds = Arrays.asList(); // List<String> | 
        try {
            apiInstance.incrementDesiredConfigurationsStatusRetries(id, configurationIds);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#incrementDesiredConfigurationsStatusRetries");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring unit id |
 **configurationIds** | [**List&lt;String&gt;**](String.md)|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | OK |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## searchMonitoringUnits

> List&lt;MonitoringUnit&gt; searchMonitoringUnits(configurationPattern, userId, targetSourceSystem, targetSourceSystemId, probeId)

Search monitoring units

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        ConfigurationPattern configurationPattern = new ConfigurationPattern(); // ConfigurationPattern | 
        String userId = "userId_example"; // String | 
        String targetSourceSystem = "targetSourceSystem_example"; // String | 
        String targetSourceSystemId = "targetSourceSystemId_example"; // String | 
        String probeId = "probeId_example"; // String | 
        try {
            List<MonitoringUnit> result = apiInstance.searchMonitoringUnits(configurationPattern, userId, targetSourceSystem, targetSourceSystemId, probeId);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#searchMonitoringUnits");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **configurationPattern** | [**ConfigurationPattern**](.md)|  | [enum: INTERNAL_PROBES, RESERVED_SIDECAR_SINGLE_PROBE, SHARED_SIDECAR_SINGLE_PROBE, RESERVED_SIDECAR_MULTI_PROBE, SHARED_SIDECAR_MULTI_PROBE, RESERVED_GLOBAL_UNIT_SINGLE_PROBE, SHARED_GLOBAL_UNIT_SINGLE_PROBE, RESERVED_GLOBAL_UNIT_MULTI_PROBE, SHARED_GLOBAL_UNIT_MULTI_PROBE]
 **userId** | **String**|  | [optional]
 **targetSourceSystem** | **String**|  | [optional]
 **targetSourceSystemId** | **String**|  | [optional]
 **probeId** | **String**|  | [optional]

### Return type

[**List&lt;MonitoringUnit&gt;**](MonitoringUnit.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## setCurrentConfigurations

> MonitoringUnit setCurrentConfigurations(id, requestBody)

Set monitoring unit current configurations

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        String id = "id_example"; // String | The monitoring unit id
        List<String> requestBody = Arrays.asList(); // List<String> | 
        try {
            MonitoringUnit result = apiInstance.setCurrentConfigurations(id, requestBody);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#setCurrentConfigurations");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring unit id |
 **requestBody** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**MonitoringUnit**](MonitoringUnit.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **400** | Bad Request |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## setCurrentConfigurationsStatusRemoving

> setCurrentConfigurationsStatusRemoving(id, configurationIds)

Set current configurations status removing to true

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        String id = "id_example"; // String | The monitoring unit id
        List<String> configurationIds = Arrays.asList(); // List<String> | 
        try {
            apiInstance.setCurrentConfigurationsStatusRemoving(id, configurationIds);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#setCurrentConfigurationsStatusRemoving");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring unit id |
 **configurationIds** | [**List&lt;String&gt;**](String.md)|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | OK |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## setDesiredConfigurationsStatusAdding

> setDesiredConfigurationsStatusAdding(id, configurationIds)

Set desired configurations status adding to true

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        String id = "id_example"; // String | The monitoring unit id
        List<String> configurationIds = Arrays.asList(); // List<String> | 
        try {
            apiInstance.setDesiredConfigurationsStatusAdding(id, configurationIds);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#setDesiredConfigurationsStatusAdding");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring unit id |
 **configurationIds** | [**List&lt;String&gt;**](String.md)|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | OK |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## setDesiredConfigurationsStatusRetryLimitExceeded

> setDesiredConfigurationsStatusRetryLimitExceeded(id, configurationIds)

Set desired configurations status retry limit exceeded to true

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        String id = "id_example"; // String | The monitoring unit id
        List<String> configurationIds = Arrays.asList(); // List<String> | 
        try {
            apiInstance.setDesiredConfigurationsStatusRetryLimitExceeded(id, configurationIds);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#setDesiredConfigurationsStatusRetryLimitExceeded");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring unit id |
 **configurationIds** | [**List&lt;String&gt;**](String.md)|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | OK |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## updateMonitoringUnitStatus

> MonitoringUnit updateMonitoringUnitStatus(id, ifMatch, updateMonitoringUnitStatusRequest)

Update monitoring unit status

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringUnitApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringUnitApi apiInstance = new MonitoringUnitApi(defaultClient);
        String id = "id_example"; // String | The monitoring unit id
        String ifMatch = "ifMatch_example"; // String | The etag of the resource
        UpdateMonitoringUnitStatusRequest updateMonitoringUnitStatusRequest = new UpdateMonitoringUnitStatusRequest(); // UpdateMonitoringUnitStatusRequest | 
        try {
            MonitoringUnit result = apiInstance.updateMonitoringUnitStatus(id, ifMatch, updateMonitoringUnitStatusRequest);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringUnitApi#updateMonitoringUnitStatus");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring unit id |
 **ifMatch** | **String**| The etag of the resource | [optional]
 **updateMonitoringUnitStatusRequest** | [**UpdateMonitoringUnitStatusRequest**](UpdateMonitoringUnitStatusRequest.md)|  | [optional]

### Return type

[**MonitoringUnit**](MonitoringUnit.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **400** | Bad Request |  -  |
| **404** | Not Found |  -  |
| **412** | Precondition Failed |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |

