

# MonitoringUnitFullConfiguration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**configurations** | [**List&lt;MonitoringConfiguration&gt;**](MonitoringConfiguration.md) |  |  [optional]



