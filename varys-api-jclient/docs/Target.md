

# Target

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceSystem** | **String** |  |  [optional]
**sourceSystemId** | **String** |  |  [optional]
**envType** | [**EnvType**](EnvType.md) |  |  [optional]



