

# MonitoringUnitStatus

## Enum


* `CREATED` (value: `"CREATED"`)

* `CONFIGURED` (value: `"CONFIGURED"`)

* `QUEUED` (value: `"QUEUED"`)

* `SYNCING` (value: `"SYNCING"`)

* `SYNCED` (value: `"SYNCED"`)

* `TOMBSTONE` (value: `"TOMBSTONE"`)

* `FAILED` (value: `"FAILED"`)



