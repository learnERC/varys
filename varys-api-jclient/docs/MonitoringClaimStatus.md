

# MonitoringClaimStatus

## Enum


* `CREATED` (value: `"CREATED"`)

* `UPDATED` (value: `"UPDATED"`)

* `QUEUED` (value: `"QUEUED"`)

* `PROCESSING` (value: `"PROCESSING"`)

* `PROCESSED` (value: `"PROCESSED"`)

* `ABORTED` (value: `"ABORTED"`)

* `DELETED` (value: `"DELETED"`)



