

# MonitoringConfigurationStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**desired** | **Boolean** |  |  [optional]
**current** | **Boolean** |  |  [optional]
**adding** | **Boolean** |  |  [optional]
**removing** | **Boolean** |  |  [optional]
**retryLimitExceeded** | **Boolean** |  |  [optional]
**retries** | **Integer** |  |  [optional]
**lastTryAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]



