

# MonitoringClaim

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**meta** | [**Meta**](Meta.md) |  |  [optional]
**userId** | **String** |  | 
**target** | [**Target**](Target.md) |  | 
**goals** | [**List&lt;Goal&gt;**](Goal.md) |  | 
**status** | [**MonitoringClaimStatus**](MonitoringClaimStatus.md) |  |  [optional]



