

# UpdateMonitoringUnitStatusRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**MonitoringUnitStatus**](MonitoringUnitStatus.md) |  |  [optional]



