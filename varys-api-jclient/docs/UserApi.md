# UserApi

All URIs are relative to *http://localhost:5000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUser**](UserApi.md#createUser) | **POST** /users | Create user
[**deleteUser**](UserApi.md#deleteUser) | **DELETE** /users/{id} | Delete user
[**getUser**](UserApi.md#getUser) | **GET** /users/{id} | Get user
[**getUsers**](UserApi.md#getUsers) | **GET** /users | Get all users
[**updateUser**](UserApi.md#updateUser) | **PATCH** /users/{id} | Update user



## createUser

> User createUser(user)

Create user

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.UserApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        UserApi apiInstance = new UserApi(defaultClient);
        User user = new User(); // User | 
        try {
            User result = apiInstance.createUser(user);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling UserApi#createUser");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | [**User**](User.md)|  | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Created |  -  |
| **400** | Bad Request |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## deleteUser

> deleteUser(id)

Delete user

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.UserApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        UserApi apiInstance = new UserApi(defaultClient);
        String id = "id_example"; // String | The user id
        try {
            apiInstance.deleteUser(id);
        } catch (ApiException e) {
            System.err.println("Exception when calling UserApi#deleteUser");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The user id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | No Content |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## getUser

> User getUser(id)

Get user

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.UserApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        UserApi apiInstance = new UserApi(defaultClient);
        String id = "id_example"; // String | The user id
        try {
            User result = apiInstance.getUser(id);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling UserApi#getUser");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The user id |

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## getUsers

> List&lt;User&gt; getUsers()

Get all users

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.UserApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        UserApi apiInstance = new UserApi(defaultClient);
        try {
            List<User> result = apiInstance.getUsers();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling UserApi#getUsers");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**List&lt;User&gt;**](User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## updateUser

> User updateUser(id, user)

Update user

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.UserApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        UserApi apiInstance = new UserApi(defaultClient);
        String id = "id_example"; // String | The user id
        User user = new User(); // User | 
        try {
            User result = apiInstance.updateUser(id, user);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling UserApi#updateUser");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The user id |
 **user** | [**User**](User.md)|  | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **400** | Bad Request |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |

