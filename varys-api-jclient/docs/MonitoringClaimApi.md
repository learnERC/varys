# MonitoringClaimApi

All URIs are relative to *http://localhost:5000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMonitoringClaim**](MonitoringClaimApi.md#createMonitoringClaim) | **POST** /monitoring-claims | Create monitoring claim
[**createMonitoringClaimBulk**](MonitoringClaimApi.md#createMonitoringClaimBulk) | **POST** /monitoring-claims/_bulk | Create monitoring claim bulk
[**deleteMonitoringClaim**](MonitoringClaimApi.md#deleteMonitoringClaim) | **DELETE** /monitoring-claims/{id} | Delete monitoring claim
[**getMonitoringClaim**](MonitoringClaimApi.md#getMonitoringClaim) | **GET** /monitoring-claims/{id} | Get monitoring claim
[**getMonitoringClaimConfigurations**](MonitoringClaimApi.md#getMonitoringClaimConfigurations) | **GET** /monitoring-claims/{id}/configurations | Get monitoring claim configurations
[**updateMonitoringClaimGoals**](MonitoringClaimApi.md#updateMonitoringClaimGoals) | **PATCH** /monitoring-claims/{id}/goals | Update monitoring claim goals
[**updateMonitoringClaimStatus**](MonitoringClaimApi.md#updateMonitoringClaimStatus) | **PATCH** /monitoring-claims/{id}/status | Update monitoring claim status



## createMonitoringClaim

> MonitoringClaim createMonitoringClaim(monitoringClaim)

Create monitoring claim

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringClaimApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringClaimApi apiInstance = new MonitoringClaimApi(defaultClient);
        MonitoringClaim monitoringClaim = new MonitoringClaim(); // MonitoringClaim | 
        try {
            MonitoringClaim result = apiInstance.createMonitoringClaim(monitoringClaim);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringClaimApi#createMonitoringClaim");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **monitoringClaim** | [**MonitoringClaim**](MonitoringClaim.md)|  | [optional]

### Return type

[**MonitoringClaim**](MonitoringClaim.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Created |  -  |
| **400** | Bad Request |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## createMonitoringClaimBulk

> List&lt;MonitoringClaim&gt; createMonitoringClaimBulk(monitoringClaim)

Create monitoring claim bulk

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringClaimApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringClaimApi apiInstance = new MonitoringClaimApi(defaultClient);
        List<MonitoringClaim> monitoringClaim = Arrays.asList(); // List<MonitoringClaim> | 
        try {
            List<MonitoringClaim> result = apiInstance.createMonitoringClaimBulk(monitoringClaim);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringClaimApi#createMonitoringClaimBulk");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **monitoringClaim** | [**List&lt;MonitoringClaim&gt;**](MonitoringClaim.md)|  | [optional]

### Return type

[**List&lt;MonitoringClaim&gt;**](MonitoringClaim.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **400** | Bad Request |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## deleteMonitoringClaim

> deleteMonitoringClaim(id, ifMatch)

Delete monitoring claim

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringClaimApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringClaimApi apiInstance = new MonitoringClaimApi(defaultClient);
        String id = "id_example"; // String | The monitoring claim id
        String ifMatch = "ifMatch_example"; // String | The etag of the resource
        try {
            apiInstance.deleteMonitoringClaim(id, ifMatch);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringClaimApi#deleteMonitoringClaim");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring claim id |
 **ifMatch** | **String**| The etag of the resource | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | No Content |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## getMonitoringClaim

> MonitoringClaim getMonitoringClaim(id)

Get monitoring claim

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringClaimApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringClaimApi apiInstance = new MonitoringClaimApi(defaultClient);
        String id = "id_example"; // String | The monitoring claim id
        try {
            MonitoringClaim result = apiInstance.getMonitoringClaim(id);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringClaimApi#getMonitoringClaim");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring claim id |

### Return type

[**MonitoringClaim**](MonitoringClaim.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## getMonitoringClaimConfigurations

> List&lt;MonitoringConfiguration&gt; getMonitoringClaimConfigurations(id)

Get monitoring claim configurations

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringClaimApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringClaimApi apiInstance = new MonitoringClaimApi(defaultClient);
        String id = "id_example"; // String | The monitoring claim id
        try {
            List<MonitoringConfiguration> result = apiInstance.getMonitoringClaimConfigurations(id);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringClaimApi#getMonitoringClaimConfigurations");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring claim id |

### Return type

[**List&lt;MonitoringConfiguration&gt;**](MonitoringConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## updateMonitoringClaimGoals

> MonitoringClaim updateMonitoringClaimGoals(id, ifMatch, updateGoalsRequest)

Update monitoring claim goals

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringClaimApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringClaimApi apiInstance = new MonitoringClaimApi(defaultClient);
        String id = "id_example"; // String | The monitoring claim id
        String ifMatch = "ifMatch_example"; // String | The etag of the resource
        UpdateGoalsRequest updateGoalsRequest = new UpdateGoalsRequest(); // UpdateGoalsRequest | 
        try {
            MonitoringClaim result = apiInstance.updateMonitoringClaimGoals(id, ifMatch, updateGoalsRequest);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringClaimApi#updateMonitoringClaimGoals");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring claim id |
 **ifMatch** | **String**| The etag of the resource | [optional]
 **updateGoalsRequest** | [**UpdateGoalsRequest**](UpdateGoalsRequest.md)|  | [optional]

### Return type

[**MonitoringClaim**](MonitoringClaim.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **400** | Bad Request |  -  |
| **404** | Not Found |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |


## updateMonitoringClaimStatus

> MonitoringClaim updateMonitoringClaimStatus(id, ifMatch, updateMonitoringClaimStatusRequest)

Update monitoring claim status

### Example

```java
// Import classes:
import io.varys.api.jclient.ApiClient;
import io.varys.api.jclient.ApiException;
import io.varys.api.jclient.Configuration;
import io.varys.api.jclient.models.*;
import io.varys.api.jclient.endpoint.MonitoringClaimApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:5000");

        MonitoringClaimApi apiInstance = new MonitoringClaimApi(defaultClient);
        String id = "id_example"; // String | The monitoring claim id
        String ifMatch = "ifMatch_example"; // String | The etag of the resource
        UpdateMonitoringClaimStatusRequest updateMonitoringClaimStatusRequest = new UpdateMonitoringClaimStatusRequest(); // UpdateMonitoringClaimStatusRequest | 
        try {
            MonitoringClaim result = apiInstance.updateMonitoringClaimStatus(id, ifMatch, updateMonitoringClaimStatusRequest);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling MonitoringClaimApi#updateMonitoringClaimStatus");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The monitoring claim id |
 **ifMatch** | **String**| The etag of the resource | [optional]
 **updateMonitoringClaimStatusRequest** | [**UpdateMonitoringClaimStatusRequest**](UpdateMonitoringClaimStatusRequest.md)|  | [optional]

### Return type

[**MonitoringClaim**](MonitoringClaim.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **400** | Bad Request |  -  |
| **404** | Not Found |  -  |
| **412** | Precondition Failed |  -  |
| **500** | Server Error |  -  |
| **503** | Service Unavailable |  -  |

