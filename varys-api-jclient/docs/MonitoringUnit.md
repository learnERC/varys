

# MonitoringUnit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**meta** | [**Meta**](Meta.md) |  |  [optional]
**pattern** | [**ConfigurationPattern**](ConfigurationPattern.md) |  |  [optional]
**destinationSystem** | **String** |  |  [optional]
**desiredConfigurations** | **List&lt;String&gt;** |  |  [optional]
**ongoingConfigurations** | **List&lt;String&gt;** |  |  [optional]
**currentConfigurations** | **List&lt;String&gt;** |  |  [optional]
**status** | [**MonitoringUnitStatus**](MonitoringUnitStatus.md) |  |  [optional]
**metadata** | **Map&lt;String, String&gt;** |  |  [optional]



